﻿using vvclient.product;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace vvclient.product.Tests
{
    [TestClass()]
    public class ProductTest
    {
        [TestInitialize()]
        public void SetUp()
        {
            var product = new Product();
            product.SetArticle("Article 1");
            product.SetName("Name 1");
            product.SetBatch("0");
            product.SetId("123");
            product.SetPrice(200);
            product.SetDiscount(20);
            product.Save();

            var product_1 = new Product();
            product_1.SetArticle("Article 2");
            product_1.SetName("Name 2");
            product_1.SetBatch("0");
            product_1.SetId("1234");
            product.SetPrice(100);
            product.SetDiscount(20);
            product_1.Save();

            var product_2 = new Product();
            product_2.SetArticle("Article 2");
            product_2.SetName("Name 2");
            product_2.SetBatch("1");
            product_2.SetId("123");
            product.SetPrice(10);
            product.SetDiscount(20);
            product_2.Save();
        }

        [TestMethod()]
        public void GetProductTest()
        {
            var product = Product.GetProduct("123", "0");
            Assert.AreEqual(product.GetBatch(), "0");
            Assert.IsNotNull(product);

            product = Product.GetProduct("123", "1");
            Assert.AreEqual(product.GetBatch(), "1");

            // Invalid product
            product = Product.GetProduct("23", "1");
            Assert.IsNull(product);
        }

        [TestCleanup]
        public void TearDown()
        {
            var product = Product.GetProduct("123", "0");
            product.Delete();
            product = Product.GetProduct("123", "1");
            product.Delete();
            product = Product.GetProduct("1234", "0");
            product.Delete();
        }

        [TestMethod()]
        public void GetProductsTest()
        {
            var products = Product.GetProducts("123");
            Assert.AreEqual(2, products.Count);
            products = Product.GetProducts("");
            Assert.AreEqual(0, products.Count);
            products = Product.GetProducts("0123");
            Assert.AreEqual(0, products.Count);
        }
    }
}
