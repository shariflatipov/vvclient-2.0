﻿using System;
using System.Windows.Forms;

namespace vvclient.config
{
    public partial class CanDeleteItem : Form
    {
        private string barcode = "";

        public CanDeleteItem()
        {
            InitializeComponent();
        }

        private void CanDeleteItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                barcode = barcode.Replace("D", "");
                if (settings.Settings.GetValue("access_code") == barcode)
                {
                    DialogResult = DialogResult.OK;
                }
                barcode = "";
            }
            else
            {
                barcode += e.KeyCode.ToString();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}