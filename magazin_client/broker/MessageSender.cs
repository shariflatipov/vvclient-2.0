﻿using System;
using System.Text;
using RabbitMQ.Client;
using vvclient.settings;
using vvclient.utils;

namespace vvclient.broker
{
    internal class MessageSender
    {
        public static void Send(object message)
        {
            if (!Config.SendToRabbitMQ)
                return;

            try
            {
                var factory = new ConnectionFactory {HostName = MainForm.Ip};
                factory.UserName = "oazis";
                factory.Password = "123";
                factory.VirtualHost = "/";

                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare(MainForm.Stock, false, false, false, null);

                        var body = Encoding.UTF8.GetBytes(message.ToString());

                        channel.BasicPublish("", MainForm.Stock, null, body);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Log(exception, "sending message to rabbit mq", Level.Warining);
            }
        }
    }
}