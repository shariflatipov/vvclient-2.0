﻿using System;
using System.Collections.Generic;
using System.Configuration;
using vvclient.discount;
using vvclient.payments;
using vvclient.peripheral;
using vvclient.product;
using vvclient.settings;
using vvclient.utils;

namespace vvclient
{
    public class PrintChk
    {
        private readonly string _acceptedMoney;
        private readonly string _changeBack;
        private readonly List<Product> _dg;
        private readonly Discount _discount;

        private readonly IPrinter _printer =
            (IPrinter) Activator.CreateInstance(Type.GetType("vvclient.peripheral." + Config.PrinterPackage));

        private readonly string _subTotal;
        private readonly string _total, _totalDiscount;
        private readonly string _transactionId;
        private readonly int _paperSize = 58;
        private PaymentType _payType;
        private readonly bool _printHeaders;

        public PrintChk(string accept, string change, string total, string totalDiscount, List<Product> dgv,
            string subTotal, string transactionId, Discount discount, PaymentType payType, bool printHeaders = true )
        {
            _printHeaders = printHeaders;
            try
            {
                _acceptedMoney = accept;
                _changeBack = change;
                _total = total;
                _totalDiscount = totalDiscount;
                _discount = discount;
                _dg = dgv;
                _subTotal = subTotal;
                _transactionId = transactionId;
                _payType = payType;
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "PrintCheck", Level.Warining);
            }
        }

        private void SaveToFile(string str)
        {
            _printer.Print(str + "\r\n");
        }

        public void Print()
        {
            _printer.BoldOn();

            if (_printHeaders)
            {

                SaveToFile(Config.StoreName);
                SaveToFile(Config.StoreAddress);
                SaveToFile(Config.StoreINN);
                SaveToFile(MainForm.Login);

                if (_discount != null)
                {
                    SaveToFile((_discount.Name ?? "") + " " + (_discount.Surname ?? "") + " " +
                               (_discount.Patronymic ?? ""));
                }

            }

            if (ConfigurationManager.AppSettings["showTRN"] != null &&
                ConfigurationManager.AppSettings["showTRN"] == "true")
            {
                SaveToFile(_transactionId);
            }
            SaveToFile("---------------------------------------");
            _printer.BoldOff();

            var i = 0;
            foreach (var product in _dg)
            {
                // TODO make printer size 58mm and 80mm


                if (_paperSize == 58)
                {
                    var prodName = product.GetName();
                    if (prodName.Length > 27)
                    {
                        prodName = prodName.Substring(0, 27);
                    }
                    prodName = (i += 1) + ". " + prodName;
                    SaveToFile(prodName);

                    var resultString = product.GetQuantity() + " X " + product.GetPrice()
                         + " (-" + product.GetDiscount() + "%)";
                    resultString = resultString.PadLeft(18);

                    SaveToFile(resultString + " = " + product.GetTotal());
                }
                else if (_paperSize == 80)
                {
                    var prodName = product.GetName();
                    if (prodName.Length > 23)
                    {
                        prodName = prodName.Substring(0, 23);
                    }
                    prodName = (i += 1) + ". " + prodName;

                    var resultString = product.GetQuantity() + " X " + product.GetPrice()
                         + " (-" + product.GetDiscount() + "%)";

                    SaveToFile(prodName + " " + resultString + " = " + product.GetTotal());
                }
            }
            _printer.BoldOn();
            SaveToFile("---------------------------------------");

            SaveToFile("Сумма:       " + _subTotal);

            if (_printHeaders)
            {
                SaveToFile("Скидка:      " + _totalDiscount);
                SaveToFile("Итого:       " + _total);
                SaveToFile("Дата:        " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));

                if (_payType != PaymentType.cash && _payType != PaymentType.card)
                {
                    SaveToFile("Тип оплаты:  " + ((_payType == PaymentType.card) ? "Карта" : "В долг"));
                }
                SaveToFile("");
                SaveToFile("");
                SaveToFile(Config.StoreSlogan);
                SaveToFile(Config.StorePhones);
                SaveToFile("      СПАСИБО ЗА ПОКУПКУ!!!!   ");
                SaveToFile("");
                SaveToFile("");
            }

            _printer.BoldOff();
     
            _printer.Cut();
        }
    }
}
