﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using vvclient.nums;
using vvclient.product;
using vvclient.settings;

namespace vvclient.goodsreturn
{
    public partial class ReturnMain : Form
    {
        private readonly List<Product> _products = new List<Product>();
        private bool _isActionReturn = true;
        private ReturnGood _returnGood = new ReturnGood();

        public ReturnMain()
        {
            InitializeComponent();
        }

        private void TxtBarcodeTextChanged(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                _isActionReturn = true;

                if (dgReturn.Rows.Count > 0)
                {
                    dgReturn.Rows.Clear();
                }

                _returnGood = ReturnGood.GetById(txtBarcode.Text);
                var alreadyReturned = ReturnGood.GetReturnsBySoldId(txtBarcode.Text);

                foreach (var item in _returnGood.ReturnProductList)
                {

                    foreach (var returns in alreadyReturned)
                    {
                        if (returns.Batch == item.Batch && returns.Product == item.Product)
                        {
                            item.Count -= returns.Count;
                        }
                    }

                    if (float.Parse(item.Discount) > 99)
                    {
                        _isActionReturn = false;
                    }
                    if (item.Count > 0)
                    {
                        dgReturn.Rows.Add(item.TrnId, item.ProductName, item.Product, item.Count, item.Price, item.Batch,
                            item.Discount);
                    }
                }

                btnConfirm.Enabled = _isActionReturn;
            }
        }

        private void BtnConfirmClick(object sender, EventArgs e)
        {
            _returnGood.ReturnProductList.Clear();

            if (chkPrepayment.Checked)
            {
                _returnGood.PayType = PayType.PREPAYMENT;
            }

            int selectedCounter = 0;

            foreach (DataGridViewRow item in dgReturn.SelectedRows)
            {
                var collection = new ReturnProductCollection();
                collection.TrnId = long.Parse(item.Cells["trnId"].Value.ToString());
                collection.Batch = item.Cells["batch"].Value.ToString();
                collection.Product = item.Cells["barcode"].Value.ToString();
                collection.ProductName = item.Cells["GoodName"].Value.ToString();
                collection.Count = double.Parse(item.Cells["Count"].Value.ToString());
                collection.Price = double.Parse(item.Cells["Price"].Value.ToString());
                collection.Discount = item.Cells["Discount"].Value.ToString();
                _returnGood.ReturnProductList.Add(collection);
                selectedCounter++;
            }
            if (selectedCounter > 0)
            {
                _returnGood.Save();
                Dispose();
            } else
            {
                MessageBox.Show("Не выбран товар для возврата");
            }
        }

        private void btnSearchByBarcode_Click(object sender, EventArgs e)
        {
            if (dgReturn.Rows.Count > 0)
            {
                dgReturn.Rows.Clear();
            }

            _returnGood = ReturnGood.GetReturnByProductBarcode(txtBarcode.Text);

            if (_returnGood != null)
            {
                foreach (var item in _returnGood.ReturnProductList)
                {
                    dgReturn.Rows.Add(item.TrnId, item.ProductName, item.Product, item.Count, item.Price, item.Batch,
                        item.Discount);
                }
            }

            _products.Clear();
        }

        private void ReturnMain_Load(object sender, EventArgs e)
        {
            chkPrepayment.Visible = Config.ShowCheckboxPrepayment;
        }

        private void dgReturn_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 3)
                {
                    int currentVal = int.Parse(dgReturn.Rows[e.RowIndex].Cells[3].Value.ToString());
                    // Change quantity of goods and calculate total
                    var nums = new NumsReturn(double.Parse(dgReturn.Rows[e.RowIndex].Cells[3].Value.ToString()));
                    nums.ShowDialog();
                    var quantity = nums.GetQuantity();

                    if (quantity <= currentVal)
                    {
                        dgReturn.Rows[e.RowIndex].Cells[3].Value = quantity;
                    }
                }
            }
        }
    }
}
