﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using vvclient.utils;

namespace vvclient.goodsreturn
{
    public enum PayType
    {
        STANDARD = 0,
        PREPAYMENT = 1
    }

    public class ReturnProductCollection
    {
        public string Batch;
        public double Count;
        public string Discount;
        public double Price;
        public string Product;
        public string ProductName;
        public long TrnId;
    }


    public class ReturnGood
    {
        public readonly List<ReturnProductCollection> ReturnProductList = new List<ReturnProductCollection>();
        public PayType PayType = PayType.STANDARD;
        private string _soldId;
        private double _discount = 0;
        private double _sum = 0;

        public static ReturnGood GetReturnByProductBarcode(string productBarcode)
        {
            ReturnGood result = null;

            using (var conn = new SQLiteConnection())
            {
                conn.ConnectionString = SQLiteWrapper.ConnectionString;

                using (var command = new SQLiteCommand(conn))
                {
                    //  sold_id, trn_id, count, sum, batch, product_id, discount
                    var commandText =
                        "SELECT t.sold_id, t.id_trn, p.product_name, t.count, t.price, t.batch, p.product_id, t.discount " +
                        "from transactions t, product p where t.sold_id not in (select sold_id from return_transactions) and " +
                        "t.product_id = p.product_id and t.product_id = '" +
                        productBarcode + "' limit 1";
                    command.CommandText = commandText;
                    conn.Open();

                    
                    using (var reader = command.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            var count = double.Parse(reader[3].ToString());
                            var price = double.Parse(reader[4].ToString());
                            var discount = Convert.ToDouble(reader[7].ToString());

                            result = new ReturnGood {_soldId = reader[0].ToString()};
                            var rpc = new ReturnProductCollection
                            {
                                TrnId = long.Parse(reader[1].ToString()),
                                ProductName = reader[2].ToString(),
                                Count = count,
                                Price = price,
                                Batch = reader[5].ToString(),
                                Product = reader[6].ToString(),
                                Discount = discount.ToString()
                            };
                            result.ReturnProductList.Add(rpc);
                        }
                    }
                }
            }
            return result;
        }

        public static ReturnGood GetById(string tmpBarcode)
        {
            var result = new ReturnGood {_soldId = tmpBarcode};

            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText =
                            "SELECT t.id_trn, p.product_name, t.count, t.price, t.batch, p.product_id, t.discount, sp.discount, sp.sum " +
                            "FROM transactions t, sold_product sp, product p  " +
                            "WHERE t.sold_id = '" + tmpBarcode + "' AND sp.status > 1 " +
                            "AND t.sold_id = sp.id_sold AND p.product_id = t.product_id AND t.batch = p.batch";
                        connection.Open();

                        double totalDiscount = 0;
                        double totalSum = 0;
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var count =double.Parse(reader[2].ToString());
                                var price = double.Parse(reader[3].ToString());
                                var discount = Convert.ToDouble(reader[6].ToString());

                                totalDiscount = Convert.ToDouble(reader[7].ToString());
                                totalSum = Convert.ToDouble(reader[8].ToString());

                                var rpc = new ReturnProductCollection {
                                    TrnId = long.Parse(reader[0].ToString()),
                                    ProductName = reader[1].ToString(),
                                    Count = count,
                                    Price = price,
                                    Batch = reader[4].ToString(),
                                    Product = reader[5].ToString(),
                                    Discount = discount.ToString()
                                };
                                result.ReturnProductList.Add(rpc);
                            }

                            result._discount = totalDiscount;
                            result._sum = totalSum;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(e, "ReturnGood.cs=>GetById");
            }
            return result;
        }

        public static List<ReturnProductCollection> GetReturnsBySoldId(string soldId)
        {
            var result = new List<ReturnProductCollection>();
            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText =
                            "select quantity, price, batch, product_id, discount from return_details rd where return_id in " +
                            "(select id from return_product rp where rp.sold_id = '" + soldId + "')";
                        connection.Open();

                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var r = new ReturnProductCollection()
                                {
                                    Batch = reader["batch"].ToString(),
                                    Count = Convert.ToInt32(reader["quantity"].ToString()),
                                    Discount = reader["discount"].ToString(),
                                    Price = Convert.ToDouble(reader["price"].ToString()),
                                    Product = reader["product_id"].ToString()
                                };
                                result.Add(r);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(e, "ReturnGood.cs=>GetById");
            }
            return result;
        }

        public void RemoveItem(int index)
        {
            ReturnProductList.RemoveAt(index);
        }

        public void Save()
        {
            var returnId = Utils.GetId();
            using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var command = new SQLiteCommand(connection))
                {
                    connection.Open();

                    var transaction = connection.BeginTransaction();

                    try
                    {
                        command.Connection = connection;
                        command.Transaction = transaction;
                        double localTotal = 0;

                        foreach (var item in ReturnProductList)
                        {
                            command.CommandText =
                                "INSERT INTO return_details(return_id, quantity, price, batch, product_id, discount) " +
                                "VALUES(@return_id, @count, @sum, @batch, @product_id, @discount)";

                            command.Parameters.Add(new SQLiteParameter("@return_id", returnId));
                            command.Parameters.Add(new SQLiteParameter("@count", item.Count));
                            command.Parameters.Add(new SQLiteParameter("@sum", item.Price));
                            command.Parameters.Add(new SQLiteParameter("@batch", item.Batch));
                            command.Parameters.Add(new SQLiteParameter("@product_id", item.Product));
                            command.Parameters.Add(new SQLiteParameter("@discount", item.Discount));

                            localTotal += (item.Count * item.Price) - (item.Count * item.Price * Double.Parse(item.Discount) / 100);

                            command.ExecuteNonQuery();
                        }

                        command.CommandText = "INSERT INTO return_product(id, sold_id, seller_id, created_at, total, discount, paytype) " +
                                              "VALUES ('" + returnId + "', '" + _soldId + "', '" + MainForm.Id + "', '" +
                                              DateTime.Now.ToString(Utils.DateFormat) + "', " + localTotal + ", " + localTotal * (_discount * 100 / _sum) / 100 + ", " + (int)PayType + ")";

                        command.ExecuteNonQuery();

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        Logger.Log(e, "ReturnGood=>Save");
                        transaction.Rollback();
                        MessageBox.Show("Не удалось завершить возврат", "Ошибка", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }
                    finally
                    {
                        command.Parameters.Clear();
                    }
                }
            }
        }


        public static void SendReturns()
        {
            try {
                using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString)) {
                    using (var cmd = new SQLiteCommand(conn)) {
                        conn.Open();
                        cmd.CommandText =
                            "select rt.id, rt.sold_id, rt.created_at, rt.total, rt.discount, rt.state, " +
                            " s.login, rt.paytype from return_product rt, seller s where " +
                            " rt.seller_id = s.id_seller and state <> 2 limit 1";
                        using (var rs = cmd.ExecuteReader()) {
                            while (rs.Read()) {
                                XMLHeaders headers = new XMLHeaders(
                                    DateTime.Parse(rs["created_at"].ToString()),
                                    rs["login"].ToString()
                                );

                                headers.SetAct(5);

                                var root = headers.GetRootElements();
                                var sold = new XElement("sold");
                                string returnId = rs["id"].ToString();
                                sold.Add(new XAttribute("id", rs["sold_id"].ToString()));
                                sold.Add(new XAttribute("date", Utils.GetNowFormated()));
                                sold.Add(new XAttribute("sum", rs["total"].ToString()));
                                sold.Add(new XAttribute("discount", rs["discount"].ToString()));

                                string paytype = "";

                                if (Convert.ToInt16(rs["paytype"].ToString()) == 1)
                                {
                                    paytype = "prepay";
                                }

                                sold.Add(new XAttribute("pay_type", paytype));

                                using (var details = new SQLiteCommand(conn)) {
                                    details.CommandText = "select quantity, price, batch, product_id, discount from return_details where return_id = @return_id";
                                    details.Parameters.Add(new SQLiteParameter("@return_id", returnId));

                                    using (var rsDetail = details.ExecuteReader()) {
                                        while (rsDetail.Read()) {
                                            var product = new XElement("product");

                                            product.Add(new XAttribute("id", rsDetail["product_id"].ToString()));
                                            product.Add(new XElement("count", rsDetail["quantity"].ToString()));
                                            product.Add(new XElement("price", rsDetail["price"].ToString()));
                                            product.Add(new XElement("batch", rsDetail["batch"].ToString()));
                                            product.Add(new XElement("discount_percent", rsDetail["discount"].ToString()));

                                            sold.Add(product);
                                        }
                                    }

                                    details.Parameters.Clear();
                                }
                                root.Add(sold);

                                var response = HttpCommunication.HttpCommun(root, 30000);
                                if (response is null)
                                {
                                    return;
                                }

                                if (response.Elements().Any())
                                {
                                    var reqTrn = Convert.ToInt64(response.Element("trn").Value);
                                    if (response.Element("status").Value.Equals("2"))
                                    {
                                        using (var update = new SQLiteCommand(conn))
                                        {
                                            update.CommandText = "UPDATE return_product set state = 2 where id = @returnId";
                                            update.Parameters.Add(new SQLiteParameter("@returnId", returnId));
                                            update.ExecuteNonQuery();
                                            update.Parameters.Clear();
                                        }
                                    }
                                }
                            }
                        }
                    }
                } 
            } catch (SQLiteException sqlce) {
                Logger.Log(sqlce, "Return sqlce exception", Level.Fatal);

            } catch (Exception ex) {
                Logger.Log(ex, "Return ex exception", Level.Fatal);
            }
        }
    }
}
