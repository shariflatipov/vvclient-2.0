﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Linq;
using vvclient.counter;
using vvclient.discount;
using vvclient.ff;
using vvclient.goodsreturn;
using vvclient.reports;
using vvclient.settings;
using vvclient.utils;

namespace vvclient
{
    public partial class SettingsForm : Form
    {
        private Counter counter = new Counter();


        public SettingsForm()
        {
            InitializeComponent();
            ShowHideButtons();
        }

        private void ShowHideButtons()
        {
            if (!Config.ShowDailyReport)
            {
                btnPrintDailyReport.Visible = false;
            }
        }

        private void btnPrintDailyReport_Click(object sender, EventArgs e)
        {
            var drView = new DailyReportView();
            drView.ShowDialog();
        }

        private void btnRegisterReturn_Click(object sender, EventArgs e)
        {
            var retMain = new ReturnMain();
            retMain.ShowDialog();
        }

        private void bntAddDiscount_Click(object sender, EventArgs e)
        {
            var df = new DiscountListForm();
            df.ShowDialog();
        }

        private void btnOtherOperations_Click(object sender, EventArgs e)
        {
            var browserForm = new MainBrowser();
            browserForm.ShowDialog();
        }

        /// <summary>
        ///     Opens form with database operations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearData_Click(object sender, EventArgs e)
        {
            var operations = new DatabaseOperations();
            operations.ShowDialog();
        }

        private void changeSettings_Click(object sender, EventArgs e)
        {
            var setting = new SettingForm();
            setting.ShowDialog();
        }

        private void btnKeyboard_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("explorer.exe");
                Process.Start("osk.exe");
            }
            catch (Exception exception)
            {
                Logger.Log(exception, "show keyboard");
            }
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            counter.GetCount().ToString();
            lblTurnCounter.Text = TransactionCounter.getTransactionInTurnCount();
        }

        private void btnSendPeopleCount_Click(object sender, EventArgs e)
        {
            XMLHeaders header = new XMLHeaders();
            header.SetAct(23);
            XElement count = new XElement("count", counter.GetCount());
            XElement date = new XElement("date", counter.GetDate());
            header.GetRootElements().Add(count);
            header.GetRootElements().Add(date);
            var result = HttpCommunication.HttpCommun(header.GetRootElements(), 5000);
            if (result.Elements().Any())
            {
                var element = result.Element("status");
                if (element != null && element.Value.Equals("2"))
                {
                    MessageBox.Show("Все данные успешно отправлены на сервер");
                }
                else
                {
                    MessageBox.Show("В данный момент сервер не доступен пожалуйста повторите попытку позднее");
                }
            }
        }

        // Установка счетчика посетителей
        private void btnSetLeads_Click(object sender, EventArgs e)
        {
            CounterForm frm = new CounterForm();
            if(frm.ShowDialog() == DialogResult.OK)
            {
                frm.Dispose();
            } else
            {
                MessageBox.Show("В данный момент сохранение данных невозможно\r\nПопробуйте позднее");
            }
        }
    }
}
