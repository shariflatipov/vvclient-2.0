﻿using System;
using System.Windows.Forms;

namespace vvclient
{
    public partial class Help : Form
    {
        public Help()
        {
            InitializeComponent();
        }

        private void Help_Load(object sender, EventArgs e)
        {
            textBox1.Text = "\r\n   1) Пароль должен состоять как минимум из 6 символов\r\n" +
                            "\r\n   2) Поля \"Пароль\" и \"Подтвердите пароль\" должны совпадать\r\n";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}