﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Media;
using System.Threading;
using System.Windows.Forms;
using vvclient.actions;
using vvclient.broker;
using vvclient.config;
using vvclient.counter;
using vvclient.discount;
using vvclient.goodsreturn;
using vvclient.nums;
using vvclient.peripheral;
using vvclient.product;
using vvclient.settings;
using vvclient.utils;

namespace vvclient {
    public partial class MainWorkForm : Form {
        public static Product _product = new Product();
        private readonly ProductCart _productCart;
        private readonly List<Product> _storedProductList = new List<Product>();
        private readonly IVfdDisplays _vfd = new VfdDisplay();
        private CalculateActionDiscount _cad = new CalculateActionDiscount();
        private Discount _discountHolder;
        private string _tempBarCode = "";
        private double _totalCost;

        public MainWorkForm() {
            InitializeComponent();
            if (Config.HideTopBar) {
                HideTopBar();
            }
            HideUneseccaryColumns();
            KeyPreview = true;
            picUsrLogo.Image = Utils.GetImageFromPath("img/logo.png");
            _productCart = new ProductCart(dgProductCart);
        }

        private void HideUneseccaryColumns() {
            var freeSpace = 0;
            if (Config.HideColumnDiscount) {
                dgProductCart.Columns["Discount"].Visible = false;
                freeSpace += dgProductCart.Columns["Discount"].Width;
            }
            if (Config.HideColumnBatch) {
                dgProductCart.Columns["Batch"].Visible = false;
                freeSpace += dgProductCart.Columns["Batch"].Width;
            }
            if (Config.HideColumnUnit) {
                dgProductCart.Columns["Unit"].Visible = false;
                freeSpace += dgProductCart.Columns["Unit"].Width;
            }

            dgProductCart.Columns["Color"].Visible = !Config.HideColumnColor;
            dgProductCart.Columns["Article"].Visible = !Config.HideColumnArticle;

            lblTotal.Visible = !Config.HideTotals;
            dgProductCart.Columns["GoodsName"].Width += freeSpace;
        }

        /// <summary>
        ///     Скрыть быстрый доступ и сдвинуть меню вверх
        /// </summary>
        private void HideTopBar() {
            pnlRoot.RowStyles[0].SizeType = SizeType.Absolute;
            pnlRoot.RowStyles[0].Height = 0;
        }

        /// <summary>
        ///     Показать быстрый доступ и сдвинуть меню вниз
        /// </summary>
        private void ShowTopBar() {
            pnlRoot.RowStyles[0].SizeType = SizeType.Absolute;
            pnlRoot.RowStyles[0].Height = 147;
        }

        private void CalculateTotal() {
            var totals = _productCart.CalculateTotal();
            _totalCost = totals["total"];
            lblProductCount.Text = totals["totalQuantity"].ToString();
            lblTotal.Text = _totalCost.ToString();
        }

        private void MainWorkForm_Load(object sender, EventArgs e) {
            // Geting Hot Plug Items into List
            DependentList.GetListContent(lstHotAccess, imLstPics, "MainList");
            lstHotAccess.LargeImageList = imLstPics;
        }

        private void lstHotAccess_MouseClick(object sender, MouseEventArgs e) {
            var a = (ListView)sender;
            ShowDependentListWithoutClosing(a.SelectedItems[0].Index);
        }

        private void ShowDependentListWithoutClosing(int index) {
            if (lstHotAccess.Items.Count > 0) {
                var depList = new DependentList(lstHotAccess.Items[index].Name, _productCart, _discountHolder, false);
                depList.ShowDialog();
                SetProductSettings();
            }
        }

        private void ShowDependentList(int index) {
            var depList = new DependentList(lstHotAccess.Items[index].Name, _productCart, _discountHolder);
            if (depList.ShowDialog() == DialogResult.OK) {
                _product = depList.GetProduct();
                _productCart.AddItem(_product, true);
            }
        }

        private void BtnConfirmBuyClick(object sender, EventArgs e) {
            // Обязательно показать меню пакетов или нет
            if (Config.AskBag) {
                DependentList bag = new DependentList("70000", _productCart, _discountHolder, false) {
                    Dock = DockStyle.Fill
                };

                bag.ShowDialog();

                SetProductSettings();
            }

            try {
                var thread = new Thread(MessageSender.Send);
                thread.Start("3~" + _totalCost);
                if (dgProductCart.Rows.Count > 0) {
                    var chb = new ChangeBack(_totalCost, dgProductCart, _vfd, _discountHolder);

                    if (chb.ShowDialog() == DialogResult.OK) {
                        ClearAll();
                    }
                }
            } catch (Exception exception) {
                Logger.Log(exception, "Can't show ChangeBack form", Level.Fatal);
            }
            dgProductCart.Focus();
        }

        private void DgProductCartCellClick(object sender, DataGridViewCellEventArgs e) {
            var columnName = dgProductCart[e.ColumnIndex, e.RowIndex].OwningColumn.Name;

            if (e.ColumnIndex >= 0 && e.RowIndex >= 0) {
                _product = Product.SetProductById(dgProductCart.Rows[e.RowIndex], e.RowIndex);

                if (columnName == "Remove") {
                    if (!Config.CanDeleteItems) {
                        var canDeleteItem = new CanDeleteItem();

                        if (canDeleteItem.ShowDialog() != DialogResult.OK) {
                            return;
                        }
                    }
                    var thread = new Thread(MessageSender.Send);
                    thread.Start("2~" + _product.GetName() + "~" + _product.GetQuantity() + "~" + _product.GetPrice());
                    // Button delete goods
                    _productCart.RemoveItem(e.RowIndex);
                    CalculateActionDiscount.GetActionDiscount(dgProductCart);
                    ClearFormLbl();
                    CalculateTotal();
                    return;
                }
                if (columnName == "Quantity") {
                    // TODO remove this bad code section
                    // Change quantity of goods and calculate total
                    var nums = new Nums("Количество");
                    if (nums.ShowDialog() == DialogResult.OK) {
                        dgProductCart.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = nums.GetQuantity();

                        dgProductCart.Rows[e.RowIndex].Cells["Total"].Value =
                            nums.GetQuantity() * double.Parse(dgProductCart.Rows[e.RowIndex].Cells
                            ["Price"].Value.ToString());
                        //_product.SetQuantity(nums.GetQuantity());
                        CalculateActionDiscount.GetActionDiscount(dgProductCart);
                        SetProductSettings();
                        nums.Dispose();
                        return;
                    }
                } else if (columnName == "Unit") {
                    if (_product.GetPackCount() > 0) {
                        var chp = new ChangePack(_product.GetPackCount());
                        _product.SetQuantity(chp.GetQuantity());
                        chp.ShowDialog();
                    } else {
                        MessageBox.Show(@"Операция запрещена");
                    }
                } else if (columnName == "Discount") {
                    if (Config.AllowChangeDiscount) {
                        var discount = new Nums("Процент дисконта");
                        if (discount.ShowDialog() == DialogResult.OK) {
                            _product.SetDiscount(discount.GetQuantity());
                            discount.Dispose();
                        }
                    }
                }
                _productCart.AddItem(_product);
                SetProductSettings();
            }
        }

        // Logout event
        private void PicUsrLogoClick(object sender, EventArgs e) {
            ClearAll();
            var au = MdiParent as MainForm;
            if (au != null) {
                au.Auth.MdiParent = MdiParent;
                au.Auth.Show();
            }
            Hide();
        }

        private void MainWorkFormKeyDown(object sender, KeyEventArgs e) {
            e.SuppressKeyPress = true;
            if (e.Control && e.KeyCode == Keys.H) {
                HideTopBar();
            } else if (e.Control && e.KeyCode == Keys.S) {
                ShowTopBar();
            } else if (e.Control && e.KeyCode == Keys.D1) {
                ShowDependentListWithoutClosing(0);
            } else if (e.Control && e.KeyCode == Keys.D2) {
                ShowDependentListWithoutClosing(1);
            } else if (e.Control && e.KeyCode == Keys.D3) {
                ShowDependentListWithoutClosing(2);
            } else if (e.Control && e.KeyCode == Keys.D4) {
                ShowDependentListWithoutClosing(3);
            } else if (e.Control && e.KeyCode == Keys.D5) {
                ShowDependentListWithoutClosing(4);
            } else if (e.Control && e.KeyCode == Keys.D6) {
                ShowDependentListWithoutClosing(5);
            } else if (e.Control && e.KeyCode == Keys.D7) {
                ShowDependentListWithoutClosing(6);
            } else if ((e.KeyValue >= 48 && e.KeyValue <= 57) || (e.KeyValue >= 96 && e.KeyValue <= 105)) {
                //} else if (e.KeyValue >= 48 && e.KeyValue <= 57) {
                // Только цифры от 0 до 9 и Enter
                //_tempBarCode += e.KeyCode.ToString();
                //} else if (e.KeyValue >= 96 && e.KeyValue <= 105)
                //{
                // Цифры из дополнительной секции клавиатуры

                switch (e.KeyCode)
                {
                    case Keys.NumPad0:
                        _tempBarCode += "0";
                        break;
                    case Keys.NumPad1:
                        _tempBarCode += "1";
                        break;
                    case Keys.NumPad2:
                        _tempBarCode += "2";
                        break;
                    case Keys.NumPad3:
                        _tempBarCode += "3";
                        break;
                    case Keys.NumPad4:
                        _tempBarCode += "4";
                        break;
                    case Keys.NumPad5:
                        _tempBarCode += "5";
                        break;
                    case Keys.NumPad6:
                        _tempBarCode += "6";
                        break;
                    case Keys.NumPad7:
                        _tempBarCode += "7";
                        break;
                    case Keys.NumPad8:
                        _tempBarCode += "8";
                        break;
                    case Keys.NumPad9:
                        _tempBarCode += "9";
                        break;
                    default:
                        _tempBarCode += ((char)e.KeyValue).ToString();
                        break;
                }

            } else if (e.KeyCode == Keys.Enter) {
                BarCodeReaded(_tempBarCode);
                _tempBarCode = "";
                CalculateActionDiscount.GetActionDiscount(dgProductCart);
                SetProductSettings();
            } else if (e.KeyCode == Keys.F3) {
                // Добавить товар без штрих кода
                picProductCost_Click(sender, e);
            } else if (e.KeyCode == Keys.F1) {
                // Оформить покупку 
                BtnConfirmBuyClick(sender, e);
            } else if (e.KeyCode == Keys.F2) {
                // Очистить все товары
                ClearDataClick(sender, e);
            } else if (e.KeyCode == Keys.F9) {
                // Discount
                var df = new DiscountListForm();
                df.ShowDialog();
            } else if (e.KeyCode == Keys.F10) {
                // Return
                var retMain = new ReturnMain();
                retMain.ShowDialog();
            } else if (e.KeyCode == Keys.PageUp) {
                Counter counter = new Counter();
                counter.IncrementCounter();
                counter.Save();
                SystemSounds.Beep.Play();
            } else if (e.KeyCode == Keys.PageDown) {
                Counter counter = new Counter();
                counter.DecrementCounter();
                counter.Save();
                SystemSounds.Beep.Play();
            }
        }

        private void BarCodeReaded(string barCode) {
            // Штрих код полностью прочитан
            //int count = barCode.Count(f => f == 'D');
            //if (count > 3)
            //{
            //    barCode = barCode.Replace("D", "");
            //}
            //barCode = barCode.Replace("NumPad", "");
            double weight = 1;

            try {
                barCode = GetBarcodeType(barCode, ref weight);

                var products = Product.GetProducts(barCode);

                switch (products.Count) {
                    case 0:
                        _product = Product.GetProductStub("Товар не зарегистрирован");
                        SystemSounds.Hand.Play();
                        break;
                    case 1:
                        _product = products[0];
                        break;
                    default:
                        SystemSounds.Hand.Play();
                        var chooseBatch = new ChooseBatch(products);
                        chooseBatch.ShowDialog();
                        if (chooseBatch.DialogResult == DialogResult.OK) {
                            foreach (
                                var product in
                                    products.Where(product => product.GetBatch().Equals(chooseBatch.Batch))) {
                                _product = product;
                            }
                        } else {
                            // Невыбрана партия продукта
                            _product = Product.GetProductStub("Выберите партию товара");
                        }
                        break;
                }

                if (!string.IsNullOrEmpty(_product.GetId())) {
                    _product.SetQuantity(weight);
                    _productCart.AddItem(_product, true);
                }
            } catch (Exception ex) {
                // Gets here if tempBarCode length equals 1
                SystemSounds.Hand.Play();
                Logger.Log(ex, "MainWorkForm.cs : function => MainWorkFormKeyDown");
            }
        }

        /// <summary>
        ///     Получает строку штрихкода и определяет к какому типу он относиться
        ///     например если штрихкод начиается цифрами 20 то это от весов
        /// </summary>
        private string GetBarcodeType(string barcode, ref double weight) {
            // Считаем что даже если штрихкод начинается на 20
            // но его длина меньше 20 то это не весовой штрихкод
            if (barcode.Length < 12) {
                return barcode; 
            }

            // Штрихкоды от весов
            // TODO Нужно 20 вынести в настройку
            if (barcode.Substring(0, 2) == "20") {
                // Позиция в штрихкоде где начинается вес товара 
                var weightStartPosition = 7;

                // Позиция в штрихкоде где заканчивается код товара
                var codeEndPosition = 5;

                // Иногда попадаются штрихкоды состоящие из 12 цифр
                if (barcode.Length == 12) {
                    weightStartPosition = 6;
                    codeEndPosition = 4;
                }

                weight = Math.Round((Convert.ToSingle(barcode.Substring(weightStartPosition, 5)) / 1000), 3);
                return Convert.ToInt32(barcode.Substring(2, codeEndPosition)).ToString();
            }

            return barcode;
        }

        private void SetProductSettings() {
            try {
                lblProductCost.Text = _product.GetPrice().ToString();
                lblProductName.Text = _product.GetName();
                picProduct.Image = _product.GetImage();

                CalculateTotal();

                if (!_vfd.IsNumeric) {
                    string line1;
                    string l1;

                    if (_product.GetName().Length > 12) {
                        line1 = _product.GetName().Substring(0, 12);
                        l1 = _product.GetPrice().ToString().PadLeft(8);
                    } else {
                        line1 = _product.GetName().PadLeft(12);
                        l1 = _product.GetPrice().ToString().PadLeft(8);
                    }

                    _vfd.Write(line1 + l1);
                    _vfd.Write("Total :" + lblTotal.Text.PadLeft(13));
                } else {
                    _vfd.Write(_product.GetPrice().ToString());
                }
            } catch (Exception ex) {
                Logger.Log(ex, "MainWorkForm.cs : function => SetProductSettings");
            }
        }

        // Delete all goods image
        private void ClearDataClick(object sender, EventArgs e) {
            ClearAll();
        }

        // Clear all labels in form
        private void ClearFormLbl() {
            lblProductCost.Text = "";
            lblProductName.Text = "";
            picProduct.Image = null;
            lblTotal.Text = "";
        }

        private void ClearDiscountHolder() {
            lblClientInfo.Text = "";
            _discountHolder = null;
        }

        public void ClearAll() {
            _product = new Product();
            _productCart.Clear();
            ClearFormLbl();
            ClearDiscountHolder();
            dgProductCart.Focus();
        }

        // Saves datagrid data to the temporary store 
        private void SaveDataClick(object sender, EventArgs e) {
            if (dgProductCart.Rows.Count > 0)
            {
                _storedProductList.Clear();

                foreach (DataGridViewRow row in dgProductCart.Rows) {
                    var product = new Product();
                    product.SetProductByFields(row.Cells["Id"].Value.ToString(),
                        row.Cells["GoodsName"].Value.ToString(),
                        row.Cells["Description"].Value.ToString(),
                        row.Cells["Unit"].Value.ToString(),
                        Convert.ToDouble(row.Cells["Price"].Value),
                        null,
                        Convert.ToDouble(row.Cells["Quantity"].Value),
                        Convert.ToDouble(row.Cells["Discount"].Value),
                        0,
                        row.Cells["Batch"].Value.ToString());
                    _storedProductList.Add(product);
                }
                ClearAll();
            }
        }

        // Restores data into datagrid from temporary source
        private void RestoreDataClick(object sender, EventArgs e)
        {
            if (_storedProductList.Count > 0)
            {
                if (dgProductCart.Rows.Count > 0)
                {
                    var result = MessageBox.Show("Есть несохраненные данные, хотите добавить данные к существующим?", "Несохраненные данные", MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.Cancel)
                    {
                        return;
                    }
                    if (DialogResult.No == result)
                    {
                        dgProductCart.Rows.Clear();
                    }

                    foreach (var product in _storedProductList)
                    {
                        dgProductCart.Rows.Add(product.GetId(), product.GetName(), product.GetDescription(),
                            product.GetQuantity(),
                            product.GetUnit(), product.GetBatch(), product.GetPrice(),
                            product.GetDiscount(), product.GetTotal());
                    }

                    _storedProductList.Clear();
                    CalculateTotal();
                }
                else
                {
                    foreach (var product in _storedProductList)
                    {
                        dgProductCart.Rows.Add(product.GetId(), product.GetName(), product.GetDescription(),
                            product.GetQuantity(),
                            product.GetUnit(), product.GetBatch(), product.GetPrice(),
                            product.GetDiscount(), product.GetTotal());
                    }

                    _storedProductList.Clear();
                    CalculateTotal();
                }
            }
        }

        // Goods without barcode
        private void picProductCost_Click(object sender, EventArgs e) {
            var product = Product.SetProductById("2500000000001", "0", false);

            var nums = new Nums("Цена");
            if (nums.ShowDialog() == DialogResult.OK) {
                // TODO this is the hack that need to be fixed
                // Need to use GetPrice() instead of GetQuantity()
                product.SetPrice(nums.GetQuantity());
                nums.Dispose();
            }

            _productCart.AddItemByPrice(product);
            SetProductSettings();
        }

        // Administrator settings
        private void PicCart_Click(object sender, EventArgs e) {
            //if (MainForm.Login.ToLower() != "admin" && MainForm.Login.ToLower() != "administrator") return;
            // Todo change this to secure implementation!!
            if (ConfigurationManager.AppSettings["showSettings"] != "true") return;
            var sf = new SettingsForm();
            sf.ShowDialog();
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            if (dgProductCart.Rows.Count > 0) {
                MessageBox.Show("Невозможно выбрать пользователя\r\nПожалуйста удалите ранее пробитые товары", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var discount = new DiscountListForm();
            if (discount.ShowDialog() == DialogResult.OK) {
                _discountHolder = discount.discount;
                lblClientInfo.Text = "Клиент: " +
                                     _discountHolder.Surname + " " + _discountHolder.Name + " " +
                                     _discountHolder.Patronymic + "\r\n" +
                                     "Остаток лимита: " + _discountHolder.Saldo;
            } else {
                _discountHolder = null;
                lblClientInfo.Text = "";
            }
            dgProductCart.Focus();
        }

        private void lblClientInfo_Click(object sender, EventArgs e) {
            var clientInfo = new ClientInfo(_discountHolder);
            clientInfo.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SearchByProductName sbpn = new SearchByProductName(_productCart);
            sbpn.ShowDialog();

            CalculateTotal();
        }
    }
}
