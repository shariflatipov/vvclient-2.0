﻿using System;
using System.Data.SQLite;
using System.Xml.Linq;
using vvclient.utils;

namespace vvclient
{
    internal class CreateXml
    {
        private const int Act = 0;
        private readonly string _dateToSend;
        private readonly double _discount;
        private readonly string _discountNum;
        private readonly string _login;
        private readonly string _pass;
        private readonly string _paymentType;
        private readonly string _shift;
        private readonly string _trnId;
        private readonly double _sum;
        private XElement _magazin;
        private XElement _sold;

        public CreateXml(string trnId, string dateToSend, string login, string pass, double discount,
            string discountNum, string shift, string paymentType, double sum)
        {
            _trnId = trnId;
            var b = DateTime.Parse(dateToSend);
            _dateToSend = b.ToString("yyyy-MM-dd HH:mm:ss");
            _login = login;
            _pass = pass;
            _discount = discount;
            _discountNum = discountNum;
            _shift = shift;
            _paymentType = paymentType;
            _sum = sum;
        }

        private void Init()
        {
            var password = MyCrypt.GetMd5Hash(_pass);
            var hashSum = MyCrypt.GetSha1(_dateToSend + "#" + _login + "#" + password);

            var seller = new XElement("seller");
            seller.Add(new XAttribute("login", _login));
            seller.Add(new XAttribute("checksum", hashSum));
            seller.Add(new XAttribute("stock", MainForm.Stock));
            seller.Add(new XAttribute("act", Act));
            seller.Add(new XAttribute("date", _dateToSend));
            seller.Add(new XAttribute("shift", _shift));
            _magazin = new XElement("magazin");
            _magazin.Add(new XElement(seller));
        }

        public XElement FillProducts()
        {
            Init();
            _sold = new XElement("sold");
            _sold.Add(new XAttribute("id", _trnId));
            _sold.Add(new XAttribute("date", _dateToSend));
            _sold.Add(new XAttribute("discount_num", _discountNum));
            _sold.Add(new XAttribute("pay_type", _paymentType));
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                conn.ConnectionString = SQLiteWrapper.ConnectionString;
                using (var cmd = new SQLiteCommand())
                {
                    try
                    {
                        cmd.CommandText =
                            "select product_id, count, price, batch, discount from transactions where sold_id = @trn";
                        cmd.Connection = conn;
                        conn.Open();
                        cmd.Parameters.Add(new SQLiteParameter("@trn", _trnId));
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var product = new XElement("product");

                                product.Add(new XAttribute("id", reader[0].ToString()));
                                product.Add(new XElement("count", reader[1].ToString()));
                                product.Add(new XElement("price", reader[2].ToString()));
                                product.Add(new XElement("batch", reader[3].ToString()));
                                product.Add(new XElement("discount_percent", reader[4].ToString()));

                                _sold.Add(product);
                            }
                        }

                        _sold.Add(new XAttribute("sum", _sum));
                        _sold.Add(new XAttribute("discount", _discount));

                        _magazin.Add(_sold);
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(ex, "FillProducts()");
                    }
                }
            }
            return _magazin;
        }
    }
}
