﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;
using vvclient.utils;

namespace vvclient
{
    public partial class SearchProductByBarcode : Form
    {
        private DataTable dt = new DataTable();

        public SearchProductByBarcode()
        {
            InitializeComponent();
        }

        private void SearchProductByBarcode_Load(object sender, EventArgs e)
        {
            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText = "select product_id, product_name, product_image from product limit 10";
                        var im = new ImageList();
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var i = 0;
                            im.ImageSize = new Size(100, 100);

                            while (reader.Read())
                            {
                                try
                                {
                                    im.Images.Add(new Bitmap(reader[2].ToString()));
                                }
                                catch (Exception)
                                {
                                    im.Images.Add(new Bitmap("img/no_image.jpg"));
                                }
                                listView1.Items.Add(reader[0].ToString(), reader[1].ToString(), i);
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "SearchProductByBarcode");
            }
            finally
            {
            }
        }
    }
}