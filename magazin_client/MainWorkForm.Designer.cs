﻿
namespace vvclient
{
    partial class MainWorkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lstHotAccess = new System.Windows.Forms.ListView();
            this.imLstPics = new System.Windows.Forms.ImageList(this.components);
            this.pnlRoot = new System.Windows.Forms.TableLayoutPanel();
            this.pnlContent = new System.Windows.Forms.TableLayoutPanel();
            this.pnlLeft = new System.Windows.Forms.TableLayoutPanel();
            this.picUsrLogo = new System.Windows.Forms.PictureBox();
            this.lblProductName = new System.Windows.Forms.Label();
            this.picProduct = new System.Windows.Forms.PictureBox();
            this.lblClientInfo = new System.Windows.Forms.Label();
            this.totalPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lblTotal = new System.Windows.Forms.Label();
            this.picCart = new System.Windows.Forms.PictureBox();
            this.lblProductCost = new System.Windows.Forms.Label();
            this.picProductCost = new System.Windows.Forms.PictureBox();
            this.lblProductCount = new System.Windows.Forms.Label();
            this.dgProductCart = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSaveData = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnConfirmBuy = new System.Windows.Forms.Button();
            this.btnRestoreData = new System.Windows.Forms.Button();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Article = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Color = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Batch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Discount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remove = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pnlRoot.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.pnlLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUsrLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProduct)).BeginInit();
            this.totalPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProductCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgProductCart)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstHotAccess
            // 
            this.lstHotAccess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstHotAccess.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstHotAccess.Location = new System.Drawing.Point(0, 0);
            this.lstHotAccess.Margin = new System.Windows.Forms.Padding(0);
            this.lstHotAccess.MultiSelect = false;
            this.lstHotAccess.Name = "lstHotAccess";
            this.lstHotAccess.Scrollable = false;
            this.lstHotAccess.Size = new System.Drawing.Size(1036, 147);
            this.lstHotAccess.TabIndex = 0;
            this.lstHotAccess.UseCompatibleStateImageBehavior = false;
            this.lstHotAccess.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstHotAccess_MouseClick);
            // 
            // imLstPics
            // 
            this.imLstPics.ColorDepth = System.Windows.Forms.ColorDepth.Depth16Bit;
            this.imLstPics.ImageSize = new System.Drawing.Size(32, 32);
            this.imLstPics.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // pnlRoot
            // 
            this.pnlRoot.ColumnCount = 1;
            this.pnlRoot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlRoot.Controls.Add(this.pnlContent, 0, 1);
            this.pnlRoot.Controls.Add(this.lstHotAccess, 0, 0);
            this.pnlRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRoot.Location = new System.Drawing.Point(0, 0);
            this.pnlRoot.Margin = new System.Windows.Forms.Padding(0);
            this.pnlRoot.Name = "pnlRoot";
            this.pnlRoot.RowCount = 2;
            this.pnlRoot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 147F));
            this.pnlRoot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlRoot.Size = new System.Drawing.Size(1036, 701);
            this.pnlRoot.TabIndex = 17;
            // 
            // pnlContent
            // 
            this.pnlContent.ColumnCount = 2;
            this.pnlContent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 325F));
            this.pnlContent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlContent.Controls.Add(this.pnlLeft, 0, 0);
            this.pnlContent.Controls.Add(this.totalPanel, 1, 1);
            this.pnlContent.Controls.Add(this.dgProductCart, 1, 0);
            this.pnlContent.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.pnlContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContent.Location = new System.Drawing.Point(0, 147);
            this.pnlContent.Margin = new System.Windows.Forms.Padding(0);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.RowCount = 2;
            this.pnlContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 79F));
            this.pnlContent.Size = new System.Drawing.Size(1036, 554);
            this.pnlContent.TabIndex = 1;
            // 
            // pnlLeft
            // 
            this.pnlLeft.ColumnCount = 1;
            this.pnlLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlLeft.Controls.Add(this.picUsrLogo, 0, 0);
            this.pnlLeft.Controls.Add(this.lblProductName, 0, 1);
            this.pnlLeft.Controls.Add(this.picProduct, 0, 2);
            this.pnlLeft.Controls.Add(this.lblClientInfo, 0, 3);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.RowCount = 5;
            this.pnlLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.pnlLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.pnlLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.pnlLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.pnlLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnlLeft.Size = new System.Drawing.Size(325, 475);
            this.pnlLeft.TabIndex = 2;
            // 
            // picUsrLogo
            // 
            this.picUsrLogo.BackColor = System.Drawing.Color.Transparent;
            this.picUsrLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picUsrLogo.Location = new System.Drawing.Point(3, 3);
            this.picUsrLogo.Name = "picUsrLogo";
            this.picUsrLogo.Size = new System.Drawing.Size(319, 58);
            this.picUsrLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picUsrLogo.TabIndex = 12;
            this.picUsrLogo.TabStop = false;
            this.picUsrLogo.Click += new System.EventHandler(this.PicUsrLogoClick);
            // 
            // lblProductName
            // 
            this.lblProductName.BackColor = System.Drawing.Color.White;
            this.lblProductName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProductName.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProductName.Location = new System.Drawing.Point(3, 64);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(319, 64);
            this.lblProductName.TabIndex = 4;
            this.lblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picProduct
            // 
            this.picProduct.BackColor = System.Drawing.Color.White;
            this.picProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picProduct.Location = new System.Drawing.Point(3, 131);
            this.picProduct.Name = "picProduct";
            this.picProduct.Size = new System.Drawing.Size(319, 144);
            this.picProduct.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picProduct.TabIndex = 3;
            this.picProduct.TabStop = false;
            // 
            // lblClientInfo
            // 
            this.lblClientInfo.AutoSize = true;
            this.lblClientInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblClientInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblClientInfo.Location = new System.Drawing.Point(3, 278);
            this.lblClientInfo.Name = "lblClientInfo";
            this.lblClientInfo.Size = new System.Drawing.Size(319, 51);
            this.lblClientInfo.TabIndex = 16;
            this.lblClientInfo.Click += new System.EventHandler(this.lblClientInfo_Click);
            // 
            // totalPanel
            // 
            this.totalPanel.ColumnCount = 5;
            this.totalPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.totalPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.totalPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.totalPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.totalPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.totalPanel.Controls.Add(this.lblTotal, 4, 0);
            this.totalPanel.Controls.Add(this.picCart, 3, 0);
            this.totalPanel.Controls.Add(this.lblProductCost, 2, 0);
            this.totalPanel.Controls.Add(this.picProductCost, 1, 0);
            this.totalPanel.Controls.Add(this.lblProductCount, 0, 0);
            this.totalPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.totalPanel.Location = new System.Drawing.Point(328, 478);
            this.totalPanel.Name = "totalPanel";
            this.totalPanel.RowCount = 1;
            this.totalPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.totalPanel.Size = new System.Drawing.Size(705, 73);
            this.totalPanel.TabIndex = 3;
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.White;
            this.lblTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotal.Font = new System.Drawing.Font("Arial", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTotal.Location = new System.Drawing.Point(519, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(183, 73);
            this.lblTotal.TabIndex = 7;
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // picCart
            // 
            this.picCart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picCart.Image = global::vvclient.Properties.Resources.cart;
            this.picCart.Location = new System.Drawing.Point(449, 3);
            this.picCart.Name = "picCart";
            this.picCart.Size = new System.Drawing.Size(64, 67);
            this.picCart.TabIndex = 11;
            this.picCart.TabStop = false;
            this.picCart.Click += new System.EventHandler(this.PicCart_Click);
            // 
            // lblProductCost
            // 
            this.lblProductCost.BackColor = System.Drawing.Color.White;
            this.lblProductCost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProductCost.Font = new System.Drawing.Font("Arial", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProductCost.Location = new System.Drawing.Point(261, 0);
            this.lblProductCost.Name = "lblProductCost";
            this.lblProductCost.Size = new System.Drawing.Size(182, 73);
            this.lblProductCost.TabIndex = 5;
            this.lblProductCost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // picProductCost
            // 
            this.picProductCost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picProductCost.Image = global::vvclient.Properties.Resources.money;
            this.picProductCost.Location = new System.Drawing.Point(191, 3);
            this.picProductCost.Name = "picProductCost";
            this.picProductCost.Size = new System.Drawing.Size(64, 67);
            this.picProductCost.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picProductCost.TabIndex = 13;
            this.picProductCost.TabStop = false;
            this.picProductCost.Click += new System.EventHandler(this.picProductCost_Click);
            // 
            // lblProductCount
            // 
            this.lblProductCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProductCount.Font = new System.Drawing.Font("Arial", 36F);
            this.lblProductCount.Location = new System.Drawing.Point(3, 0);
            this.lblProductCount.Name = "lblProductCount";
            this.lblProductCount.Size = new System.Drawing.Size(182, 73);
            this.lblProductCount.TabIndex = 14;
            this.lblProductCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgProductCart
            // 
            this.dgProductCart.AllowUserToAddRows = false;
            this.dgProductCart.AllowUserToDeleteRows = false;
            this.dgProductCart.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgProductCart.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgProductCart.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgProductCart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProductCart.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.GoodsName,
            this.Article,
            this.Color,
            this.Description,
            this.Quantity,
            this.Unit,
            this.Batch,
            this.Price,
            this.Discount,
            this.Total,
            this.Remove});
            this.dgProductCart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgProductCart.GridColor = System.Drawing.Color.LimeGreen;
            this.dgProductCart.Location = new System.Drawing.Point(325, 0);
            this.dgProductCart.Margin = new System.Windows.Forms.Padding(0);
            this.dgProductCart.MultiSelect = false;
            this.dgProductCart.Name = "dgProductCart";
            this.dgProductCart.ReadOnly = true;
            this.dgProductCart.RowHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dgProductCart.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgProductCart.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgProductCart.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProductCart.Size = new System.Drawing.Size(711, 475);
            this.dgProductCart.TabIndex = 1;
            this.dgProductCart.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgProductCartCellClick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnSaveData, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnAdd, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnConfirmBuy, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnRestoreData, 4, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 478);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(319, 73);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // btnSaveData
            // 
            this.btnSaveData.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSaveData.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.btnSaveData.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.btnSaveData.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.btnSaveData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveData.ForeColor = System.Drawing.Color.Black;
            this.btnSaveData.Image = global::vvclient.Properties.Resources.document_save;
            this.btnSaveData.Location = new System.Drawing.Point(192, 3);
            this.btnSaveData.Name = "btnSaveData";
            this.btnSaveData.Size = new System.Drawing.Size(57, 67);
            this.btnSaveData.TabIndex = 15;
            this.btnSaveData.TabStop = false;
            this.btnSaveData.UseVisualStyleBackColor = false;
            this.btnSaveData.Click += new System.EventHandler(this.SaveDataClick);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.btnAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.btnAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.ForeColor = System.Drawing.Color.Black;
            this.btnAdd.Image = global::vvclient.Properties.Resources.add;
            this.btnAdd.Location = new System.Drawing.Point(66, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(57, 67);
            this.btnAdd.TabIndex = 17;
            this.btnAdd.TabStop = false;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Image = global::vvclient.Properties.Resources.search1;
            this.button1.Location = new System.Drawing.Point(129, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 67);
            this.button1.TabIndex = 18;
            this.button1.TabStop = false;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnConfirmBuy
            // 
            this.btnConfirmBuy.BackColor = System.Drawing.Color.Transparent;
            this.btnConfirmBuy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnConfirmBuy.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.btnConfirmBuy.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.btnConfirmBuy.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.btnConfirmBuy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmBuy.ForeColor = System.Drawing.Color.Black;
            this.btnConfirmBuy.Image = global::vvclient.Properties.Resources.camera_test;
            this.btnConfirmBuy.Location = new System.Drawing.Point(3, 3);
            this.btnConfirmBuy.Name = "btnConfirmBuy";
            this.btnConfirmBuy.Size = new System.Drawing.Size(57, 67);
            this.btnConfirmBuy.TabIndex = 8;
            this.btnConfirmBuy.TabStop = false;
            this.btnConfirmBuy.UseVisualStyleBackColor = false;
            this.btnConfirmBuy.Click += new System.EventHandler(this.BtnConfirmBuyClick);
            // 
            // btnRestoreData
            // 
            this.btnRestoreData.BackColor = System.Drawing.Color.Transparent;
            this.btnRestoreData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRestoreData.FlatAppearance.BorderColor = System.Drawing.Color.Lime;
            this.btnRestoreData.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.btnRestoreData.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.btnRestoreData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRestoreData.ForeColor = System.Drawing.Color.Black;
            this.btnRestoreData.Image = global::vvclient.Properties.Resources.view_refresh;
            this.btnRestoreData.Location = new System.Drawing.Point(255, 3);
            this.btnRestoreData.Name = "btnRestoreData";
            this.btnRestoreData.Size = new System.Drawing.Size(61, 67);
            this.btnRestoreData.TabIndex = 16;
            this.btnRestoreData.TabStop = false;
            this.btnRestoreData.UseVisualStyleBackColor = false;
            this.btnRestoreData.Click += new System.EventHandler(this.RestoreDataClick);
            // 
            // Id
            // 
            this.Id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Id.HeaderText = "Column1";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            this.Id.Width = 54;
            // 
            // GoodsName
            // 
            this.GoodsName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.GoodsName.HeaderText = "Наименование продукта";
            this.GoodsName.Name = "GoodsName";
            this.GoodsName.ReadOnly = true;
            // 
            // Article
            // 
            this.Article.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Article.HeaderText = "Артикул";
            this.Article.Name = "Article";
            this.Article.ReadOnly = true;
            // 
            // Color
            // 
            this.Color.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Color.HeaderText = "Цвет";
            this.Color.Name = "Color";
            this.Color.ReadOnly = true;
            // 
            // Description
            // 
            this.Description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Description.HeaderText = "Описание продукта";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.Visible = false;
            this.Description.Width = 131;
            // 
            // Quantity
            // 
            this.Quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Quantity.HeaderText = "Кол-во";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            this.Quantity.Width = 66;
            // 
            // Unit
            // 
            this.Unit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Unit.HeaderText = "Ед. изм";
            this.Unit.Name = "Unit";
            this.Unit.ReadOnly = true;
            this.Unit.Width = 71;
            // 
            // Batch
            // 
            this.Batch.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Batch.HeaderText = "Серия";
            this.Batch.Name = "Batch";
            this.Batch.ReadOnly = true;
            this.Batch.Width = 63;
            // 
            // Price
            // 
            this.Price.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Price.HeaderText = "Цена";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.Width = 58;
            // 
            // Discount
            // 
            this.Discount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Discount.HeaderText = "Скидка";
            this.Discount.Name = "Discount";
            this.Discount.ReadOnly = true;
            this.Discount.Width = 69;
            // 
            // Total
            // 
            this.Total.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Total.HeaderText = "Итого";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Width = 62;
            // 
            // Remove
            // 
            this.Remove.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Remove.HeaderText = "Удалить";
            this.Remove.Name = "Remove";
            this.Remove.ReadOnly = true;
            this.Remove.Text = "Remove";
            this.Remove.ToolTipText = "Удалить продукт из корзины";
            this.Remove.Width = 56;
            // 
            // MainWorkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1036, 701);
            this.Controls.Add(this.pnlRoot);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainWorkForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Главное окно формы";
            this.Load += new System.EventHandler(this.MainWorkForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainWorkFormKeyDown);
            this.pnlRoot.ResumeLayout(false);
            this.pnlContent.ResumeLayout(false);
            this.pnlLeft.ResumeLayout(false);
            this.pnlLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUsrLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProduct)).EndInit();
            this.totalPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProductCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgProductCart)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstHotAccess;
        private System.Windows.Forms.ImageList imLstPics;
        private System.Windows.Forms.TableLayoutPanel pnlRoot;
        private System.Windows.Forms.TableLayoutPanel pnlContent;
        private System.Windows.Forms.TableLayoutPanel pnlLeft;
        private System.Windows.Forms.PictureBox picUsrLogo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnConfirmBuy;
        private System.Windows.Forms.Button btnSaveData;
        private System.Windows.Forms.Button btnRestoreData;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.PictureBox picProduct;
        private System.Windows.Forms.Label lblClientInfo;
        private System.Windows.Forms.TableLayoutPanel totalPanel;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.PictureBox picCart;
        private System.Windows.Forms.Label lblProductCost;
        private System.Windows.Forms.PictureBox picProductCost;
        private System.Windows.Forms.DataGridView dgProductCart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblProductCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn GoodsName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Article;
        private System.Windows.Forms.DataGridViewTextBoxColumn Color;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Batch;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Discount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewButtonColumn Remove;
    }
}