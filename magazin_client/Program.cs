﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using vvclient.settings;
using vvclient.utils;

namespace vvclient
{
    internal static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary
        [STAThread]
        private static void Main()
        {
            Thread.CurrentThread.CurrentCulture = ApplicationCulture.GetCurrentCulture();
            Thread.CurrentThread.CurrentUICulture = ApplicationCulture.GetCurrentCulture();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            if (!File.Exists("db.db3"))
            {
                Application.Run(new CreateAdminPass());
            }
            else
            {
                try
                {
                    SQLiteWrapper.ExecuteNonQuery("delete from sold_product where sum = 0");
                }
                catch (Exception ex)
                {
                    Logger.Log(ex, "Program.cs : function => Main()");
                }
                try
                {
                    var p = new Process();
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.FileName = "kill.bat";
                    p.Start();

                    var output = p.StandardOutput.ReadToEnd();
                    p.WaitForExit();
                }
                catch
                {
                }
                Application.Run(new MainForm());
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logger.Log((Exception) e.ExceptionObject, "Unhandled exception");
            MessageBox.Show(e.ToString() + sender);
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Logger.Log(e.Exception, "Unhandled thread exception");
            MessageBox.Show(e + sender.ToString());
        }
    }
}