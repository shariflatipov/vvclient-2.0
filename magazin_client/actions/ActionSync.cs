﻿using System.Collections.Generic;
using System.Linq;
using vvclient.utils;
using vvclient.actions.dao;

namespace vvclient.actions
{
    public class ActionSync
    {
        public static List<NextWithDiscountProduct> GetNextWithDiscountAction()
        {
            var returnVal = new List<NextWithDiscountProduct>();
            var headers = new XMLHeaders();
            headers.SetAct(30);
            var result = HttpCommunication.HttpCommun(headers.GetRootElements(), 5000);

            if(result != null)
            {
                if (result.Elements().Any())
                {
                    var element = result.Element("status");
                    if (element != null && element.Value.Equals("2"))
                    {
                        var query = from mag in result.Elements("product")
                            select mag;

                        foreach (var item in query)
                        {
                            var product = new NextWithDiscountProduct();
                            product.ProductName = item.Attribute("name_startwith").Value;
                            var dict = new Dictionary<int, float>();
                            foreach (var countDiscount in item.Elements("item"))
                            {
                                dict.Add(int.Parse(countDiscount.Attribute("count").Value),
                                    float.Parse(countDiscount.Attribute("discount").Value));
                            }
                            product.CountDiscount = dict;
                            returnVal.Add(product);
                        }
                    }
                }
            }

            return returnVal;
        }
    }
}
