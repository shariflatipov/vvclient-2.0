﻿using System.Collections.Generic;

namespace vvclient.actions.dao
{
    public class NextWithDiscountProduct
    {
        /*
         <?xml version="1.0" encoding="utf-8"?>
        <magazin>
            <description>OK</description>
            <status>2</status>
            <promo>
                <product name_startwith="asd884447">
                    <item count="1" discount="25.36"/>
                    <item count="2" discount="46.25"/>
                    <item count="3" discount="60.25"/>
                    <item count="4" discount="80.25"/>
                </product>
                <product name_startwith="asd33447">
                    <item count="1" discount="25.36"/>
                    <item count="2" discount="46.25"/>
                    <item count="3" discount="60.25"/>
                </product>
                <product name_startwith="asd83324447">
                    <item count="1" discount="25.36"/>
                    <item count="2" discount="46.25"/>
                    <item count="3" discount="60.25"/>
                </product>
            </promo>
        </magazin>
         */

        public string ProductName { get; set; }
        public Dictionary<int, float> CountDiscount { get; set; }
    }
}
