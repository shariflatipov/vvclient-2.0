﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using vvclient.product;
using vvclient.settings;

namespace vvclient.actions
{
    public class CalculateActionDiscount
    {
        // public List<IActionTypes>
        public static float GetActionDiscount(string productName, float quantityIn)
        {
            foreach (var item in MainForm.Actions)
            {
                if (productName == item.ProductName) {

                    bool first = true;
                    foreach(var quantity in item.CountDiscount.OrderByDescending(key => key.Key))
                    {
                        if (first)
                        {
                            if (quantity.Key <= quantityIn)
                            {
                                return quantity.Value;
                            }
                            first = false;
                        }
                        if (quantity.Key == quantityIn)
                        {
                            return quantity.Value;
                        }
                    }
                }
            }
            return 0;
        }

        public static void GetActionDiscount(DataGridView dgv)
        {
            Dictionary<string, float> actions = new Dictionary<string, float>();

            foreach (DataGridViewRow row in dgv.Rows)
            {
                string prodName = row.Cells["GoodsName"].Value.ToString();

                string dict = IsInActionList(prodName);

                if (dict != null)
                {
                    if (actions.ContainsKey(dict))
                    {
                        actions[dict] += float.Parse(row.Cells["Quantity"].Value.ToString());
                    }
                    else
                    {
                        actions[dict] = float.Parse(row.Cells["Quantity"].Value.ToString());
                    }
                }
            }

            var discountPercents = GetProductDiscount(actions);
            UpdateDatagridViewDiscount(dgv, discountPercents);


        }

        private static string IsInActionList(string name)
        {
            foreach (var data in MainForm.Actions)
            {
                if (name.StartsWith(data.ProductName))
                {
                    return data.ProductName; 
                }
            }

            return null;
        }

        private static Dictionary<string, float> GetProductDiscount(Dictionary<string, float> productsQuantity)
        {
            var result = new Dictionary<string, float>();
            foreach(var prod in productsQuantity)
            {
                result.Add(prod.Key, GetActionDiscount(prod.Key, prod.Value));
            }
            return result;
        }

        private static void UpdateDatagridViewDiscount(DataGridView dgv, Dictionary<string, float> actions)
        {
            foreach(DataGridViewRow row in dgv.Rows)
            {
                foreach(var action in actions)
                {
                    if (row.Cells["GoodsName"].Value.ToString().StartsWith(action.Key))
                    {
                        var product = Product.GetProduct(row.Cells["Id"].Value.ToString(), row.Cells["Batch"].Value.ToString());
                        if (product != null)
                        {
                            row.Cells["Discount"].Value = product.GetDiscount() + action.Value;
                            var ttl = (Convert.ToDouble(row.Cells["Price"].Value)) *
                                      (Convert.ToDouble(row.Cells["Quantity"].Value));
                            ttl -= (ttl*(Convert.ToDouble(row.Cells["Discount"].Value))/100);
                            row.Cells["Total"].Value = Math.Round(ttl, Config.RoundPreciesion);
                            break;
                        }
                    }
                }
            }
        }
    }
}
