﻿using vvclient.product;

namespace vvclient.actions
{
    public interface IAction
    {
        double GetActionDiscount(Product product);
    }
}
