﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using vvclient.broker;
using vvclient.discount;
using vvclient.nums;
using vvclient.payments;
using vvclient.peripheral;
using vvclient.product;
using vvclient.settings;
using vvclient.utils;

namespace vvclient {
    public partial class ChangeBack : Form {
        private readonly List<Product> _orderedList;
        private readonly double _total;
        private readonly IVfdDisplays _vfdDisplay;
        private Discount _discount;
        private IDiscountCalculator _discountCalculator = new DiscountZeroCalculator();

        public ChangeBack(double total, DataGridView dgv, IVfdDisplays vfdDisplay, Discount discount) {
            InitializeComponent();

            _discount = discount;
            _orderedList = Product.GetOrderedList(dgv);
            _total = total;
            _vfdDisplay = vfdDisplay;

            lblToPay.Text = _total.ToString();
            lblFinal.Text = _total.ToString();

            if (vfdDisplay.IsNumeric) {
                vfdDisplay.Write(lblToPay.Text);
            } else {
                vfdDisplay.Write(lblToPay.Text.PadRight(20));
            }

            HideBackButton();

            if (_discount == null) {
                _discount = new Discount();
            } else {
                _flagDiscount = true;
                lblDiscount.Text = ((int)_discount.Percents).ToString();
                lblDiscount_TextChanged(null, null);
            }
        }

        private void HideBackButton() {
            cancel.Visible = !Config.HideTotals;
            btnDebt.Visible = Config.BtnShowDebt;
            btnDiscount.Visible = Config.BtnShowDiscount;
            btnDiscountFixed.Visible = Config.BtnShowDiscountFixed;
            btnDiscountPercent.Visible = Config.BtnShowDiscountPercent;
            btnCreditCard.Visible = Config.BtnShowVisa;
            btnDiscountAuto.Visible = Config.BtnAutoDiscountSell;
        }

        private void ShowCurrencies() {
            label8.Text = Currency.GetCurrencies();
        }

        private void ChangeBack_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.F1) {
                ConfirmClick(null, null);
            } else {
                var key = Utils.KeyToChar(e.KeyCode.ToString());

                switch (key) {
                    case "dec":
                        BtnDotClick(sender, e);
                        break;
                    case "esc":
                        CancelClick(sender, e);
                        break;
                    case "clr":
                        ClearClick(sender, e);
                        break;
                    case "enter":
                        ConfirmClick(sender, e);
                        break;
                    case "back":
                        BackClick(sender, e);
                        break;
                    default:
                        if (_flagFixed || _flagPercent) {
                            lblDiscount.Text += key;
                        } else if (_flagAutoDiscount) {
                            lblAcceptedMoney.Text += key;
                            double tmpSum = double.Parse(lblAcceptedMoney.Text);
                            double toPay = double.Parse(lblToPay.Text);
                            if (toPay >= tmpSum) {
                                lblDiscount.Text = (toPay - tmpSum).ToString();
                            }
                        } else {
                            lblAcceptedMoney.Text += key;
                        }
                        break;
                }
            }
        }

        private void ProcessPayment(PaymentType paymentType) {
            bool printCheck = true;

            if (paymentType == PaymentType.debt) {
                if (_discount == null || string.IsNullOrWhiteSpace(_discount.DiscountCode)) {
                    MessageBox.Show("Нужно выбрать пользователя для продажи в долг");
                    return;
                }
            }

            if (Config.ShowPrintCheckDialog)
            {
                var dialog = MessageBox.Show("Распечатать чек?", "Распечатка чека", MessageBoxButtons.YesNo);

                if (dialog == DialogResult.No)
                {
                    printCheck = false;
                }
            }

            try {
                double discount;
                double.TryParse(lblDiscount.Text, out discount);

                var totalDiscount = Math.Round(_discountCalculator.calculate(discount, _orderedList), Config.RoundPreciesion);
                _discount.ChangeSum((_total - totalDiscount));

                var transaction = new Transaction(_orderedList);
                var trnId = transaction.SaveSoldProduct(_total, totalDiscount, _discount.DiscountCode, paymentType);

                _vfdDisplay.Write("                    ");
                _vfdDisplay.Write("Total :" + lblFinal.Text.PadLeft(13));

                if (printCheck)
                {
                    try {
                        var printer = new PrintChk(lblAcceptedMoney.Text, lblChange.Text, lblFinal.Text,
                            lblDiscountTotal.Text, _orderedList, lblToPay.Text, trnId, _discount, paymentType);
                        printer.Print();

                        if (Config.PrintCheckHeaders) {
                            var copy = new PrintChk(lblAcceptedMoney.Text, lblChange.Text, lblFinal.Text,
                                lblDiscountTotal.Text, _orderedList, lblToPay.Text, trnId, _discount, paymentType, false);
                            copy.Print();
                        }
                    } catch (Exception exc) {
                        Logger.Log(exc, "Cannot print check", Level.Info);
                    }
                }

                var thread = new Thread(MessageSender.Send);
                thread.Start("4~" + lblAcceptedMoney.Text + "~" + lblDiscountTotal.Text + "~" + lblToPay.Text);
                DialogResult = DialogResult.OK;
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Критическая ошибка: Обратитесь к администратору", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Logger.Log(ex, "ConfirmClick => ChangeBack.cs => ErrorWhileCalculatingDiscount", Level.Fatal);
            }
        }

        private void btnDebt_Click(object sender, EventArgs e) {
            ProcessPayment(PaymentType.debt);
        }

        private void btnCreditCard_Click(object sender, EventArgs e) {
            ProcessPayment(PaymentType.card);
        }

        private void ConfirmClick(object sender, EventArgs e) {
            ProcessPayment(PaymentType.cash);
        }

        private void label8_DoubleClick(object sender, EventArgs e) {
            ShowCurrencies();
        }

        private void btnSplitPay_Click(object sender, EventArgs e) {
            var amountSplitted = new Nums("Сумма наличными");
            if (amountSplitted.ShowDialog() == DialogResult.OK) {
                MessageBox.Show(amountSplitted.GetQuantity().ToString());
            }
            label1.Focus();
        }

        #region Buttons click event handling region

        private void CancelClick(object sender, EventArgs e) {
            Dispose();
        }


        /// <summary>
        ///     Number button click event.
        /// </summary>
        private void BtnClick(object sender, EventArgs e) {
            var num = (Button)sender;
            if (_flagFixed || _flagPercent) {
                lblDiscount.Text += num.Name.Substring(4);
            } else if (_flagAutoDiscount) {
                lblAcceptedMoney.Text += num.Name.Substring(4);
                double tmpSum = double.Parse(lblAcceptedMoney.Text);
                double toPay = double.Parse(lblToPay.Text);

                if (toPay >= tmpSum) {
                    lblDiscount.Text = (toPay - tmpSum).ToString();
                }
            } else {
                lblAcceptedMoney.Text += num.Name.Substring(4);
            }
            label1.Focus();
        }

        /// <summary>
        ///     Button Clear click event.
        /// </summary>
        private void ClearClick(object sender, EventArgs e) {
            if (_flagFixed || _flagPercent) {
                lblDiscount.Text = "";
            } else if (_flagAutoDiscount) {
                lblAcceptedMoney.Text = "";
                lblDiscount.Text = "";
            } else {
                lblAcceptedMoney.Text = "";
            }
            label1.Focus();
        }

        /// <summary>
        ///     Button back click event.
        /// </summary>
        private void BackClick(object sender, EventArgs e) {
            Label holder = null;

            if (_flagPercent || _flagFixed) {
                holder = lblDiscount;
            } else {
                holder = lblAcceptedMoney;
            }

            if (holder.Text.Length >= 1) {
                holder.Text = holder.Text.Substring(0, holder.Text.Length - 1);
            }

            if (_flagAutoDiscount) {
                double tmpSum = double.Parse(lblAcceptedMoney.Text);
                double toPay = double.Parse(lblToPay.Text);
                if (toPay >= tmpSum) {
                    lblDiscount.Text = (toPay - tmpSum).ToString();
                }
            }

            label1.Focus();
        }

        private void BtnDotClick(object sender, EventArgs e) {
            Label holder = null;
            if (_flagFixed || _flagPercent) {
                holder = lblDiscount;
            } else {
                holder = lblAcceptedMoney;
            }

            if (!holder.Text.Contains('.')) {
                holder.Text += ".";
            }
            label1.Focus();
        }

        /// <summary>
        ///     Lable count text changed.
        /// </summary>
        private void LblCountTextChanged(object sender, EventArgs e) {
            // lblDiscount.Text = 
            Btn_Confirm_Validation();
        }

        /// <summary>
        ///     Enabling or disabling btnConfirm
        /// </summary>
        private void Btn_Confirm_Validation() {
            try {
                double payed = 0;
                double discount = 0;

                if (!string.IsNullOrWhiteSpace(lblDiscountTotal.Text)) {
                    discount = Convert.ToDouble(lblDiscountTotal.Text);
                }

                if (lblAcceptedMoney.Text != "") {
                    payed = Convert.ToDouble(lblAcceptedMoney.Text);
                    lblChange.Text = Math.Round(((payed + discount) - _total), Config.RoundPreciesion).ToString();
                    confirm.Visible = Convert.ToDouble(lblAcceptedMoney.Text) + discount >= _total;
                } else {
                    lblChange.Text = "";
                }
            } catch (Exception ex) {
                Logger.Log(ex, "ChangeBack.cs : function => Btn_Confirm_Validation", Level.Warining);
                confirm.Visible = false;
            }
        }

        #endregion

        #region Discount

        private bool _flagPercent;
        private bool _flagFixed;
        private bool _flagDiscount;
        private bool _flagAutoDiscount;

        private void btnDiscountTypeIndicator_Click(object sender, EventArgs e) {
            if (sender.Equals(btnDiscountFixed)) {
                if (!_flagFixed) {
                    _flagFixed = true;
                    btnDiscountFixed.BackColor = Color.Yellow;

                    _flagPercent = _flagAutoDiscount = false;
                    btnDiscountAuto.BackColor = btnDiscountPercent.BackColor = Color.White;
                } else {
                    _flagFixed = false;
                    btnDiscountFixed.BackColor = Color.White;
                }
            } else if (sender.Equals(btnDiscountPercent)) {
                if (!_flagPercent) {
                    _flagPercent = true;
                    btnDiscountPercent.BackColor = Color.Yellow;

                    _flagFixed = _flagAutoDiscount = false;
                    btnDiscountAuto.BackColor = btnDiscountFixed.BackColor = Color.White;
                } else {
                    _flagPercent = false;
                    btnDiscountPercent.BackColor = Color.White;
                }
            } else if (sender.Equals(btnDiscountAuto)) {
                if (!_flagAutoDiscount) {
                    _flagAutoDiscount = true;
                    btnDiscountAuto.BackColor = Color.Yellow;

                    _flagPercent = _flagFixed = false;
                    btnDiscountFixed.BackColor = btnDiscountPercent.BackColor = Color.White;
                } else {
                    _flagAutoDiscount = false;
                    btnDiscountAuto.BackColor = Color.White;
                }
            }
            label1.Focus();
        }

        private void lblDiscount_TextChanged(object sender, EventArgs e) {
            try {
                double discount = 0;
                double.TryParse(lblDiscount.Text, out discount);

                if (discount == 0) {
                    _discountCalculator = new DiscountZeroCalculator();
                    lblDiscountTotal.Text = "0";
                    return;
                }

                double toPay = 0;
                double.TryParse(lblToPay.Text, out toPay);

                if (_flagFixed || _flagAutoDiscount) {
                    if (toPay > discount) {
                        lblDiscountTotal.Text = discount.ToString();
                        _discountCalculator = new DiscountFixedCalcualtor();
                    } else {
                        throw new Exception("Сумма скидки не может превышать сумму покупки");
                    }
                } else if (_flagPercent || _flagDiscount) {
                    if (discount <= 100) {
                        _discountCalculator = new DiscountPersentCalculator();
                        lblDiscountTotal.Text = (Math.Round(toPay * discount / 100, Config.RoundPreciesion)).ToString();
                    } else {
                        throw new Exception("Скидка не может превышать 100%");
                    }
                }
            } catch (Exception ex) {
                lblDiscount.Text = "";
                lblDiscountTotal.Text = "";
                _discountCalculator = new DiscountZeroCalculator();
                MessageBox.Show(this, ex.Message, "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblDiscountTotal_TextChanged(object sender, EventArgs e) {
            try {
                double toPay = 0;
                double accepted = 0;
                double discount = 0;

                double.TryParse(lblToPay.Text, out toPay);
                double.TryParse(lblAcceptedMoney.Text, out accepted);
                double.TryParse(lblDiscountTotal.Text, out discount);

                lblChange.Text = ((accepted + discount) - toPay).ToString();
                lblFinal.Text = (toPay - discount).ToString();
                Btn_Confirm_Validation();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDiscount_Click(object sender, EventArgs e) {
            using (var cd = new CalculateDiscount(lblNetError)) {
                if (cd.ShowDialog() == DialogResult.OK) {
                    _flagDiscount = true;
                    _discount = cd.discount;
                    if (!String.IsNullOrEmpty(_discount.Coupon)) {
                        AskCouponCode ask = new AskCouponCode(_discount.Coupon);
                        if (DialogResult.OK == ask.ShowDialog()) {
                            if (ask.result) {
                                lblDiscount.Text = ((int)_discount.NewPercent).ToString();
                            } else {
                                lblDiscount.Text = ((int)_discount.Percents).ToString();
                                lblNetError.Text = "Код не совпадает";
                            }
                        }
                    } else {
                        lblDiscount.Text = ((int)_discount.Percents).ToString();
                    }
                } else {
                    if (cd.discount != null) {
                        _flagDiscount = true;
                        _discount = cd.discount;
                    }
                }
            }
            label1.Focus();
        }

        #endregion


        private void btnInfo_Click_1(object sender, EventArgs e) {
            ClientInfo info = new ClientInfo(_discount);
            info.ShowDialog();
        }
    }
}