﻿using System;
using System.Windows.Forms;
using Gecko;

namespace vvclient.ff
{
    public partial class MainBrowser : Form
    {
        private readonly GeckoWebBrowser _browser = new GeckoWebBrowser {Dock = DockStyle.Fill};

        public MainBrowser()
        {
            InitializeComponent();
            Xpcom.Initialize(@"xul");
            GeckoPreferences.User["extensions.blocklist.enabled"] = false;

            _browser.DomClick += DomClick;
            _browser.DocumentCompleted += BrowserDocumentCompleted;

            Controls.Add(_browser);
            _browser.Navigate("http://" + MainForm.Ip);
        }

        private void BrowserDocumentCompleted(object sender, EventArgs e)
        {
        }

        private void DomClick(object sender, DomEventArgs e)
        {
        }
    }
}
