﻿using System;
using System.Windows.Forms;
using vvclient.product;

namespace vvclient.settings
{
    public partial class SearchByProductName : Form
    {
        private ProductCart _cart = null;

        public SearchByProductName(ProductCart cart)
        {
            InitializeComponent();
            _cart = cart;
        }

        private void AddProduct_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                DataGridViewRow row = dataGridView1.SelectedRows[0];

                var selectedProduct = Product.SetProductById(
                    row.Cells["Barcode"].Value.ToString(),
                    row.Cells["ProdBatch"].Value.ToString(),
                    true);
                _cart.AddItem(selectedProduct, true);
            }
            else
            {
                MessageBox.Show("Выберите строку");
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(textBox1.Text))
            {
                dataGridView1.Rows.Clear();

                var lst = Product.GetProducts(textBox1.Text, true);
                foreach (Product prod in lst)
                {
                    dataGridView1.Rows.Add(
                        prod.GetId(),
                        prod.GetName(),
                        prod.GetArticle(),
                        prod.GetColor(),
                        prod.GetSize(),
                        prod.GetBatch(),
                        prod.GetDiscount(),
                        prod.GetPrice()
                    );
                }
            }
        }
    }
}
