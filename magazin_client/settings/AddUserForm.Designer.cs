﻿namespace vvclient.settings
{
    partial class AddUserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateUser = new System.Windows.Forms.Button();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtPatronimyc = new System.Windows.Forms.TextBox();
            this.dtpBirthdate = new System.Windows.Forms.DateTimePicker();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.txtConfirmPass = new System.Windows.Forms.TextBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblPatronimyc = new System.Windows.Forms.Label();
            this.lblBirhtdate = new System.Windows.Forms.Label();
            this.lblLogin = new System.Windows.Forms.Label();
            this.lblPass = new System.Windows.Forms.Label();
            this.lblConfirmPass = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnEnter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCreateUser
            // 
            this.btnCreateUser.Location = new System.Drawing.Point(324, 367);
            this.btnCreateUser.Name = "btnCreateUser";
            this.btnCreateUser.Size = new System.Drawing.Size(120, 35);
            this.btnCreateUser.TabIndex = 0;
            this.btnCreateUser.Text = "Сохранить";
            this.btnCreateUser.UseVisualStyleBackColor = true;
            this.btnCreateUser.Click += new System.EventHandler(this.ConfirmAddUser);
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(241, 20);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(200, 20);
            this.txtLastName.TabIndex = 3;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(241, 70);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(200, 20);
            this.txtFirstName.TabIndex = 4;
            // 
            // txtPatronimyc
            // 
            this.txtPatronimyc.Location = new System.Drawing.Point(241, 121);
            this.txtPatronimyc.Name = "txtPatronimyc";
            this.txtPatronimyc.Size = new System.Drawing.Size(200, 20);
            this.txtPatronimyc.TabIndex = 5;
            // 
            // dtpBirthdate
            // 
            this.dtpBirthdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBirthdate.Location = new System.Drawing.Point(241, 171);
            this.dtpBirthdate.Name = "dtpBirthdate";
            this.dtpBirthdate.Size = new System.Drawing.Size(200, 20);
            this.dtpBirthdate.TabIndex = 6;
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(241, 210);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(200, 20);
            this.txtLogin.TabIndex = 7;
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(241, 258);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(200, 20);
            this.txtPass.TabIndex = 8;
            // 
            // txtConfirmPass
            // 
            this.txtConfirmPass.Location = new System.Drawing.Point(241, 305);
            this.txtConfirmPass.Name = "txtConfirmPass";
            this.txtConfirmPass.PasswordChar = '*';
            this.txtConfirmPass.Size = new System.Drawing.Size(200, 20);
            this.txtConfirmPass.TabIndex = 9;
            // 
            // lblLastName
            // 
            this.lblLastName.Location = new System.Drawing.Point(12, 23);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(170, 23);
            this.lblLastName.TabIndex = 10;
            this.lblLastName.Text = "Введите фамилию";
            // 
            // lblFirstName
            // 
            this.lblFirstName.Location = new System.Drawing.Point(12, 73);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(170, 23);
            this.lblFirstName.TabIndex = 11;
            this.lblFirstName.Text = "Введите имя";
            // 
            // lblPatronimyc
            // 
            this.lblPatronimyc.Location = new System.Drawing.Point(12, 124);
            this.lblPatronimyc.Name = "lblPatronimyc";
            this.lblPatronimyc.Size = new System.Drawing.Size(170, 23);
            this.lblPatronimyc.TabIndex = 12;
            this.lblPatronimyc.Text = "Введите отчество";
            // 
            // lblBirhtdate
            // 
            this.lblBirhtdate.Location = new System.Drawing.Point(12, 177);
            this.lblBirhtdate.Name = "lblBirhtdate";
            this.lblBirhtdate.Size = new System.Drawing.Size(170, 23);
            this.lblBirhtdate.TabIndex = 13;
            this.lblBirhtdate.Text = "Выберите дату рождения";
            // 
            // lblLogin
            // 
            this.lblLogin.Location = new System.Drawing.Point(12, 213);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(170, 23);
            this.lblLogin.TabIndex = 14;
            this.lblLogin.Text = "Введите логин";
            // 
            // lblPass
            // 
            this.lblPass.Location = new System.Drawing.Point(12, 261);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(170, 23);
            this.lblPass.TabIndex = 15;
            this.lblPass.Text = "Введите пароль";
            // 
            // lblConfirmPass
            // 
            this.lblConfirmPass.Location = new System.Drawing.Point(12, 308);
            this.lblConfirmPass.Name = "lblConfirmPass";
            this.lblConfirmPass.Size = new System.Drawing.Size(170, 23);
            this.lblConfirmPass.TabIndex = 16;
            this.lblConfirmPass.Text = "Подтвердите пароль";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(15, 367);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(120, 35);
            this.btnExit.TabIndex = 17;
            this.btnExit.Text = "Выйти";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.BtnExitClick);
            // 
            // btnEnter
            // 
            this.btnEnter.Location = new System.Drawing.Point(171, 367);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(120, 35);
            this.btnEnter.TabIndex = 18;
            this.btnEnter.Text = "Удалить";
            this.btnEnter.UseVisualStyleBackColor = true;
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // AddUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 428);
            this.Controls.Add(this.btnEnter);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblConfirmPass);
            this.Controls.Add(this.lblPass);
            this.Controls.Add(this.lblLogin);
            this.Controls.Add(this.lblBirhtdate);
            this.Controls.Add(this.lblPatronimyc);
            this.Controls.Add(this.lblFirstName);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.txtConfirmPass);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.txtLogin);
            this.Controls.Add(this.dtpBirthdate);
            this.Controls.Add(this.txtPatronimyc);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.btnCreateUser);
            this.Name = "AddUserForm";
            this.Text = "AddUserForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreateUser;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtPatronimyc;
        private System.Windows.Forms.DateTimePicker dtpBirthdate;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.TextBox txtConfirmPass;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblPatronimyc;
        private System.Windows.Forms.Label lblBirhtdate;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Label lblConfirmPass;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnEnter;
    }
}