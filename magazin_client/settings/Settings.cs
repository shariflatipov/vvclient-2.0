﻿using System.Collections.Generic;
using System.Data.SQLite;
using vvclient.utils;

namespace vvclient.settings
{
    public class Settings
    {
        public List<string> GetSettingStrings()
        {
            var result = new List<string>();
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand("SELECT name_unit FROM unit", conn))
                {
                    conn.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(reader[0].ToString());
                        }
                    }
                }
            }
            return result;
        }

        public int UpdateValue(string settingName, string settingValue)
        {
            var query = "update unit set status = '" + settingValue +
                        "' where name_unit='" + settingName + "'";
            var count = int.Parse(SQLiteWrapper.ExecuteNonQuery(query).ToString());
            return count;
        }

        public static string GetValue(string param)
        {
            var result = "";

            using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var command = new SQLiteCommand(connection))
                {
                    command.CommandText = "select status from unit where name_unit = @param";
                    command.Parameters.Add(new SQLiteParameter("@param", param));
                    connection.Open();
                    result = (string) command.ExecuteScalar();
                }
            }
            return result;
        }
    }
}