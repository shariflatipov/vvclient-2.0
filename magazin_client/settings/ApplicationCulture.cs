﻿using System.Globalization;

namespace vvclient.settings
{
    public static class ApplicationCulture
    {
        private static CultureInfo _cultureInfo;

        public static CultureInfo GetCurrentCulture()
        {
            return _cultureInfo ?? (_cultureInfo = new CultureInfo("en-US"));
        }
    }
}