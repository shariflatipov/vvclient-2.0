﻿using System;
using System.Windows.Forms;

namespace vvclient.settings
{
    public partial class SettingForm : Form
    {
        private readonly Settings settings = new Settings();

        public SettingForm()
        {
            InitializeComponent();
            FillForm();
        }

        private void FillForm()
        {
            var list = settings.GetSettingStrings();
            foreach (var s in list)
            {
                cmbSettings.Items.Add(s);
            }
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void save_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Updated : " + settings.UpdateValue(cmbSettings.Text, textBox1.Text) + " rows");
        }
    }
}