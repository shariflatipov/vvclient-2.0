﻿namespace vvclient.settings
{
    partial class DatabaseOperations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClearTransactions = new System.Windows.Forms.Button();
            this.btnResetCounter = new System.Windows.Forms.Button();
            this.btnUsers = new System.Windows.Forms.Button();
            this.btnClearDiscount = new System.Windows.Forms.Button();
            this.btnClearProducts = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnClearTransactions
            // 
            this.btnClearTransactions.Location = new System.Drawing.Point(13, 13);
            this.btnClearTransactions.Name = "btnClearTransactions";
            this.btnClearTransactions.Size = new System.Drawing.Size(150, 62);
            this.btnClearTransactions.TabIndex = 0;
            this.btnClearTransactions.Text = "Очистить продажи";
            this.btnClearTransactions.UseVisualStyleBackColor = true;
            this.btnClearTransactions.Click += new System.EventHandler(this.btnClearTransactions_Click);
            // 
            // btnResetCounter
            // 
            this.btnResetCounter.Location = new System.Drawing.Point(169, 13);
            this.btnResetCounter.Name = "btnResetCounter";
            this.btnResetCounter.Size = new System.Drawing.Size(150, 62);
            this.btnResetCounter.TabIndex = 1;
            this.btnResetCounter.Text = "Переустановить значение счетчика";
            this.btnResetCounter.UseVisualStyleBackColor = true;
            this.btnResetCounter.Click += new System.EventHandler(this.btnResetCounter_Click);
            // 
            // btnUsers
            // 
            this.btnUsers.Location = new System.Drawing.Point(325, 13);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(150, 62);
            this.btnUsers.TabIndex = 2;
            this.btnUsers.Text = "Пользователи";
            this.btnUsers.UseVisualStyleBackColor = true;
            this.btnUsers.Click += new System.EventHandler(this.btnUsers_Click);
            // 
            // btnClearDiscount
            // 
            this.btnClearDiscount.Location = new System.Drawing.Point(13, 81);
            this.btnClearDiscount.Name = "btnClearDiscount";
            this.btnClearDiscount.Size = new System.Drawing.Size(150, 62);
            this.btnClearDiscount.TabIndex = 3;
            this.btnClearDiscount.Text = "Очистка держателей дисконтных карт";
            this.btnClearDiscount.UseVisualStyleBackColor = true;
            this.btnClearDiscount.Click += new System.EventHandler(this.btnClearDiscount_Click);
            // 
            // btnClearProducts
            // 
            this.btnClearProducts.BackColor = System.Drawing.Color.OrangeRed;
            this.btnClearProducts.ForeColor = System.Drawing.Color.White;
            this.btnClearProducts.Location = new System.Drawing.Point(169, 81);
            this.btnClearProducts.Name = "btnClearProducts";
            this.btnClearProducts.Size = new System.Drawing.Size(150, 62);
            this.btnClearProducts.TabIndex = 4;
            this.btnClearProducts.Text = "Удалить все товары!!";
            this.btnClearProducts.UseVisualStyleBackColor = false;
            this.btnClearProducts.Click += new System.EventHandler(this.btnClearProducts_Click);
            // 
            // DatabaseOperations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 420);
            this.Controls.Add(this.btnClearProducts);
            this.Controls.Add(this.btnClearDiscount);
            this.Controls.Add(this.btnUsers);
            this.Controls.Add(this.btnResetCounter);
            this.Controls.Add(this.btnClearTransactions);
            this.Name = "DatabaseOperations";
            this.Text = "DatabaseOperations";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClearTransactions;
        private System.Windows.Forms.Button btnResetCounter;
        private System.Windows.Forms.Button btnUsers;
        private System.Windows.Forms.Button btnClearDiscount;
        private System.Windows.Forms.Button btnClearProducts;
    }
}