﻿using System;
using System.Windows.Forms;

namespace vvclient.settings
{
    public partial class AddUserForm : Form
    {
        private readonly bool toUpdate;
        private User user;

        public AddUserForm(User user)
        {
            InitializeComponent();
            this.user = user;
            txtFirstName.Text = user.Name;
            txtLastName.Text = user.LastName;
            txtPatronimyc.Text = user.Patronymic;
            dtpBirthdate.Text = user.Birthdate.ToString();
            txtLogin.Text = user.Login;
            toUpdate = true;
            btnCreateUser.Text = "Обновить";
        }

        public AddUserForm()
        {
            InitializeComponent();
            btnEnter.Visible = false;
        }

        private void ConfirmAddUser(object sender, EventArgs e)
        {
            if (txtFirstName.TextLength > 2 && txtLastName.TextLength > 2 && txtLogin.TextLength > 2)
            {
                if (txtPass.Text == txtConfirmPass.Text)
                {
                    try
                    {
                        if (toUpdate)
                        {
                            user.Name = txtFirstName.Text;
                            user.LastName = txtLastName.Text;
                            user.Patronymic = txtPatronimyc.Text;
                            user.Birthdate = dtpBirthdate.Value;
                            user.Login = txtLogin.Text;
                            user.Password = txtPass.Text;
                            if (user.UpdateUser() >= 1)
                            {
                                MessageBox.Show("Пользователь успешно обновлен!");
                                Dispose();
                            }
                        }
                        else
                        {
                            if (User.isLoginExists(txtLogin.Text) < 1)
                            {
                                user = new User(txtFirstName.Text, txtLastName.Text,
                                    txtPatronimyc.Text, dtpBirthdate.Value, txtLogin.Text, txtPass.Text,
                                    1);
                                if (user.SaveUser() >= 1)
                                {
                                    MessageBox.Show("Пользователь успешно добавлен", "Success", MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                                    Dispose();
                                }
                                else
                                {
                                    MessageBox.Show("Невозможно создать пользователя");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Error: Данный логин существует!", "Incorrect password",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Error: Обратитесь к администратору!", "Incorrect password",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Пароли не совпадают!", "Incorrect password", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля!", "Incorrect password", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnExitClick(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            if (user != null)
            {
                if (user.DeleteUser() >= 1)
                {
                    MessageBox.Show("Пользователь успешно удален!");
                    Dispose();
                }
            }
        }
    }
}