﻿using System;
using System.Windows.Forms;
using vvclient.utils;

namespace vvclient.settings
{
    public partial class DatabaseOperations : Form
    {
        public DatabaseOperations()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Deletes all transactions from database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearTransactions_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(string.Format("Deleted rows from table sold_product: {0}",
                    SQLiteWrapper.ExecuteNonQuery("delete from sold_product")));
                MessageBox.Show(string.Format("Deleted rows from table transactions: {0}",
                    SQLiteWrapper.ExecuteNonQuery("delete from transactions")));
            }
            catch (Exception ex)
            {
                Logger.Log(ex,
                    "Can\'t to clear tables sold_product and transactions");
            }
        }

        /// <summary>
        ///     Resets database sold_product's autoincrement counter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnResetCounter_Click(object sender, EventArgs e)
        {
            var rcv = new ResetCounterValues();
            rcv.ShowDialog();
        }

        private void btnUsers_Click(object sender, EventArgs e)
        {
            var users = new FrmUsers();
            users.ShowDialog();
        }

        private void btnClearDiscount_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == MessageBox.Show("Вы действетельно хотите удалить держателей дисконтный карт", 
                "Удаление дисконтов", MessageBoxButtons.OKCancel))
            {
                discount.Discount.DeleteAll();
            }
        }

        private void btnClearProducts_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == MessageBox.Show("Вы действетельно хотите удалить все товары",
                "Удаление товаров", MessageBoxButtons.OKCancel))
            {
                if (DialogResult.OK == MessageBox.Show("Данная операция необратима, вы уверенны?",
                "Удаление товаров", MessageBoxButtons.OKCancel))
                {
                    if (product.Product.DeleteAll())
                    {
                        MessageBox.Show("Все данные в таблице product удалены");
                    }
                }
            }
        }
    }
}