﻿using System;
using System.Configuration;

namespace vvclient.settings
{
    public static class Config
    {
        public static string VERSION = "1.4.0";
        // Printer receipt fields
        public static string PrinterPackage {
            get {
                try
                {
                    return ConfigurationManager.AppSettings["printerPackage"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static int RoundPreciesion {
            get {
                try
                {
                    return Int32.Parse(ConfigurationManager.AppSettings["roundPreciesion"]);
                }
                catch (Exception)
                {
                    return 2;
                }
            }
        }

        public static int CheckCopies {
            get {
                try
                {
                    return Int32.Parse(ConfigurationManager.AppSettings["checkCopies"]);
                }
                catch (Exception)
                {
                    return 1;
                }
            }
        }

        public static string StoreName {
            get {
                try
                {
                    return ConfigurationManager.AppSettings["chkStoreName"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static string StoreAddress {
            get {
                try
                {
                    return ConfigurationManager.AppSettings["chkStoreAddress"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static string StoreINN {
            get {
                try
                {
                    return ConfigurationManager.AppSettings["chkStoreINN"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static string StoreSlogan {
            get {
                try
                {
                    return ConfigurationManager.AppSettings["chkStoreSlogan"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static string StorePhones {
            get {
                try
                {
                    return ConfigurationManager.AppSettings["chkStorePhones"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static bool HideTopBar {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["hideTopBar"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool HideColumnDiscount {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["hideColumnDiscount"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool HideColumnArticle {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["hideColumnArticle"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool HideColumnColor {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["hideColumnColor"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool HideColumnUnit {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["hideColumnUnit"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool HideColumnBatch {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["hideColumnBatch"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool HideTotals {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["hideTotals"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool BtnShowDiscountPercent {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["BtnShowDiscountPercent"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool BtnShowDiscountFixed {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["BtnShowDiscountFixed"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool BtnShowDebt {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["BtnShowDebt"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool BtnShowDiscount {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["BtnShowDiscount"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool BtnShowVisa {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["BtnShowVisa"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool BtnAutoDiscountSell {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["BtnAutoDiscountSell"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static string PrinterPort {
            get {
                try
                {
                    return ConfigurationManager.AppSettings["printerPort"];
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static bool ShowDailyReport {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["showDailyReport"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool AllowChangeDiscount {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["allowChangeDiscount"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool SendToRabbitMQ {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["sendToRabbitMQ"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool CanDeleteItems {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["canDeleteItems"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool LiveStockRemain {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["LiveStockRemain"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool WriteLogsToFile {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["WriteLogsToFile"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool ShowLogo {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["ShowLogo"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool AskBag {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["AskBag"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Печать заголовков чека(имеющие отношения к магазину) для второй копии
        /// </summary>
        public static bool PrintCheckHeaders {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["PrintCheckHeaders"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Показать галочку предоплата во время возврата
        /// </summary>
        public static bool ShowCheckboxPrepayment {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["ShowCheckboxPrepayment"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Показать диалоговое окно распечатки чека
        /// </summary>
        public static bool ShowPrintCheckDialog {
            get {
                try
                {
                    return bool.Parse(ConfigurationManager.AppSettings["ShowPrintCheckDialog"].ToLower());
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
