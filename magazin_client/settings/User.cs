﻿using System;
using System.Linq;
using System.Data;
using System.Data.SQLite;
using System.Xml.Linq;
using vvclient.utils;

namespace vvclient.settings
{
    public class User
    {
        public User()
        {
        }

        public User(string name, string lastName, string patronymic, DateTime birthdate, string login, string password,
            int state)
        {
            Name = name;
            LastName = lastName;
            Patronymic = patronymic;
            Birthdate = birthdate;
            Login = login;
            Password = password;
            State = state;
        }

        public User(string firstName, string lastName, string login, string password)
        {
            Name = firstName;
            LastName = lastName;
            Login = login;
            Password = password;
        }

        public long id { get; private set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public DateTime Birthdate { get; set; }

        public string Login { get; set; }
        public string Password { get; set; }
        public int State { get; set; }

        public static DataSet GetUsersList()
        {
            var ds = new DataSet();

            using (var conn = new SQLiteConnection())
            {
                conn.ConnectionString = SQLiteWrapper.ConnectionString;

                using (
                    var adapter =
                        new SQLiteDataAdapter("SELECT id_seller, last_name, first_name, patronymic, login FROM seller",
                            conn.ConnectionString))
                {
                    using (var builder = new SQLiteCommandBuilder(adapter))
                    {
                        adapter.Fill(ds);
                    }
                }
            }
            return ds;
        }

        public int UpdateUser()
        {
            var result = 0;
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand(conn))
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "UPDATE seller SET last_name=@lname, first_name=@fname, " +
                                      "patronymic=@patron, birthdate=@bdate, login=@login, pass=@pass, status=@state WHERE id_seller=@id";
                    cmd.Parameters.Add(new SQLiteParameter("@lname", LastName));
                    cmd.Parameters.Add(new SQLiteParameter("@fname", Name));
                    cmd.Parameters.Add(new SQLiteParameter("@patron", Patronymic));
                    cmd.Parameters.Add(new SQLiteParameter("@bdate", Birthdate));
                    cmd.Parameters.Add(new SQLiteParameter("@login", Login));
                    cmd.Parameters.Add(new SQLiteParameter("@pass", Password));
                    cmd.Parameters.Add(new SQLiteParameter("@state", State));
                    cmd.Parameters.Add(new SQLiteParameter("@id", id));
                    conn.Open();
                    result = cmd.ExecuteNonQuery();
                }
            }
            return result;
        }

        public static User GetUserById(long id)
        {
            User user = null;
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand(conn))
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT last_name, first_name, " +
                                      "patronymic, birthdate, login, pass, status FROM seller WHERE id_seller=@id";
                    cmd.Parameters.Add(new SQLiteParameter("@id", id));
                    conn.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            user = new User();
                            user.id = id;
                            user.LastName = reader[0].ToString();
                            user.Name = reader[1].ToString();
                            user.Patronymic = reader[2].ToString();
                            user.Birthdate = DateTime.Now;
                            user.Login = reader[4].ToString();
                            user.Password = reader[5].ToString();
                            user.State = int.Parse(reader[6].ToString());
                        }
                    }
                }
            }
            return user;
        }

        public int SaveUser()
        {
            var result = 0;
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand(conn))
                {
                    cmd.Connection = conn;
                    cmd.CommandText =
                        "INSERT INTO seller (last_name, first_name, patronymic, birthdate, login, pass, status) " +
                        "VALUES(@lname, @fname, @patron, @bdate, @login, @pass, @state)";
                    cmd.Parameters.Add(new SQLiteParameter("@lname", LastName));
                    cmd.Parameters.Add(new SQLiteParameter("@fname", Name));
                    cmd.Parameters.Add(new SQLiteParameter("@patron", Patronymic));
                    cmd.Parameters.Add(new SQLiteParameter("@bdate", Birthdate.ToString("yyyy-MM-dd")));
                    cmd.Parameters.Add(new SQLiteParameter("@login", Login));
                    cmd.Parameters.Add(new SQLiteParameter("@pass", Password));
                    cmd.Parameters.Add(new SQLiteParameter("@state", State));
                    conn.Open();
                    result = cmd.ExecuteNonQuery();
                }
            }
            return result;
        }

        public static int isLoginExists(string login)
        {
            int result;

            int.TryParse(
                SQLiteWrapper.ExecuteScalar("select count(*) from seller where login = '" + login + "'").ToString(),
                out result);

            return result;
        }

        public int DeleteUser()
        {
            var result = -1;
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand(conn))
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "DELETE FROM seller WHERE id_seller = @id";
                    cmd.Parameters.Add(new SQLiteParameter("@id", id));
                    conn.Open();
                    result = cmd.ExecuteNonQuery();
                }
            }
            return result;
        }


        public static bool HandleDataFromServer(XElement data)
        {
            bool result = false;
            var query = from mag in data.Elements("people")
                        select mag;

            try
            {
                foreach (var mg in query)
                {
                    User usr = new User(mg.Attribute("first_name").Value, mg.Attribute("last_name").Value,
                        mg.Attribute("patronymic").Value, DateTime.Now, mg.Attribute("login").Value,
                        mg.Attribute("password").Value, 1);
                    usr.SaveUser();
                }
                result = true;
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "Save user from server");
                return result;
            }
            return result;
        }
    }
}