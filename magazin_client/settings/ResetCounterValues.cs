﻿using System;
using System.Windows.Forms;
using vvclient.utils;

namespace vvclient.settings
{
    public partial class ResetCounterValues : Form
    {
        public ResetCounterValues()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int autoincrement;
                if (int.TryParse(txtValue.Text, out autoincrement))
                {
                    if (SQLiteWrapper.ExecuteNonQuery("UPDATE SQLITE_SEQUENCE SET seq = " +
                                                      autoincrement + " where name = 'sold_product'") == 1)
                    {
                        MessageBox.Show("Значение счетчика переустановленно");
                    }
                }
                else
                {
                    MessageBox.Show("Введите положительное числовое значение");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}