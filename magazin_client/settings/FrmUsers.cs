﻿using System;
using System.Windows.Forms;
using System.Xml.Linq;
using vvclient.utils;

namespace vvclient.settings
{
    public partial class FrmUsers : Form
    {
        public FrmUsers()
        {
            InitializeComponent();
        }

        private void FrmUsers_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = User.GetUsersList().Tables[0];
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var user = User.GetUserById(long.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString()));
            var auf = new AddUserForm(user);
            auf.ShowDialog();
            dataGridView1.DataSource = User.GetUsersList().Tables[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var auf = new AddUserForm();
            auf.ShowDialog();
            dataGridView1.DataSource = User.GetUsersList().Tables[0];
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            XMLHeaders header = new XMLHeaders();
            header.SetAct(22);
            XElement response = HttpCommunication.HttpCommun(header.GetRootElements(), 15000);
            User.HandleDataFromServer(response);
            dataGridView1.DataSource = User.GetUsersList().Tables[0];
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
    }
}