﻿using System.Data.SQLite;
using System.Xml.Linq;
using vvclient.product;
using vvclient.utils;

namespace vvclient.promotions
{
    public class BatchPromotion
    {

        public string Batch { get; set; }
        public float Discount { get; set; }

        public static void GetPromotions()
        {
            XMLHeaders headers = new XMLHeaders();
            headers.SetAct(15);

            var result = HttpCommunication.HttpCommun(headers.GetRootElements(), 60000);

            /* <?xml version="1.0" encoding="utf-8"?>
               <magazin>
                   <description>OK</description>
                   <status>2</status>
                   <action_on>1</action_on>
                   <batches>
                       <batch action_discount=10>NC</batch>
                       <batch>NEW</batch>
                       <batch>Hello</batch>
                   </batches>
               </magazin>
            */
            HandleReceivedPromotions(result);
        }

        public static void HandleReceivedPromotions(XElement data)
        {
            if (data != null)
            {
                if (data.Element("action_on").Value.Equals("1"))
                {
                    var batches = data.Elements("batches");
                    var batchCount = batches.Elements("batch");
                   

                    foreach (var batch in batches.Elements("batch"))
                    {
                        BatchPromotion bp = new BatchPromotion()
                        {
                            Batch = batch.Value,
                            Discount = float.Parse(batch.Attribute("action_discount").Value)
                        };
                        bp.Save();
                    }
                } else if (data.Element("action_on").Value.Equals("0"))
                {
                    DeleteAllBatches();
                }
            }
        }

        public static void DeleteAllBatches()
        {
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    string commandText = "delete from promotions";

                    conn.Open();
                    cmd.CommandText = commandText;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Save()
        {
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    string commandText = "select count(*) from promotions where batch=@batch";

                    conn.Open();
                    cmd.CommandText = commandText;
                    cmd.Parameters.Add(new SQLiteParameter("@batch", Batch));
                    if (int.Parse(cmd.ExecuteScalar().ToString()) == 0)
                    {
                        try
                        {
                            cmd.CommandText = "insert into promotions(batch, discount) values(@batch, @discount)";
                            cmd.Parameters.Add(new SQLiteParameter("@discount", Discount));
                            cmd.ExecuteNonQuery();
                        }
                        catch (SQLiteException sqle)
                        {

                        }
                    }
                    cmd.Parameters.Clear();
                }
            }

        }

        public double GetDiscountPercentForBatch(Product product)
        {
            float result = 0;

            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;

                    try
                    {
                        cmd.CommandText = "select discount from promotions where batch=@batch";
                        cmd.Parameters.Add(new SQLiteParameter("@batch", product.GetBatch()));
                        conn.Open();
                        object res = cmd.ExecuteScalar();

                        if (res != null)
                        {
                            float.TryParse(res.ToString(), out result);
                        }
                    }
                    catch (SQLiteException sqle)
                    {
                        Logger.Log(sqle, "batch= " + product.GetBatch());
                    }
                    finally
                    {
                        cmd.Parameters.Clear();
                    }
                }
            }
            if (result > 0)
            {
                double productPrice = product.GetPrice() - (product.GetPrice() * product.GetDiscount() / 100);
                double newProductPrice = productPrice * result / 100;
                double additionalDiscount = newProductPrice * 100 / product.GetPrice();
                return additionalDiscount;
            }
            return 0;
        }
    }
}
