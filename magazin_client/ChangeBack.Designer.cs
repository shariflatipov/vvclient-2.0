﻿namespace vvclient
{
    partial class ChangeBack
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_1 = new System.Windows.Forms.Button();
            this.btn_2 = new System.Windows.Forms.Button();
            this.btn_5 = new System.Windows.Forms.Button();
            this.btn_6 = new System.Windows.Forms.Button();
            this.btn_3 = new System.Windows.Forms.Button();
            this.btn_9 = new System.Windows.Forms.Button();
            this.btn_8 = new System.Windows.Forms.Button();
            this.btn_4 = new System.Windows.Forms.Button();
            this.btn_7 = new System.Windows.Forms.Button();
            this.lblAcceptedMoney = new System.Windows.Forms.Label();
            this.btn_0 = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.lblDiscountTotal = new System.Windows.Forms.Label();
            this.lblChange = new System.Windows.Forms.Label();
            this.btn_dot = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblToPay = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnDiscountPercent = new System.Windows.Forms.Button();
            this.btnDiscountFixed = new System.Windows.Forms.Button();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblFinal = new System.Windows.Forms.Label();
            this.btnDiscount = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnDebt = new System.Windows.Forms.Button();
            this.btnCreditCard = new System.Windows.Forms.Button();
            this.confirm = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.rootPanel = new System.Windows.Forms.TableLayoutPanel();
            this.operationPanel = new System.Windows.Forms.TableLayoutPanel();
            this.pnlTopLeft = new System.Windows.Forms.TableLayoutPanel();
            this.pnlTopRight = new System.Windows.Forms.TableLayoutPanel();
            this.pnlBottomLeft = new System.Windows.Forms.TableLayoutPanel();
            this.btnDiscountAuto = new System.Windows.Forms.Button();
            this.btnInfo = new System.Windows.Forms.Button();
            this.btnSplitPay = new System.Windows.Forms.Button();
            this.lblNetError = new System.Windows.Forms.Label();
            this.pnlBottomRight = new System.Windows.Forms.TableLayoutPanel();
            this.rootPanel.SuspendLayout();
            this.operationPanel.SuspendLayout();
            this.pnlTopLeft.SuspendLayout();
            this.pnlTopRight.SuspendLayout();
            this.pnlBottomLeft.SuspendLayout();
            this.pnlBottomRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_1
            // 
            this.btn_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btn_1.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_1.Location = new System.Drawing.Point(131, 3);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(104, 79);
            this.btn_1.TabIndex = 0;
            this.btn_1.TabStop = false;
            this.btn_1.Text = "1";
            this.btn_1.UseVisualStyleBackColor = false;
            this.btn_1.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_2
            // 
            this.btn_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btn_2.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_2.Location = new System.Drawing.Point(241, 3);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(104, 79);
            this.btn_2.TabIndex = 1;
            this.btn_2.TabStop = false;
            this.btn_2.Text = "2";
            this.btn_2.UseVisualStyleBackColor = false;
            this.btn_2.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_5
            // 
            this.btn_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btn_5.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_5.Location = new System.Drawing.Point(241, 88);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(104, 79);
            this.btn_5.TabIndex = 2;
            this.btn_5.TabStop = false;
            this.btn_5.Text = "5";
            this.btn_5.UseVisualStyleBackColor = false;
            this.btn_5.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_6
            // 
            this.btn_6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btn_6.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_6.Location = new System.Drawing.Point(351, 88);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(104, 79);
            this.btn_6.TabIndex = 3;
            this.btn_6.TabStop = false;
            this.btn_6.Text = "6";
            this.btn_6.UseVisualStyleBackColor = false;
            this.btn_6.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_3
            // 
            this.btn_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btn_3.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_3.Location = new System.Drawing.Point(351, 3);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(104, 79);
            this.btn_3.TabIndex = 4;
            this.btn_3.TabStop = false;
            this.btn_3.Text = "3";
            this.btn_3.UseVisualStyleBackColor = false;
            this.btn_3.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_9
            // 
            this.btn_9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_9.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btn_9.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_9.Location = new System.Drawing.Point(351, 173);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(104, 79);
            this.btn_9.TabIndex = 5;
            this.btn_9.TabStop = false;
            this.btn_9.Text = "9";
            this.btn_9.UseVisualStyleBackColor = false;
            this.btn_9.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_8
            // 
            this.btn_8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_8.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btn_8.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_8.Location = new System.Drawing.Point(241, 173);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(104, 79);
            this.btn_8.TabIndex = 6;
            this.btn_8.TabStop = false;
            this.btn_8.Text = "8";
            this.btn_8.UseVisualStyleBackColor = false;
            this.btn_8.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_4
            // 
            this.btn_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btn_4.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_4.Location = new System.Drawing.Point(131, 88);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(104, 79);
            this.btn_4.TabIndex = 7;
            this.btn_4.TabStop = false;
            this.btn_4.Text = "4";
            this.btn_4.UseVisualStyleBackColor = false;
            this.btn_4.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_7
            // 
            this.btn_7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_7.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btn_7.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_7.Location = new System.Drawing.Point(131, 173);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(104, 79);
            this.btn_7.TabIndex = 8;
            this.btn_7.TabStop = false;
            this.btn_7.Text = "7";
            this.btn_7.UseVisualStyleBackColor = false;
            this.btn_7.Click += new System.EventHandler(this.BtnClick);
            // 
            // lblAcceptedMoney
            // 
            this.lblAcceptedMoney.BackColor = System.Drawing.Color.Transparent;
            this.lblAcceptedMoney.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAcceptedMoney.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcceptedMoney.ForeColor = System.Drawing.Color.Red;
            this.lblAcceptedMoney.Location = new System.Drawing.Point(273, 0);
            this.lblAcceptedMoney.Name = "lblAcceptedMoney";
            this.lblAcceptedMoney.Size = new System.Drawing.Size(513, 70);
            this.lblAcceptedMoney.TabIndex = 11;
            this.lblAcceptedMoney.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAcceptedMoney.TextChanged += new System.EventHandler(this.LblCountTextChanged);
            // 
            // btn_0
            // 
            this.btn_0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btn_0.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_0.Location = new System.Drawing.Point(241, 258);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(104, 79);
            this.btn_0.TabIndex = 12;
            this.btn_0.TabStop = false;
            this.btn_0.Text = "0";
            this.btn_0.UseVisualStyleBackColor = false;
            this.btn_0.Click += new System.EventHandler(this.BtnClick);
            // 
            // back
            // 
            this.back.Dock = System.Windows.Forms.DockStyle.Fill;
            this.back.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.back.ForeColor = System.Drawing.Color.Firebrick;
            this.back.Location = new System.Drawing.Point(131, 258);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(104, 79);
            this.back.TabIndex = 13;
            this.back.TabStop = false;
            this.back.Text = "<<";
            this.back.UseVisualStyleBackColor = false;
            this.back.Click += new System.EventHandler(this.BackClick);
            // 
            // clear
            // 
            this.clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.clear.ForeColor = System.Drawing.Color.Firebrick;
            this.clear.Location = new System.Drawing.Point(351, 258);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(104, 79);
            this.clear.TabIndex = 14;
            this.clear.TabStop = false;
            this.clear.Text = "C";
            this.clear.UseVisualStyleBackColor = false;
            this.clear.Click += new System.EventHandler(this.ClearClick);
            // 
            // lblDiscountTotal
            // 
            this.lblDiscountTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblDiscountTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDiscountTotal.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscountTotal.ForeColor = System.Drawing.Color.Black;
            this.lblDiscountTotal.Location = new System.Drawing.Point(273, 210);
            this.lblDiscountTotal.Name = "lblDiscountTotal";
            this.lblDiscountTotal.Size = new System.Drawing.Size(513, 70);
            this.lblDiscountTotal.TabIndex = 15;
            this.lblDiscountTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDiscountTotal.TextChanged += new System.EventHandler(this.lblDiscountTotal_TextChanged);
            // 
            // lblChange
            // 
            this.lblChange.BackColor = System.Drawing.Color.Transparent;
            this.lblChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblChange.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChange.ForeColor = System.Drawing.Color.Red;
            this.lblChange.Location = new System.Drawing.Point(273, 350);
            this.lblChange.Name = "lblChange";
            this.lblChange.Size = new System.Drawing.Size(513, 70);
            this.lblChange.TabIndex = 16;
            this.lblChange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_dot
            // 
            this.btn_dot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_dot.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btn_dot.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_dot.Location = new System.Drawing.Point(241, 343);
            this.btn_dot.Name = "btn_dot";
            this.btn_dot.Size = new System.Drawing.Size(104, 79);
            this.btn_dot.TabIndex = 17;
            this.btn_dot.TabStop = false;
            this.btn_dot.Text = ".";
            this.btn_dot.UseVisualStyleBackColor = false;
            this.btn_dot.Click += new System.EventHandler(this.BtnDotClick);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(264, 70);
            this.label1.TabIndex = 18;
            this.label1.Text = "Получено";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(3, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(264, 70);
            this.label2.TabIndex = 19;
            this.label2.Text = "Сумма";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(3, 350);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(264, 70);
            this.label3.TabIndex = 20;
            this.label3.Text = "Сдача";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(3, 210);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(264, 70);
            this.label4.TabIndex = 25;
            this.label4.Text = "∑ Скидки";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblToPay
            // 
            this.lblToPay.BackColor = System.Drawing.Color.Transparent;
            this.lblToPay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblToPay.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToPay.ForeColor = System.Drawing.Color.Black;
            this.lblToPay.Location = new System.Drawing.Point(273, 70);
            this.lblToPay.Name = "lblToPay";
            this.lblToPay.Size = new System.Drawing.Size(513, 70);
            this.lblToPay.TabIndex = 24;
            this.lblToPay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(3, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(264, 70);
            this.label5.TabIndex = 27;
            this.label5.Text = "Скидка";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(1259, 70);
            this.label6.TabIndex = 28;
            this.label6.Text = "ОПЛАТА ПОКУПОК";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDiscountPercent
            // 
            this.btnDiscountPercent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDiscountPercent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDiscountPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btnDiscountPercent.ForeColor = System.Drawing.Color.Black;
            this.btnDiscountPercent.Location = new System.Drawing.Point(131, 3);
            this.btnDiscountPercent.Name = "btnDiscountPercent";
            this.btnDiscountPercent.Size = new System.Drawing.Size(104, 79);
            this.btnDiscountPercent.TabIndex = 29;
            this.btnDiscountPercent.TabStop = false;
            this.btnDiscountPercent.Text = "П";
            this.btnDiscountPercent.UseVisualStyleBackColor = false;
            this.btnDiscountPercent.Click += new System.EventHandler(this.btnDiscountTypeIndicator_Click);
            // 
            // btnDiscountFixed
            // 
            this.btnDiscountFixed.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDiscountFixed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDiscountFixed.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btnDiscountFixed.ForeColor = System.Drawing.Color.Black;
            this.btnDiscountFixed.Location = new System.Drawing.Point(351, 3);
            this.btnDiscountFixed.Name = "btnDiscountFixed";
            this.btnDiscountFixed.Size = new System.Drawing.Size(104, 79);
            this.btnDiscountFixed.TabIndex = 30;
            this.btnDiscountFixed.TabStop = false;
            this.btnDiscountFixed.Text = "Ф";
            this.btnDiscountFixed.UseVisualStyleBackColor = false;
            this.btnDiscountFixed.Click += new System.EventHandler(this.btnDiscountTypeIndicator_Click);
            // 
            // lblDiscount
            // 
            this.lblDiscount.BackColor = System.Drawing.Color.Transparent;
            this.lblDiscount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDiscount.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscount.ForeColor = System.Drawing.Color.Black;
            this.lblDiscount.Location = new System.Drawing.Point(273, 140);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(513, 70);
            this.lblDiscount.TabIndex = 31;
            this.lblDiscount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDiscount.TextChanged += new System.EventHandler(this.lblDiscount_TextChanged);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(3, 280);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(264, 70);
            this.label7.TabIndex = 32;
            this.label7.Text = "К оплате";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFinal
            // 
            this.lblFinal.BackColor = System.Drawing.Color.Transparent;
            this.lblFinal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFinal.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinal.ForeColor = System.Drawing.Color.Red;
            this.lblFinal.Location = new System.Drawing.Point(273, 280);
            this.lblFinal.Name = "lblFinal";
            this.lblFinal.Size = new System.Drawing.Size(513, 70);
            this.lblFinal.TabIndex = 33;
            this.lblFinal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDiscount
            // 
            this.btnDiscount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDiscount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btnDiscount.ForeColor = System.Drawing.Color.Black;
            this.btnDiscount.Location = new System.Drawing.Point(241, 3);
            this.btnDiscount.Name = "btnDiscount";
            this.btnDiscount.Size = new System.Drawing.Size(104, 79);
            this.btnDiscount.TabIndex = 34;
            this.btnDiscount.TabStop = false;
            this.btnDiscount.Text = "Д";
            this.btnDiscount.UseVisualStyleBackColor = false;
            this.btnDiscount.Click += new System.EventHandler(this.btnDiscount_Click);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(503, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(244, 80);
            this.label8.TabIndex = 35;
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label8.DoubleClick += new System.EventHandler(this.label8_DoubleClick);
            // 
            // btnDebt
            // 
            this.btnDebt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDebt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDebt.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDebt.ForeColor = System.Drawing.Color.Black;
            this.btnDebt.Location = new System.Drawing.Point(3, 9);
            this.btnDebt.Name = "btnDebt";
            this.btnDebt.Size = new System.Drawing.Size(244, 74);
            this.btnDebt.TabIndex = 36;
            this.btnDebt.TabStop = false;
            this.btnDebt.Text = "В долг";
            this.btnDebt.UseVisualStyleBackColor = false;
            this.btnDebt.Click += new System.EventHandler(this.btnDebt_Click);
            // 
            // btnCreditCard
            // 
            this.btnCreditCard.BackgroundImage = global::vvclient.Properties.Resources.visa6;
            this.btnCreditCard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCreditCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCreditCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreditCard.ForeColor = System.Drawing.Color.Firebrick;
            this.btnCreditCard.Location = new System.Drawing.Point(131, 88);
            this.btnCreditCard.Name = "btnCreditCard";
            this.btnCreditCard.Size = new System.Drawing.Size(104, 79);
            this.btnCreditCard.TabIndex = 37;
            this.btnCreditCard.TabStop = false;
            this.btnCreditCard.UseVisualStyleBackColor = false;
            this.btnCreditCard.Visible = false;
            this.btnCreditCard.Click += new System.EventHandler(this.btnCreditCard_Click);
            // 
            // confirm
            // 
            this.confirm.BackgroundImage = global::vvclient.Properties.Resources.camera_test;
            this.confirm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.confirm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.confirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.confirm.ForeColor = System.Drawing.Color.Firebrick;
            this.confirm.Location = new System.Drawing.Point(131, 343);
            this.confirm.Name = "confirm";
            this.confirm.Size = new System.Drawing.Size(104, 79);
            this.confirm.TabIndex = 10;
            this.confirm.TabStop = false;
            this.confirm.UseVisualStyleBackColor = false;
            this.confirm.Visible = false;
            this.confirm.Click += new System.EventHandler(this.ConfirmClick);
            // 
            // cancel
            // 
            this.cancel.BackgroundImage = global::vvclient.Properties.Resources.Close;
            this.cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.cancel.ForeColor = System.Drawing.Color.Firebrick;
            this.cancel.Location = new System.Drawing.Point(351, 343);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(104, 79);
            this.cancel.TabIndex = 9;
            this.cancel.TabStop = false;
            this.cancel.UseVisualStyleBackColor = false;
            this.cancel.Click += new System.EventHandler(this.CancelClick);
            // 
            // rootPanel
            // 
            this.rootPanel.ColumnCount = 1;
            this.rootPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.rootPanel.Controls.Add(this.label6, 0, 0);
            this.rootPanel.Controls.Add(this.operationPanel, 0, 1);
            this.rootPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rootPanel.Location = new System.Drawing.Point(0, 0);
            this.rootPanel.Name = "rootPanel";
            this.rootPanel.RowCount = 2;
            this.rootPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.rootPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.rootPanel.Size = new System.Drawing.Size(1265, 693);
            this.rootPanel.TabIndex = 38;
            // 
            // operationPanel
            // 
            this.operationPanel.ColumnCount = 2;
            this.operationPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.22478F));
            this.operationPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.77522F));
            this.operationPanel.Controls.Add(this.pnlTopLeft, 0, 0);
            this.operationPanel.Controls.Add(this.pnlTopRight, 1, 0);
            this.operationPanel.Controls.Add(this.pnlBottomLeft, 0, 1);
            this.operationPanel.Controls.Add(this.pnlBottomRight, 1, 1);
            this.operationPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.operationPanel.Location = new System.Drawing.Point(3, 73);
            this.operationPanel.Name = "operationPanel";
            this.operationPanel.RowCount = 2;
            this.operationPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 72.26277F));
            this.operationPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.73723F));
            this.operationPanel.Size = new System.Drawing.Size(1259, 617);
            this.operationPanel.TabIndex = 29;
            // 
            // pnlTopLeft
            // 
            this.pnlTopLeft.ColumnCount = 2;
            this.pnlTopLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 270F));
            this.pnlTopLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlTopLeft.Controls.Add(this.lblAcceptedMoney, 1, 0);
            this.pnlTopLeft.Controls.Add(this.lblChange, 1, 5);
            this.pnlTopLeft.Controls.Add(this.label3, 0, 5);
            this.pnlTopLeft.Controls.Add(this.label7, 0, 4);
            this.pnlTopLeft.Controls.Add(this.lblFinal, 1, 4);
            this.pnlTopLeft.Controls.Add(this.label1, 0, 0);
            this.pnlTopLeft.Controls.Add(this.label2, 0, 1);
            this.pnlTopLeft.Controls.Add(this.label4, 0, 3);
            this.pnlTopLeft.Controls.Add(this.lblDiscount, 1, 2);
            this.pnlTopLeft.Controls.Add(this.lblDiscountTotal, 1, 3);
            this.pnlTopLeft.Controls.Add(this.lblToPay, 1, 1);
            this.pnlTopLeft.Controls.Add(this.label5, 0, 2);
            this.pnlTopLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTopLeft.Location = new System.Drawing.Point(3, 3);
            this.pnlTopLeft.Name = "pnlTopLeft";
            this.pnlTopLeft.RowCount = 7;
            this.pnlTopLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.pnlTopLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.pnlTopLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.pnlTopLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.pnlTopLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.pnlTopLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.pnlTopLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlTopLeft.Size = new System.Drawing.Size(789, 439);
            this.pnlTopLeft.TabIndex = 0;
            // 
            // pnlTopRight
            // 
            this.pnlTopRight.ColumnCount = 4;
            this.pnlTopRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlTopRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.pnlTopRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.pnlTopRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.pnlTopRight.Controls.Add(this.btn_1, 1, 0);
            this.pnlTopRight.Controls.Add(this.btn_2, 2, 0);
            this.pnlTopRight.Controls.Add(this.btn_3, 3, 0);
            this.pnlTopRight.Controls.Add(this.btn_4, 1, 1);
            this.pnlTopRight.Controls.Add(this.btn_5, 2, 1);
            this.pnlTopRight.Controls.Add(this.btn_6, 3, 1);
            this.pnlTopRight.Controls.Add(this.btn_7, 1, 2);
            this.pnlTopRight.Controls.Add(this.btn_8, 2, 2);
            this.pnlTopRight.Controls.Add(this.btn_9, 3, 2);
            this.pnlTopRight.Controls.Add(this.back, 1, 3);
            this.pnlTopRight.Controls.Add(this.btn_0, 2, 3);
            this.pnlTopRight.Controls.Add(this.clear, 3, 3);
            this.pnlTopRight.Controls.Add(this.confirm, 1, 4);
            this.pnlTopRight.Controls.Add(this.btn_dot, 2, 4);
            this.pnlTopRight.Controls.Add(this.cancel, 3, 4);
            this.pnlTopRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTopRight.Location = new System.Drawing.Point(798, 3);
            this.pnlTopRight.Name = "pnlTopRight";
            this.pnlTopRight.RowCount = 6;
            this.pnlTopRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.pnlTopRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.pnlTopRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.pnlTopRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.pnlTopRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.pnlTopRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlTopRight.Size = new System.Drawing.Size(458, 439);
            this.pnlTopRight.TabIndex = 1;
            // 
            // pnlBottomLeft
            // 
            this.pnlBottomLeft.ColumnCount = 4;
            this.pnlBottomLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.pnlBottomLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.pnlBottomLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.pnlBottomLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlBottomLeft.Controls.Add(this.btnDiscountAuto, 0, 2);
            this.pnlBottomLeft.Controls.Add(this.btnInfo, 0, 2);
            this.pnlBottomLeft.Controls.Add(this.btnDebt, 0, 1);
            this.pnlBottomLeft.Controls.Add(this.btnSplitPay, 1, 1);
            this.pnlBottomLeft.Controls.Add(this.label8, 2, 1);
            this.pnlBottomLeft.Controls.Add(this.lblNetError, 1, 2);
            this.pnlBottomLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBottomLeft.Location = new System.Drawing.Point(3, 448);
            this.pnlBottomLeft.Name = "pnlBottomLeft";
            this.pnlBottomLeft.RowCount = 3;
            this.pnlBottomLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlBottomLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.pnlBottomLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.pnlBottomLeft.Size = new System.Drawing.Size(789, 166);
            this.pnlBottomLeft.TabIndex = 2;
            // 
            // btnDiscountAuto
            // 
            this.btnDiscountAuto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDiscountAuto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDiscountAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDiscountAuto.ForeColor = System.Drawing.Color.Firebrick;
            this.btnDiscountAuto.Location = new System.Drawing.Point(253, 89);
            this.btnDiscountAuto.Name = "btnDiscountAuto";
            this.btnDiscountAuto.Size = new System.Drawing.Size(244, 74);
            this.btnDiscountAuto.TabIndex = 41;
            this.btnDiscountAuto.TabStop = false;
            this.btnDiscountAuto.Text = "Авто";
            this.btnDiscountAuto.UseVisualStyleBackColor = false;
            this.btnDiscountAuto.Click += new System.EventHandler(this.btnDiscountTypeIndicator_Click);
            // 
            // btnInfo
            // 
            this.btnInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold);
            this.btnInfo.ForeColor = System.Drawing.Color.Black;
            this.btnInfo.Location = new System.Drawing.Point(3, 89);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(244, 74);
            this.btnInfo.TabIndex = 39;
            this.btnInfo.TabStop = false;
            this.btnInfo.Text = "Info";
            this.btnInfo.UseVisualStyleBackColor = false;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click_1);
            // 
            // btnSplitPay
            // 
            this.btnSplitPay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSplitPay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSplitPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSplitPay.ForeColor = System.Drawing.Color.Black;
            this.btnSplitPay.Location = new System.Drawing.Point(253, 9);
            this.btnSplitPay.Name = "btnSplitPay";
            this.btnSplitPay.Size = new System.Drawing.Size(244, 74);
            this.btnSplitPay.TabIndex = 37;
            this.btnSplitPay.TabStop = false;
            this.btnSplitPay.Text = "Разд.Плат";
            this.btnSplitPay.UseVisualStyleBackColor = false;
            this.btnSplitPay.Visible = false;
            this.btnSplitPay.Click += new System.EventHandler(this.btnSplitPay_Click);
            // 
            // lblNetError
            // 
            this.lblNetError.AutoSize = true;
            this.lblNetError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNetError.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNetError.ForeColor = System.Drawing.Color.Red;
            this.lblNetError.Location = new System.Drawing.Point(503, 86);
            this.lblNetError.Name = "lblNetError";
            this.lblNetError.Size = new System.Drawing.Size(244, 80);
            this.lblNetError.TabIndex = 40;
            // 
            // pnlBottomRight
            // 
            this.pnlBottomRight.ColumnCount = 4;
            this.pnlBottomRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlBottomRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.pnlBottomRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.pnlBottomRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.pnlBottomRight.Controls.Add(this.btnDiscountPercent, 1, 0);
            this.pnlBottomRight.Controls.Add(this.btnCreditCard, 1, 1);
            this.pnlBottomRight.Controls.Add(this.btnDiscount, 2, 0);
            this.pnlBottomRight.Controls.Add(this.btnDiscountFixed, 3, 0);
            this.pnlBottomRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBottomRight.Location = new System.Drawing.Point(798, 448);
            this.pnlBottomRight.Name = "pnlBottomRight";
            this.pnlBottomRight.RowCount = 2;
            this.pnlBottomRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.pnlBottomRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.pnlBottomRight.Size = new System.Drawing.Size(458, 166);
            this.pnlBottomRight.TabIndex = 3;
            // 
            // ChangeBack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1265, 693);
            this.Controls.Add(this.rootPanel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "ChangeBack";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nums";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChangeBack_KeyDown);
            this.rootPanel.ResumeLayout(false);
            this.operationPanel.ResumeLayout(false);
            this.pnlTopLeft.ResumeLayout(false);
            this.pnlTopRight.ResumeLayout(false);
            this.pnlBottomLeft.ResumeLayout(false);
            this.pnlBottomLeft.PerformLayout();
            this.pnlBottomRight.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_5;
        private System.Windows.Forms.Button btn_6;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Button btn_9;
        private System.Windows.Forms.Button btn_8;
        private System.Windows.Forms.Button btn_4;
        private System.Windows.Forms.Button btn_7;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button confirm;
        private System.Windows.Forms.Label lblAcceptedMoney;
        private System.Windows.Forms.Button btn_0;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Label lblDiscountTotal;
        private System.Windows.Forms.Label lblChange;
        private System.Windows.Forms.Button btn_dot;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        //private System.Windows.Forms.Button btnDiscount_1;
        //private System.Windows.Forms.Button btnDiscount_2;
        //private System.Windows.Forms.Button btnDiscount_3;
        //private System.Windows.Forms.Button btnClearDiscount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblToPay;
        private System.Windows.Forms.Label label5;
        //private System.Windows.Forms.Label lblDiscount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnDiscountPercent;
        private System.Windows.Forms.Button btnDiscountFixed;
        private System.Windows.Forms.Label lblDiscount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblFinal;
        private System.Windows.Forms.Button btnDiscount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnDebt;
        private System.Windows.Forms.Button btnCreditCard;
        private System.Windows.Forms.TableLayoutPanel rootPanel;
        private System.Windows.Forms.TableLayoutPanel operationPanel;
        private System.Windows.Forms.TableLayoutPanel pnlTopLeft;
        private System.Windows.Forms.TableLayoutPanel pnlTopRight;
        private System.Windows.Forms.TableLayoutPanel pnlBottomLeft;
        private System.Windows.Forms.TableLayoutPanel pnlBottomRight;
        private System.Windows.Forms.Button btnSplitPay;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.Label lblNetError;
        private System.Windows.Forms.Button btnDiscountAuto;
    }
}