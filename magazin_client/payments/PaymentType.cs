﻿namespace vvclient.payments
{
    public enum PaymentType
    {
        cash,
        debt,
        card
    }
}