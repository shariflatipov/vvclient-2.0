﻿using System;
using System.Data.SQLite;
using vvclient.utils;

namespace vvclient.payments
{
    public class PaymentPerson
    {
        public int Id { get; private set; }
        public string Code { get; private set; }
        public string Name { get; private set; }
        public int Status { get; private set; }

        public static PaymentPerson GetPaymentPersonById(string code)
        {
            var person = new PaymentPerson();
            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (
                        var command =
                            new SQLiteCommand(
                                "select id, name, discount_code, state from discount where discount_code = '" + code +
                                "'"))
                    {
                        command.Connection = connection;
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                person.Id = int.Parse(reader[0].ToString());
                                person.Name = reader[1].ToString();
                                person.Code = reader[2].ToString();
                                person.Status = int.Parse(reader[3].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }

            if (person.Id > 0)
            {
                return person;
            }
            return null;
        }
    }
}