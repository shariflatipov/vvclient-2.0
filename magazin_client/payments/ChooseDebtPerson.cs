﻿using System;
using System.Windows.Forms;

namespace vvclient.payments
{
    public partial class ChooseDebtPerson : Form
    {
        public PaymentPerson Person;

        public ChooseDebtPerson()
        {
            InitializeComponent();
        }

        private void txtDebtPerson_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Person = PaymentPerson.GetPaymentPersonById(txtDebtPerson.Text);
                if (Person == null)
                {
                    label2.Text = "Человека с данным дисконтом не существует";
                }
                else
                {
                    label2.Text = "ФИО: " + Person.Name;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}