﻿namespace vvclient
{
    partial class AuthForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Pass = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.deskKeyboard = new System.Windows.Forms.PictureBox();
            this.Login = new System.Windows.Forms.ComboBox();
            this.A = new System.Windows.Forms.Button();
            this.B = new System.Windows.Forms.Button();
            this.C = new System.Windows.Forms.Button();
            this.D = new System.Windows.Forms.Button();
            this.E = new System.Windows.Forms.Button();
            this.F = new System.Windows.Forms.Button();
            this.G = new System.Windows.Forms.Button();
            this.H = new System.Windows.Forms.Button();
            this.I = new System.Windows.Forms.Button();
            this.J = new System.Windows.Forms.Button();
            this.K = new System.Windows.Forms.Button();
            this.L = new System.Windows.Forms.Button();
            this.M = new System.Windows.Forms.Button();
            this.N = new System.Windows.Forms.Button();
            this.O = new System.Windows.Forms.Button();
            this.P = new System.Windows.Forms.Button();
            this.Q = new System.Windows.Forms.Button();
            this.R = new System.Windows.Forms.Button();
            this.S = new System.Windows.Forms.Button();
            this.T = new System.Windows.Forms.Button();
            this.U = new System.Windows.Forms.Button();
            this.V = new System.Windows.Forms.Button();
            this.W = new System.Windows.Forms.Button();
            this.X = new System.Windows.Forms.Button();
            this.Y = new System.Windows.Forms.Button();
            this.Z = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.backspace = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.deskKeyboard)).BeginInit();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(471, 458);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(147, 68);
            this.button2.TabIndex = 3;
            this.button2.Text = "Войти";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.UserAuth);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(305, 345);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 35);
            this.label1.TabIndex = 7;
            this.label1.Text = "Логин:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(305, 386);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 35);
            this.label2.TabIndex = 8;
            this.label2.Text = "Пароль:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Pass
            // 
            this.Pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Pass.Location = new System.Drawing.Point(461, 392);
            this.Pass.Name = "Pass";
            this.Pass.PasswordChar = '*';
            this.Pass.Size = new System.Drawing.Size(157, 29);
            this.Pass.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(23, 488);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 38);
            this.label3.TabIndex = 6;
            this.label3.Text = "Вход  в автоматизированную систему магазина";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.Menu;
            this.btnExit.Location = new System.Drawing.Point(288, 458);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(144, 68);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Выйти";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.BtnExitClick);
            // 
            // deskKeyboard
            // 
            this.deskKeyboard.Image = global::vvclient.Properties.Resources.apple_aluminum_keyboard1;
            this.deskKeyboard.Location = new System.Drawing.Point(12, 423);
            this.deskKeyboard.Name = "deskKeyboard";
            this.deskKeyboard.Size = new System.Drawing.Size(122, 54);
            this.deskKeyboard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.deskKeyboard.TabIndex = 8;
            this.deskKeyboard.TabStop = false;
            this.deskKeyboard.Visible = false;
            // 
            // Login
            // 
            this.Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Login.FormattingEnabled = true;
            this.Login.Location = new System.Drawing.Point(461, 348);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(157, 32);
            this.Login.TabIndex = 1;
            // 
            // A
            // 
            this.A.Location = new System.Drawing.Point(78, 174);
            this.A.Name = "A";
            this.A.Size = new System.Drawing.Size(75, 69);
            this.A.TabIndex = 9;
            this.A.Text = "A";
            this.A.UseVisualStyleBackColor = true;
            this.A.Click += new System.EventHandler(this.NumValidation);
            // 
            // B
            // 
            this.B.Location = new System.Drawing.Point(484, 249);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(75, 69);
            this.B.TabIndex = 10;
            this.B.Text = "B";
            this.B.UseVisualStyleBackColor = true;
            this.B.Click += new System.EventHandler(this.NumValidation);
            // 
            // C
            // 
            this.C.Location = new System.Drawing.Point(323, 250);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(75, 69);
            this.C.TabIndex = 11;
            this.C.Text = "C";
            this.C.UseVisualStyleBackColor = true;
            this.C.Click += new System.EventHandler(this.NumValidation);
            // 
            // D
            // 
            this.D.Location = new System.Drawing.Point(240, 175);
            this.D.Name = "D";
            this.D.Size = new System.Drawing.Size(75, 69);
            this.D.TabIndex = 12;
            this.D.Text = "D";
            this.D.UseVisualStyleBackColor = true;
            this.D.Click += new System.EventHandler(this.NumValidation);
            // 
            // E
            // 
            this.E.Location = new System.Drawing.Point(200, 100);
            this.E.Name = "E";
            this.E.Size = new System.Drawing.Size(75, 69);
            this.E.TabIndex = 13;
            this.E.Text = "E";
            this.E.UseVisualStyleBackColor = true;
            this.E.Click += new System.EventHandler(this.NumValidation);
            // 
            // F
            // 
            this.F.Location = new System.Drawing.Point(321, 174);
            this.F.Name = "F";
            this.F.Size = new System.Drawing.Size(75, 69);
            this.F.TabIndex = 14;
            this.F.Text = "F";
            this.F.UseVisualStyleBackColor = true;
            this.F.Click += new System.EventHandler(this.NumValidation);
            // 
            // G
            // 
            this.G.Location = new System.Drawing.Point(401, 174);
            this.G.Name = "G";
            this.G.Size = new System.Drawing.Size(75, 69);
            this.G.TabIndex = 15;
            this.G.Text = "G";
            this.G.UseVisualStyleBackColor = true;
            this.G.Click += new System.EventHandler(this.NumValidation);
            // 
            // H
            // 
            this.H.Location = new System.Drawing.Point(482, 174);
            this.H.Name = "H";
            this.H.Size = new System.Drawing.Size(75, 69);
            this.H.TabIndex = 16;
            this.H.Text = "H";
            this.H.UseVisualStyleBackColor = true;
            this.H.Click += new System.EventHandler(this.NumValidation);
            // 
            // I
            // 
            this.I.Location = new System.Drawing.Point(605, 100);
            this.I.Name = "I";
            this.I.Size = new System.Drawing.Size(75, 69);
            this.I.TabIndex = 17;
            this.I.Text = "I";
            this.I.UseVisualStyleBackColor = true;
            this.I.Click += new System.EventHandler(this.NumValidation);
            // 
            // J
            // 
            this.J.Location = new System.Drawing.Point(563, 174);
            this.J.Name = "J";
            this.J.Size = new System.Drawing.Size(75, 69);
            this.J.TabIndex = 18;
            this.J.Text = "J";
            this.J.UseVisualStyleBackColor = true;
            this.J.Click += new System.EventHandler(this.NumValidation);
            // 
            // K
            // 
            this.K.Location = new System.Drawing.Point(644, 174);
            this.K.Name = "K";
            this.K.Size = new System.Drawing.Size(75, 69);
            this.K.TabIndex = 19;
            this.K.Text = "K";
            this.K.UseVisualStyleBackColor = true;
            this.K.Click += new System.EventHandler(this.NumValidation);
            // 
            // L
            // 
            this.L.Location = new System.Drawing.Point(726, 174);
            this.L.Name = "L";
            this.L.Size = new System.Drawing.Size(75, 69);
            this.L.TabIndex = 20;
            this.L.Text = "L";
            this.L.UseVisualStyleBackColor = true;
            this.L.Click += new System.EventHandler(this.NumValidation);
            // 
            // M
            // 
            this.M.Location = new System.Drawing.Point(647, 249);
            this.M.Name = "M";
            this.M.Size = new System.Drawing.Size(75, 69);
            this.M.TabIndex = 21;
            this.M.Text = "M";
            this.M.UseVisualStyleBackColor = true;
            this.M.Click += new System.EventHandler(this.NumValidation);
            // 
            // N
            // 
            this.N.Location = new System.Drawing.Point(566, 249);
            this.N.Name = "N";
            this.N.Size = new System.Drawing.Size(75, 69);
            this.N.TabIndex = 22;
            this.N.Text = "N";
            this.N.UseVisualStyleBackColor = true;
            this.N.Click += new System.EventHandler(this.NumValidation);
            // 
            // O
            // 
            this.O.Location = new System.Drawing.Point(686, 99);
            this.O.Name = "O";
            this.O.Size = new System.Drawing.Size(75, 69);
            this.O.TabIndex = 23;
            this.O.Text = "O";
            this.O.UseVisualStyleBackColor = true;
            this.O.Click += new System.EventHandler(this.NumValidation);
            // 
            // P
            // 
            this.P.Location = new System.Drawing.Point(767, 99);
            this.P.Name = "P";
            this.P.Size = new System.Drawing.Size(75, 69);
            this.P.TabIndex = 24;
            this.P.Text = "P";
            this.P.UseVisualStyleBackColor = true;
            this.P.Click += new System.EventHandler(this.NumValidation);
            // 
            // Q
            // 
            this.Q.Location = new System.Drawing.Point(38, 99);
            this.Q.Name = "Q";
            this.Q.Size = new System.Drawing.Size(75, 69);
            this.Q.TabIndex = 25;
            this.Q.Text = "Q";
            this.Q.UseVisualStyleBackColor = true;
            this.Q.Click += new System.EventHandler(this.NumValidation);
            // 
            // R
            // 
            this.R.Location = new System.Drawing.Point(281, 100);
            this.R.Name = "R";
            this.R.Size = new System.Drawing.Size(75, 69);
            this.R.TabIndex = 26;
            this.R.Text = "R";
            this.R.UseVisualStyleBackColor = true;
            this.R.Click += new System.EventHandler(this.NumValidation);
            // 
            // S
            // 
            this.S.Location = new System.Drawing.Point(159, 174);
            this.S.Name = "S";
            this.S.Size = new System.Drawing.Size(75, 69);
            this.S.TabIndex = 27;
            this.S.Text = "S";
            this.S.UseVisualStyleBackColor = true;
            this.S.Click += new System.EventHandler(this.NumValidation);
            // 
            // T
            // 
            this.T.Location = new System.Drawing.Point(362, 100);
            this.T.Name = "T";
            this.T.Size = new System.Drawing.Size(75, 69);
            this.T.TabIndex = 28;
            this.T.Text = "T";
            this.T.UseVisualStyleBackColor = true;
            this.T.Click += new System.EventHandler(this.NumValidation);
            // 
            // U
            // 
            this.U.Location = new System.Drawing.Point(524, 100);
            this.U.Name = "U";
            this.U.Size = new System.Drawing.Size(75, 69);
            this.U.TabIndex = 29;
            this.U.Text = "U";
            this.U.UseVisualStyleBackColor = true;
            this.U.Click += new System.EventHandler(this.NumValidation);
            // 
            // V
            // 
            this.V.Location = new System.Drawing.Point(404, 250);
            this.V.Name = "V";
            this.V.Size = new System.Drawing.Size(75, 69);
            this.V.TabIndex = 30;
            this.V.Text = "V";
            this.V.UseVisualStyleBackColor = true;
            this.V.Click += new System.EventHandler(this.NumValidation);
            // 
            // W
            // 
            this.W.Location = new System.Drawing.Point(119, 99);
            this.W.Name = "W";
            this.W.Size = new System.Drawing.Size(75, 69);
            this.W.TabIndex = 31;
            this.W.Text = "W";
            this.W.UseVisualStyleBackColor = true;
            this.W.Click += new System.EventHandler(this.NumValidation);
            // 
            // X
            // 
            this.X.Location = new System.Drawing.Point(242, 249);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(75, 69);
            this.X.TabIndex = 32;
            this.X.Text = "X";
            this.X.UseVisualStyleBackColor = true;
            this.X.Click += new System.EventHandler(this.NumValidation);
            // 
            // Y
            // 
            this.Y.Location = new System.Drawing.Point(443, 100);
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(75, 69);
            this.Y.TabIndex = 33;
            this.Y.Text = "Y";
            this.Y.UseVisualStyleBackColor = true;
            this.Y.Click += new System.EventHandler(this.NumValidation);
            // 
            // Z
            // 
            this.Z.Location = new System.Drawing.Point(160, 249);
            this.Z.Name = "Z";
            this.Z.Size = new System.Drawing.Size(75, 69);
            this.Z.TabIndex = 34;
            this.Z.Text = "Z";
            this.Z.UseVisualStyleBackColor = true;
            this.Z.Click += new System.EventHandler(this.NumValidation);
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(85, 324);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(149, 69);
            this.clear.TabIndex = 35;
            this.clear.Text = "clear";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.NumValidation);
            // 
            // backspace
            // 
            this.backspace.Location = new System.Drawing.Point(652, 324);
            this.backspace.Name = "backspace";
            this.backspace.Size = new System.Drawing.Size(149, 69);
            this.backspace.TabIndex = 37;
            this.backspace.Text = "backspace";
            this.backspace.UseVisualStyleBackColor = true;
            this.backspace.Click += new System.EventHandler(this.NumValidation);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(37, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 69);
            this.button1.TabIndex = 38;
            this.button1.Text = "0";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.NumValidation);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(118, 24);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 69);
            this.button3.TabIndex = 39;
            this.button3.Text = "1";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.NumValidation);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(199, 25);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 69);
            this.button4.TabIndex = 40;
            this.button4.Text = "2";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.NumValidation);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(280, 25);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 69);
            this.button5.TabIndex = 41;
            this.button5.Text = "3";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.NumValidation);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(361, 25);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 69);
            this.button6.TabIndex = 42;
            this.button6.Text = "4";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.NumValidation);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(442, 25);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 69);
            this.button7.TabIndex = 43;
            this.button7.Text = "5";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.NumValidation);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(523, 25);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 69);
            this.button8.TabIndex = 44;
            this.button8.Text = "6";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.NumValidation);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(604, 25);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 69);
            this.button9.TabIndex = 45;
            this.button9.Text = "7";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.NumValidation);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(685, 25);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 69);
            this.button10.TabIndex = 46;
            this.button10.Text = "8";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.NumValidation);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(766, 25);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 69);
            this.button11.TabIndex = 47;
            this.button11.Text = "9";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.NumValidation);
            // 
            // AuthForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(909, 571);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.backspace);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.Z);
            this.Controls.Add(this.Y);
            this.Controls.Add(this.X);
            this.Controls.Add(this.W);
            this.Controls.Add(this.V);
            this.Controls.Add(this.U);
            this.Controls.Add(this.T);
            this.Controls.Add(this.S);
            this.Controls.Add(this.R);
            this.Controls.Add(this.Q);
            this.Controls.Add(this.P);
            this.Controls.Add(this.O);
            this.Controls.Add(this.N);
            this.Controls.Add(this.M);
            this.Controls.Add(this.L);
            this.Controls.Add(this.K);
            this.Controls.Add(this.J);
            this.Controls.Add(this.I);
            this.Controls.Add(this.H);
            this.Controls.Add(this.G);
            this.Controls.Add(this.F);
            this.Controls.Add(this.E);
            this.Controls.Add(this.D);
            this.Controls.Add(this.C);
            this.Controls.Add(this.B);
            this.Controls.Add(this.A);
            this.Controls.Add(this.Login);
            this.Controls.Add(this.deskKeyboard);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Pass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AuthForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConfigWindow";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.AuthFormLoad);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AuthForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.deskKeyboard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Pass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.PictureBox deskKeyboard;
        private System.Windows.Forms.ComboBox Login;
        private System.Windows.Forms.Button A;
        private System.Windows.Forms.Button B;
        private System.Windows.Forms.Button C;
        private System.Windows.Forms.Button D;
        private System.Windows.Forms.Button E;
        private System.Windows.Forms.Button F;
        private System.Windows.Forms.Button G;
        private System.Windows.Forms.Button H;
        private System.Windows.Forms.Button I;
        private System.Windows.Forms.Button J;
        private System.Windows.Forms.Button K;
        private System.Windows.Forms.Button L;
        private System.Windows.Forms.Button M;
        private System.Windows.Forms.Button N;
        private System.Windows.Forms.Button O;
        private System.Windows.Forms.Button P;
        private System.Windows.Forms.Button Q;
        private System.Windows.Forms.Button R;
        private System.Windows.Forms.Button S;
        private System.Windows.Forms.Button T;
        private System.Windows.Forms.Button U;
        private System.Windows.Forms.Button V;
        private System.Windows.Forms.Button W;
        private System.Windows.Forms.Button X;
        private System.Windows.Forms.Button Y;
        private System.Windows.Forms.Button Z;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button backspace;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
    }
}