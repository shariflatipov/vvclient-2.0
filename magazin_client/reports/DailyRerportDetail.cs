﻿using System;
using System.Windows.Forms;
using vvclient.goodsreturn;
using vvclient.nums;

namespace vvclient.reports
{
    public partial class DailyRerportDetail : Form
    {
        ReturnGood _returnGood = new ReturnGood();
        public DailyRerportDetail(string soldId)
        {
            InitializeComponent();
            _returnGood = ReturnGood.GetById(soldId);
            var alreadyReturned = ReturnGood.GetReturnsBySoldId(soldId);

            foreach (var item in _returnGood.ReturnProductList)
            {

                foreach (var returns in alreadyReturned)
                {
                    if (returns.Batch == item.Batch && returns.Product == item.Product)
                    {
                        item.Count -= returns.Count;
                    }
                }

                if (item.Count > 0)
                {
                    dataGridView1.Rows.Add(item.TrnId, item.ProductName, item.Product, item.Count, item.Price, item.Batch,
                        item.Discount);
                }
            }

        }

        private void DgReturnCellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 3)
                {
                    // Change quantity of goods and calculate total
                    var nums = new NumsReturn(double.Parse(dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString()));
                    nums.ShowDialog();
                    var quantity = nums.GetQuantity();
                    dataGridView1.Rows[e.RowIndex].Cells[3].Value = quantity;
                }
            }
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            _returnGood.ReturnProductList.Clear();
            int selectedCounter = 0;
            foreach (DataGridViewRow item in dataGridView1.SelectedRows)
            {
                var collection = new ReturnProductCollection()
                {
                    TrnId = long.Parse(item.Cells["trnId"].Value.ToString()),
                    Batch = item.Cells["batch"].Value.ToString(),
                    Product = item.Cells["barcode"].Value.ToString(),
                    ProductName = item.Cells["GoodName"].Value.ToString(),
                    Count = double.Parse(item.Cells["Count"].Value.ToString()),
                    Price = double.Parse(item.Cells["Price"].Value.ToString()),
                    Discount = item.Cells["Discount"].Value.ToString()
                };
                _returnGood.ReturnProductList.Add(collection);
                selectedCounter++;
            }
            if (selectedCounter > 0)
            {
                _returnGood.Save();
                Dispose();
            } else
            {
                MessageBox.Show("Не выбран товар для возврата");
            }
        }
    }
}
