﻿using System;
using System.Windows.Forms;

namespace vvclient.reports {
    public partial class SoldDetailsReport : Form {
        public string _soldId;
        public SoldDetailsReport(string soldId) {
            _soldId = soldId;
            InitializeComponent();
        }

        private void SoldDetailsReport_Load(object sender, EventArgs e) {
            dgvDetails.DataSource = SoldReport.GetSoldDetails(_soldId);
        }
    }
}
