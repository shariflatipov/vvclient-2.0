﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using vvclient.utils;

namespace vvclient.reports
{
    internal static class DailyReport
    {
        public static List<DailyReportObject> GetDailyReport()
        {
            return GetDailyReport(DateTime.Today);
        }

        public static List<DailyReportObject> GetDailyReport(DateTime time)
        {
            var today = DateTime.Today;
            var delta = (today - time).TotalDays;

            var resultList = new List<DailyReportObject>();

            double maxAllowedDays = 1;
            double.TryParse(DBConnection.GetDBValues("MaxAllowedDays"), out maxAllowedDays); 

            if (delta > maxAllowedDays)
            {
                return resultList;
            }

            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText = "SELECT sp.id_sold, s.login, sp.[sum], sp.discount, (select sum(count) from transactions t where t.sold_id = sp.id_sold) as cnt " +
                                              "FROM sold_product sp, seller s " +
                                              "WHERE " +
                                              " sp.seller_id = s.id_seller " +
                                              " and start_date like @sdt union all SELECT sp.sold_id as id_sold, s.login, sp.total * -1, sp.discount * -1 " +
                                              ", (select sum(quantity) from return_details rd where rd.return_id = sp.id) as cnt " + 
                                              "FROM return_product sp, seller s " +
                                              "WHERE " +
                                              " sp.seller_id = s.id_seller " +
                                              " and created_at like @sdt";

                        command.Parameters.Add(new SQLiteParameter("@sdt", time.ToString("yyyy-MM-dd") + '%'));

                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var sum = double.Parse(reader[2].ToString());
                                var discount = double.Parse(reader[3].ToString());
                                var obj = new DailyReportObject
                                {
                                    SoldId = reader[0].ToString(),
                                    Login = reader[1].ToString(),
                                    Sum = sum,
                                    DiscountSum = Math.Round(discount, 2),
                                    Total = Math.Round(sum - discount, 2),
                                    DetailCount = double.Parse(reader[4].ToString())
                                };

                                resultList.Add(obj);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Log(exception, "Daily Report");
            }
            return resultList;
        }
    }
}