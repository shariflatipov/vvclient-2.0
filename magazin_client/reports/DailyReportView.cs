﻿using System;
using System.Windows.Forms;
using System.Linq;

namespace vvclient.reports
{
    public partial class DailyReportView : Form
    {
        BindingSource _source = new BindingSource();

        public DailyReportView()
        {
            InitializeComponent();
        }

        private void DailyReportView_Load(object sender, EventArgs e)
        {
            var lst = DailyReport.GetDailyReport();
            _source.DataSource = lst;

            var sm = Math.Round(lst.Sum(x => x.Total), 2);

            dataGridView1.DataSource = _source;
            lblIncomeValue.Text = sm.ToString();
        }

        private void DailyReportView_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                string soldId = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                DailyRerportDetail form = new DailyRerportDetail(soldId);
                form.ShowDialog();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            DateTime time = dateTimePicker1.Value;

            var lst = DailyReport.GetDailyReport(time);
            _source.DataSource = lst;

            var sm = lst.Sum(x => x.Total);

            dataGridView1.DataSource = _source;
            lblIncomeValue.Text = sm.ToString();
        }
    }
}