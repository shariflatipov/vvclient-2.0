﻿namespace vvclient.reports
{
    public class DailyReportObject
    {
        public string SoldId { get; set; }
        public string Login { get; set; }
        public double Sum { get; set; }
        public double DiscountSum { get; set; }
        public double Total { get; set; }
        public double DetailCount { get; set; }
    }
}
