﻿using System;
using System.Data;
using System.Data.SQLite;
using vvclient.utils;

namespace vvclient.reports {
    public class SoldReport {

        public static DataTable GetSoldProducts() {
            var dtReturns = new DataTable();
            dtReturns.Columns.Add("SoldId", typeof(string));
            dtReturns.Columns.Add("Login", typeof(string));
            dtReturns.Columns.Add("CashSum", typeof(double));
            dtReturns.Columns.Add("Discount", typeof(double));
            dtReturns.Columns.Add("Total", typeof(double));

            try {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString)) {
                    using (var command = new SQLiteCommand(connection)) {
                        command.CommandText = "SELECT sp.id_sold, s.login, sp.[sum], sp.discount, (sp.[sum] - sp.discount) as total " +
                                              "FROM sold_product sp, seller s " +
                                              "WHERE " +
                                              " sp.seller_id = s.id_seller " +
                                              " and start_date like @sdt ";

                        command.Parameters.Add(new SQLiteParameter("@sdt", DateTime.Today.ToString("yyyy-MM-dd") + '%'));

                        connection.Open();
                        using (var reader = command.ExecuteReader()) {
                            while (reader.Read()) {
                                dtReturns.Rows.Add(
                                    reader[0].ToString(),
                                    reader[1].ToString(),
                                    double.Parse(reader[2].ToString()),
                                    double.Parse(reader[3].ToString()),
                                    double.Parse(reader[4].ToString()));
                            }
                        }
                    }
                }
            } catch (Exception exception) {
                Logger.Log(exception, "Daily Report");
            }
            return dtReturns;
        }


        public static DataTable GetSoldDetails(string soldId) {
            var dtReturns = new DataTable();
            dtReturns.Columns.Add("SoldId", typeof(string));
            dtReturns.Columns.Add("Login", typeof(string));
            dtReturns.Columns.Add("CashSum", typeof(double));
            dtReturns.Columns.Add("Discount", typeof(double));

            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText = "select * from transactions where sold_id = @soldid";

                        command.Parameters.Add(new SQLiteParameter("@soldid", soldId));

                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                dtReturns.Rows.Add(
                                    reader[0].ToString(),
                                    reader[1].ToString(),
                                    double.Parse(reader[2].ToString()),
                                    double.Parse(reader[3].ToString()));
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Log(exception, "Daily Report");
            }
            return dtReturns;
        }
    }
}
