﻿using System.Data.SQLite;
using System.Text;
using System.Windows.Forms;

namespace vvclient.utils
{
    internal class SQLiteWrapper
    {
        public static readonly string ConnectionString = string.Format
            ("Data Source=\"{0}\";Password=\"{1}\"", "db.db3", "lsJKG98ljf&*^!@#");

        public static int ExecuteNonQuery(string query)
        {
            var affectedRows = 0;
            using (var connection = new SQLiteConnection(ConnectionString))
            {
                using (var command = new SQLiteCommand(query, connection))
                {
                    connection.Open();
                    affectedRows = command.ExecuteNonQuery();
                }
            }
            return affectedRows;
        }

        public static object ExecuteScalar(string query)
        {
            object result = null;
            using (var connection = new SQLiteConnection(ConnectionString))
            {
                using (var command = new SQLiteCommand(query, connection))
                {
                    connection.Open();
                    result = command.ExecuteScalar();
                }
            }
            return result;
        }

        public static long ExecuteAndReturnLastId(string query)
        {
            long lastId = 0;
            using (var connection = new SQLiteConnection(ConnectionString))
            {
                using (var command = new SQLiteCommand(query, connection))
                {
                    connection.Open();
                    if (command.ExecuteNonQuery() > 0)
                    {
                        lastId = connection.LastInsertRowId;
                    }
                }
            }
            return lastId;
        }

        public static string PrepareMultiRow(DataGridView dgv, string constantToAllFields = "", params string[] fields)
        {
            var builder = new StringBuilder();

            for (var i = 0; i < dgv.Rows.Count; i++)
            {
                builder.Append("( ");

                if (!string.IsNullOrWhiteSpace(constantToAllFields))
                {
                    builder.Append("'");
                    builder.Append(constantToAllFields);
                    builder.Append("', ");
                }

                for (var j = 0; j < fields.Length; j++)
                {
                    builder.Append("'");
                    builder.Append(dgv.Rows[i].Cells[fields[j]].Value);
                    builder.Append("'");
                    if (fields.Length > j + 1)
                    {
                        builder.Append(", ");
                    }
                }

                builder.Append((dgv.Rows.Count > i + 1) ? "), " : ")");
            }
            return builder.ToString();
        }

        public static string ExecuteBatch(SQLiteDataReader reader, string constantToAllFields, params string[] fields)
        {
            var builder = new StringBuilder();

            for (var i = 0; i < reader.FieldCount; i++)
            {
                builder.Append("( ");

                if (!string.IsNullOrWhiteSpace(constantToAllFields))
                {
                    builder.Append("'");
                    builder.Append(constantToAllFields);
                    builder.Append("', ");
                }

                for (var j = 0; j < fields.Length; j++)
                {
                    builder.Append("'");
                    builder.Append(reader[fields[j]]);
                    builder.Append("'");
                    if (fields.Length > j + 1)
                    {
                        builder.Append(", ");
                    }
                    ;
                }

                builder.Append((reader.FieldCount > i + 1) ? "), " : ")");
            }
            return builder.ToString();
        }
    }
}