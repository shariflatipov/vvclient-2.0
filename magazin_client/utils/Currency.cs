﻿using System;
using System.Data.SQLite;
using System.Linq;
using System.Xml.Linq;

namespace vvclient.utils
{
    public class Currency
    {
        public static string GetCurrencies()
        {
            var strings = "";
            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (
                        var command =
                            new SQLiteCommand("select code, value from currencies"))
                    {
                        command.Connection = connection;
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                strings += reader[0] + ": ";
                                strings += reader[1].ToString();
                                strings += "\r\n";
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            return strings;
        }

        public static void SaveCurrencies(XElement currencies)
        {
            try
            {
                var query = from mag in currencies.Elements("currency")
                    select mag;

                foreach (var mg in query)
                {
                    try
                    {
                        var code = mg.Element("code").Value;
                        var rate = mg.Element("rate").Value;

                        if (int.Parse(SQLiteWrapper.ExecuteScalar
                            ("select count(*) from currencies where code = '" + code + "'").ToString()) != 0)
                        {
                            SQLiteWrapper.ExecuteNonQuery
                                ("update currencies set value = '" + rate + "' where code = '" + code + "'");
                        }
                        else
                        {
                            SQLiteWrapper.ExecuteNonQuery("insert into currencies (code, value) values('" + code +
                                                          "', '" + rate + "')");
                        }
                    }
                    catch (Exception genExce)
                    {
                        Logger.Log(genExce, "saveCurrencies() inside the foreach statement: " + mg);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "saveCurrencies ----Invalid XML Answer----");
            }
        }
    }
}