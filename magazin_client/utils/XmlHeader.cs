﻿using System;
using System.Xml.Linq;
using vvclient.settings;

namespace vvclient.utils
{
    public enum UpdateTask
    {
        Get,
        Ok
    }

    public class XMLHeaders
    {
        private string _login;
        private readonly string _dateToSend;
        private XElement _header;
        private XElement _rootElement;

        public XMLHeaders(DateTime dateToSend, string login) {
            _login = login;
            _dateToSend = Utils.FormatDate(dateToSend);

            Init();
        }

        public XMLHeaders()
        {
            _login = MainForm.Login;
            _dateToSend = Utils.GetNowFormated();

            Init();
        }

        public XElement GetRootElements()
        {
            return _rootElement;
        }

        private void Init()
        {
            var password = MyCrypt.GetMd5Hash(MainForm.Pass);
            var hashSum = MyCrypt.GetSha1(_dateToSend + "#" + MainForm.Login + "#" + password);

            _header = new XElement("seller");

            _header.Add(new XAttribute("login", _login));
            _header.Add(new XAttribute("checksum", hashSum));
            _header.Add(new XAttribute("stock", MainForm.Stock));
            _header.Add(new XAttribute("act", 1));
            _header.Add(new XAttribute("date", _dateToSend));
            _header.Add(new XAttribute("version", Config.VERSION));

            _rootElement = new XElement("magazin");
            _rootElement.Add(_header);
        }

        public void OperationType(UpdateTask type)
        {
            var operation = (type == UpdateTask.Get) ? "get" : "ok";
            if (_rootElement.Element("update") == null)
            {
                var update = new XElement("update");
                update.SetValue(operation);
                _rootElement.Add(new XElement(update));
            }
            else
            {
                _rootElement.Element("update").SetValue(operation);
            }
        }

        public void SetAct(int act)
        {
            if (_header.Attribute("act") == null)
            {
                _header.Add(new XAttribute("act", act));
            }
            else
            {
                _header.Attribute("act").SetValue(act);
            }
        }

        public string GetAct()
        {
            return _header.Attribute("act").Value;
        }
    }
}
