﻿using System;
using System.IO;
using System.Windows.Forms;
using vvclient.settings;

namespace vvclient.utils
{
    public enum Level
    {
        UpdateLog = 4,
        Warining = 2,
        Error = 1,
        Info = 3,
        Fatal = 0
    }

    public class Logger
    {
        private static string dt
        {
            get { return "_" + DateTime.Today.ToShortDateString().Replace("/", "_"); }
        }

        public static void Log(Exception ex, string function = "", Level level = Level.Error)
        {
            if (!Config.WriteLogsToFile && level != Level.Fatal)
            {
                return;
            }

            try
            {
                var path = "logs/nonlevel.txt";

                if (level == Level.Error)
                {
                    path = "logs/ERROR" + dt + ".txt";
                }
                else if (level == Level.Warining)
                {
                    path = "logs/WARNING" + dt + ".txt";
                }
                else if (level == Level.Info)
                {
                    path = "logs/INFO" + dt + ".txt";
                }
                else if (level == Level.Fatal)
                {
                    path = "logs/--Fatal--.txt";
                }
                else if (level == Level.UpdateLog)
                {
                    path = "logs/UpdateLog" + dt + ".txt";
                }

                var fs = File.Open(path, FileMode.Append, FileAccess.Write);
                var sw = new StreamWriter(fs);
                sw.WriteLine();
                sw.WriteLine(
                    "-------------------------------------------------------------------------------------------------------");
                sw.WriteLine("Exception: " + ex);
                sw.WriteLine("Target site: " + ex.TargetSite.Name);
                sw.WriteLine("Function and line: " + function);
                sw.WriteLine("Timestamp: " + DateTime.Now);
                sw.WriteLine(
                    "-------------------------------------------------------------------------------------------------------");
                sw.WriteLine();
                sw.Flush();
                fs.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void Log(string data, Level level = Level.UpdateLog)
        {
            if (!Config.WriteLogsToFile && level != Level.Fatal)
            {
                return;
            }
            try
            {
                var path = "logs/nonlevel.txt";

                if (level == Level.Error)
                {
                    path = "logs/ERROR" + dt + ".txt";
                }
                else if (level == Level.Warining)
                {
                    path = "logs/WARNING" + dt + ".txt";
                }
                else if (level == Level.Info)
                {
                    path = "logs/INFO" + dt + ".txt";
                }
                else if (level == Level.Fatal)
                {
                    path = "logs/--Fatal--.txt";
                }
                else if (level == Level.UpdateLog)
                {
                    path = "logs/UpdateLog" + dt + ".txt";
                }

                var fs = File.Open(path, FileMode.Append, FileAccess.Write);
                var sw = new StreamWriter(fs);
                sw.WriteLine();
                sw.WriteLine(
                    "-------------------------------------------------------------------------------------------------------");
                sw.WriteLine(data);
                sw.WriteLine(
                    "-------------------------------------------------------------------------------------------------------");
                sw.WriteLine("Timestamp: " + DateTime.Now);
                sw.WriteLine();
                sw.Flush();
                fs.Close();
            }
            catch
            {
            }
        }

        public static void Log(Exception ex, string otherData, DataGridView dataGrid)
        {
            if (!Config.WriteLogsToFile)
            {
                return;
            }
            try
            {
                var path = "logs/Fatal" + dt + ".txt";

                var fs = File.Open(path, FileMode.Append, FileAccess.Write);
                var sw = new StreamWriter(fs);
                sw.WriteLine();
                sw.WriteLine(
                    "-------------------------------------------------------------------------------------------------------");

                sw.WriteLine("Data in datagrid");

                foreach (DataGridViewRow row in dataGrid.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        sw.Write(cell.OwningColumn.HeaderText + "  ----  " + cell.Value);
                    }
                    sw.WriteLine();
                }

                sw.WriteLine();
                sw.WriteLine("End of data in grid");
                sw.WriteLine();
                sw.WriteLine("Other data");
                sw.WriteLine(otherData);
                sw.WriteLine();
                sw.WriteLine();

                sw.WriteLine("Exception: " + ex);
                sw.WriteLine("Target site: " + ex.TargetSite.Name);
                sw.WriteLine("Timestamp: " + DateTime.Now);
                sw.WriteLine(
                    "-------------------------------------------------------------------------------------------------------");

                sw.Flush();
                fs.Close();
            }
            catch
            {
            }
        }
    }
}