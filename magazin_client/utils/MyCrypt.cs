﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace vvclient.utils
{
    internal class MyCrypt
    {
        /// /// Шифрует строку value
        /// 
        /// Строка которую необходимо зашифровать
        /// Ключ шифрования
        public static string Encrypt(string str, string keyCrypt)
        {
            return Convert.ToBase64String(Encrypt(Encoding.UTF8.GetBytes(str), keyCrypt));
        }

        /// /// Расшифроывает данные из строки value
        /// 
        /// Зашифрованая строка
        /// Ключ шифрования
        /// Возвращает null, если прочесть данные не удалось
        [DebuggerNonUserCode]
        public static string Decrypt(string str, string keyCrypt)
        {
            string result;
            try
            {
                var cs = InternalDecrypt(Convert.FromBase64String(str), keyCrypt);
                var sr = new StreamReader(cs);
                result = sr.ReadToEnd();
                if (sr != null)
                {
                    sr.Dispose();
                }
            }
            catch (CryptographicException ex)
            {
                Logger.Log(ex, "MyCrypt.cs : function => Decrypt()", Level.Warining);
                return null;
            }

            return result;
        }

        private static byte[] Encrypt(byte[] key, string value)
        {
            SymmetricAlgorithm sa = Rijndael.Create();
            using (var ct = sa.CreateEncryptor((new PasswordDeriveBytes(value, null)).GetBytes(16), new byte[16]))
            {
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
                cs.Write(key, 0, key.Length);
                cs.FlushFinalBlock();

                var result = ms.ToArray();
                if (cs != null)
                {
                    cs.Dispose();
                }
                return result;
            }
        }

        private static CryptoStream InternalDecrypt(byte[] key, string value)
        {
            SymmetricAlgorithm sa = Rijndael.Create();
            var ct = sa.CreateDecryptor((new PasswordDeriveBytes(value, null)).GetBytes(16), new byte[16]);

            var ms = new MemoryStream(key);
            return new CryptoStream(ms, ct, CryptoStreamMode.Read);
        }

        public static string GetMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            var md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            var data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            var sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (var i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static string GetSha1(string userPassword)
        {
            return
                BitConverter.ToString(SHA1.Create().ComputeHash(Encoding.Default.GetBytes(userPassword)))
                    .Replace("-", "")
                    .ToLower();
        }
    }
}