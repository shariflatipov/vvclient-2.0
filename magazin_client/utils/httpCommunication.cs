﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace vvclient.utils
{
    internal class HttpCommunication
    {
        public static XElement HttpCommun(XElement xml, int timeout)
        {
            XElement reqResponse;
            var responseFromServer = "";

            if (xml != null)
            {
                Logger.Log("---------------Request from client------------------");
                Logger.Log(xml.ToString());
            }
            try
            {
                /************** POST Request method ****************/

                // Create a request for the URL. 		

                var myHttpWebRequest = (HttpWebRequest) WebRequest.Create("http://" + MainForm.Ip + "/pos_client/api/");

                var postData = xml.ToString();
                var bytes = Encoding.UTF8.GetBytes(postData);


                myHttpWebRequest.Method = "POST";
                myHttpWebRequest.Timeout = timeout;
                myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
                myHttpWebRequest.ContentLength = bytes.Length;
                var newStream = myHttpWebRequest.GetRequestStream();
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();
                var webResp = (HttpWebResponse) myHttpWebRequest.GetResponse();

                if (webResp.StatusCode == HttpStatusCode.OK)
                {
                    var loResponseStream = new StreamReader(webResp.GetResponseStream(), Encoding.UTF8);
                    responseFromServer = loResponseStream.ReadToEnd();
                }

                reqResponse = XElement.Parse(@responseFromServer);
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "HttpCommunication.cs : function => HttpCommun()");
                reqResponse = null;
            }

            Logger.Log("Response from server");
            if (reqResponse != null)
            {
                Logger.Log(reqResponse.ToString());
            }
            else
            {
                Logger.Log("Response was null");
            }

            return reqResponse;
        }

        public static bool HandleResponseFromServer(XElement xmlToSend, string trnID)
        {
            var result = false;
            try
            {
                var response = HttpCommun(xmlToSend, 15000);
                if (response != null)
                {
                    if (response.Elements().Any())
                    {
                        var reqTrn = response.Element("trn").Value;

                        if (response.Element("status").Value.Equals("2"))
                        {
                            //    Transaction.FinishProduct(reqTrn);
                            //} else {
                            if (Transaction.FinishProduct(reqTrn, response.Element("description").Value,
                                Convert.ToInt16(response.Element("status").Value)))
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
            catch (Exception excep)
            {
                Logger.Log(excep, "CreateXML.cs : function => HandleResponseFromServer()");
                Transaction.FinishProduct(trnID, "error", 3);
            }

            return result;
        }
    }
}