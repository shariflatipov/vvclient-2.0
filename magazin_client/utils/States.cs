﻿namespace vvclient.utils
{
    public enum State
    {
        New,
        SendReady,
        Ok,
        Error
    }
}