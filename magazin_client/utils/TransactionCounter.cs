﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace vvclient.utils
{
    public class TransactionCounter
    {
        public static string getTransactionInTurnCount(){
            string result = "";
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                string commandString = "select count(*) from sold_product where status <> 2";
                using (var command = new SQLiteCommand(commandString, conn))
                {
                    conn.Open();
                    result = command.ExecuteScalar().ToString();    
                }
            }

            return result;
        }
    }
}
