﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;

namespace vvclient.utils
{
    public static class Utils
    {
        public const string DateFormat = "yyyy-MM-dd HH:mm:ss";

        public static string KeyToChar(string key)
        {
            switch (key)
            {
                case "NumPad0":
                case "D0":
                    return "0";
                case "NumPad1":
                case "D1":
                    return "1";
                case "NumPad2":
                case "D2":
                    return "2";
                case "NumPad3":
                case "D3":
                    return "3";
                case "NumPad4":
                case "D4":
                    return "4";
                case "NumPad5":
                case "D5":
                    return "5";
                case "NumPad6":
                case "D6":
                    return "6";
                case "NumPad7":
                case "D7":
                    return "7";
                case "NumPad8":
                case "D8":
                    return "8";
                case "NumPad9":
                case "D9":
                    return "9";
                case "Escape":
                    return "esc";
                case "Decimal":
                    return "dec"; // ,
                case "Delete":
                    return "clr";
                case "Back":
                    return "back";
                case "Enter":
                case "Return":
                    return "enter";
                default:
                    return "";
            }
        }

        public static Image GetImageFromPath(string path)
        {
            Image image = null;
            try
            {
                if (path.Contains("jpg") || path.Contains("jpeg") || path.Contains("png"))
                {
                    image = Image.FromFile(path);
                }
                else
                {
                    image = Image.FromFile("img/no_image.jpg");
                }
            }
            catch (Exception ex)
            {
                image = Image.FromFile("img/no_image.jpg");
                Logger.Log(ex, path);
            }
            return image;
        }

        // Calculates barcode check sum from first 12 digits
        public static int CalculateCheckSum(string digits)
        {
            int i;
            var checksum = 0;


            for (i = 1; i < 12; i += 2)
            {
                checksum += Convert.ToInt32(digits.Substring(i, 1));
            }
            checksum *= 3;
            for (i = 0; i < 12; i += 2)
            {
                checksum += Convert.ToInt32(digits.Substring(i, 1));
            }

            return (10 - checksum%10)%10;
        }

        // saves hex string image to the pathToSave
        public static void SaveImageFromHex(string hex, string pathToSave)
        {
            // Call function to Convert the hex data to byte array
            var newByte = ToByteArray(hex);
            var memStream = new MemoryStream(newByte);

            // Save the memorystream to file
            Image.FromStream(memStream).Save(pathToSave);
        }

        // Function converts hex data into byte array
        private static byte[] ToByteArray(string HexString)
        {
            var NumberChars = HexString.Length;

            var bytes = new byte[NumberChars/2];

            for (var i = 0; i < NumberChars; i += 2)
            {
                bytes[i/2] = Convert.ToByte(HexString.Substring(i, 2), 16);
            }
            return bytes;
        }

        public static string FirstCharToUpper(string input)
        {
            if (input.Length > 1)
            {
                return input.First().ToString().ToUpper() + input.Substring(1);
            }
            if (input.Length == 1)
            {
                return input.ToUpper();
            }
            return input;
        }

        public static string FormatDate(DateTime dt) {
            return dt.ToString(DateFormat);
        }

        public static string FormatDate(string dt) {
            var f = DateTime.Parse(dt).ToString(DateFormat);
            return f;
        }

        public static string GetNowFormated() {
            return DateTime.Now.ToString(DateFormat);
        }

        public static string GetId()
        {
            return DateTime.Now.ToString("ddMMyyyyHHmmssff");
        }
    }
}