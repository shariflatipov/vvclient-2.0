﻿using System;
using System.Linq;
using System.Windows.Forms;
using vvclient.utils;

namespace vvclient.nums
{
    public partial class Nums : Form
    {
        protected double _count;

        public Nums(string caption, string stockRemain = "")
        {
            InitializeComponent();
            lblInfo.Text = caption;
            lblStockRemain.Text = stockRemain;
        }

        public double GetQuantity()
        {
            return _count;
        }

        private void CancelClick(object sender, EventArgs e)
        {
            Dispose();
        }

        protected virtual void ConfirmClick(object sender, EventArgs e)
        {
            try
            {
                if (lblCount.Text != null)
                {
                    _count = double.Parse(lblCount.Text);
                }
                DialogResult = DialogResult.OK;
            }
            catch (Exception)
            {
                _count = 0;
            }
            Hide();
        }

        private void BtnClick(object sender, EventArgs e)
        {
            var num = (Button) sender;
            lblCount.Text += num.Name.Substring(4);
            label1.Focus();
        }

        private void ClearClick(object sender, EventArgs e)
        {
            lblCount.Text = "";
            label1.Focus();
        }

        private void DotClick(object sender, EventArgs e)
        {
            if (!lblCount.Text.Contains('.'))
            {
                lblCount.Text += ".";
            }
            label1.Focus();
        }

        private void BackClick(object sender, EventArgs e)
        {
            if (lblCount.Text.Length >= 1)
            {
                lblCount.Text = lblCount.Text.Substring(0, lblCount.Text.Length - 1);
            }
            label1.Focus();
        }

        private void NumsKeyDown(object sender, KeyEventArgs e)
        {
            var key = "";
            key = Utils.KeyToChar(e.KeyCode.ToString());

            if (key.Equals("dec"))
            {
                // ,
                DotClick(null, null);
            }
            else if (key.Equals("esc"))
            {
                CancelClick(null, null);
            }
            else if (key.Equals("clr"))
            {
                ClearClick(null, null);
            }
            else if (key.Equals("back"))
            {
                BackClick(sender, e);
            }
            else if (key.Equals("enter"))
            {
                ConfirmClick(sender, e);
            }
            else
            {
                lblCount.Text += key;
            }
        }
    }
}