﻿using System;

namespace vvclient.nums
{
    public class NumsReturn : Nums
    {
        private readonly double _maxQuantity;

        public NumsReturn(double quantity) : base("Количество")
        {
            _maxQuantity = quantity;
        }

        protected override void ConfirmClick(object sender, EventArgs e)
        {
            double lQuantity;
            if (double.TryParse(lblCount.Text, out lQuantity))
            {
                // Return quantity if received quantity smaller or equals to max quantity
                if (lQuantity > 0 && lQuantity <= _maxQuantity)
                {
                    _count = lQuantity;
                }
            }
            Hide();
        }
    }
}