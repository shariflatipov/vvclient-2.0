﻿using System;
using vvclient.utils;

namespace vvclient.nums
{
    public class ChangePack : Nums
    {
        private readonly double _packCount;

        public ChangePack(double packCount) :
            base("Количество в пачке" + "\r\n" + packCount)
        {
            _packCount = packCount;
        }

        protected override void ConfirmClick(object sender, EventArgs e)
        {
            double count = 0;
            try
            {
                count = Convert.ToDouble(lblCount.Text);
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "ChangePack.cs : function => ConfirmClick()");
            }

            if (count > 0)
            {
                _count = count*100/_packCount/100;
            }
            Dispose();
        }
    }
}