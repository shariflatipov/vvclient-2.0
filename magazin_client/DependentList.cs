﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;
using vvclient.discount;
using vvclient.nums;
using vvclient.product;
using vvclient.settings;
using vvclient.utils;

namespace vvclient
{
    public partial class DependentList : Form
    {
        private readonly Discount _discount;
        private readonly ProductCart _pc;
        private Product _product;
        private string productId;
        private bool _closeImidiate;

        public DependentList(string id, ProductCart pc, Discount discount, bool closeImidiate = false)
        {
            InitializeComponent();
            productId = id;
            _pc = pc;
            _discount = discount;
            _closeImidiate = closeImidiate;
        }

        private void DependentList_Load(object sender, EventArgs e)
        {
            GetListContent(depImgList, "DepList", productId);
            depList.LargeImageList = depImgList;
            depList.MouseClick += depList_MouseClick;
        }

        public Product GetProduct()
        {
            return _product;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            depList.Clear();
            depImgList.Images.Clear();
            GetListContent(depImgList, "DepList", productId);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (depList.Items.Count > 0)
            {
                depList.EnsureVisible(depList.Items.Count - 1);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (depList.Items.Count > 0)
            {
                depList.EnsureVisible(0);
            }
        }

        private void GetListContent(ImageList im, string commandType, string depId = "")
        {
            var commandText = "";
            switch (commandType)
            {
                case "MainList":
                    commandText = "SELECT product_id, product_name, product_image " +
                                  " FROM product where is_list = 1";
                    break;
                case "DepList":
                    commandText = "SELECT product_id, product_name, product_image, batch " +
                                  " FROM  product where dependent = '" + depId + "' order by product_name";
                    break;
            }
            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(commandText, connection))
                    {
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var i = 0;
                            im.ImageSize = new Size(100, 100);

                            while (reader.Read())
                            {
                                im.Images.Add(Utils.GetImageFromPath(reader[2].ToString()));
                                if (commandType.Equals("MainList"))
                                {
                                    depList.Items.Add(reader[0].ToString(), reader[1].ToString(), i);
                                }
                                else if (commandType.Equals("DepList"))
                                {
                                    depList.Items.Add(reader[0].ToString(), reader[1] + "#" + reader[3], i);
                                }
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "SQLCommunication.cs : function => GetListContent()");
            }
        }

        public static void GetListContent(ListView depList, ImageList im, string commandType, string depId = "")
        {
            try
            {
                var commandText = "";
                switch (commandType)
                {
                    case "MainList":
                        commandText = "SELECT product_id, product_name, product_image " +
                                      " FROM product where is_list = 1";
                        break;
                    case "DepList":
                        commandText =
                            "SELECT product_id, product_name, product_image " +
                            " FROM  product where dependent = '" + depId + "' order by product_name";
                        break;
                }

                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(commandText, connection))
                    {
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var i = 0;
                            im.ImageSize = new Size(100, 100);

                            while (reader.Read())
                            {
                                im.Images.Add(Utils.GetImageFromPath(reader[2].ToString()));
                                depList.Items.Add(reader[0].ToString(), reader[1].ToString(), i);
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "SQLCommunication.cs : function => GetListContent()");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            depList.Clear();
            var commandText = "";
            if (!chkEverywhere.Checked)
            {
                commandText = "SELECT product_id, product_name, product_image " +
                              " FROM product where product_name like @name and dependent = @product or product_name like @tolowername and dependent = @product";
            }
            else
            {
                commandText = "SELECT product_id, product_name, product_image " +
                              " FROM product where product_name like @name or product_name like @tolowername limit 25";
            }
            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(commandText, connection))
                    {
                        command.Parameters.Add(new SQLiteParameter("@name", "%" + textBox1.Text + "%"));
                        command.Parameters.Add(new SQLiteParameter("@tolowername",
                            "%" + Utils.FirstCharToUpper(textBox1.Text) + "%"));
                        command.Parameters.Add(new SQLiteParameter("@product", productId));
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            var i = 0;
                            depImgList.ImageSize = new Size(100, 100);

                            while (reader.Read())
                            {
                                depImgList.Images.Add(Utils.GetImageFromPath(reader[2].ToString()));
                                depList.Items.Add(reader[0].ToString(), reader[1].ToString(), i);
                                i++;
                            }
                        }
                        command.Parameters.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "SQLCommunication.cs : function => GetListContent()");
            }
        }

        private void chkEverywhere_CheckedChanged(object sender, EventArgs e)
        {
            textBox1_TextChanged(sender, e);
        }

        private void depList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                depList_MouseClick(sender, null);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                Dispose();
            }
        }

        private void depList_MouseClick(object sender, MouseEventArgs e)
        {
            var a = (ListView) sender;
            var id = a.SelectedItems[0].Name;

            // Если есть элементы у которых существуют дочерние элементы
            // 
            if (Product.GetDependentCount(id) > 0)
            {
                depList.Clear();
                depImgList.Images.Clear();
                productId = id;
                GetListContent(depImgList, "DepList", id);
            }
            else
            {
                // TODO rewrite method if count of dependent is 0 then show choose quantity dialog 
                // else change this to dependent of this product
                _product = Product.SetProductById(a.SelectedItems[0].Name, "0", true);
                if (_product != null)
                {
                    var stockRemain = "";
                    if (Config.LiveStockRemain)
                    {
                        stockRemain = Product.GetStockRemain(_product);
                    }
                    var quantity = new Nums(_product.GetName() + "\r\n" + _product.GetPrice() + " c.", stockRemain);
                    if (quantity.ShowDialog() == DialogResult.OK)
                    {
                        _product.SetQuantity(quantity.GetQuantity());
                        _product.SetDiscount(_product.GetDiscount() +
                                             CategoriesUserDiscount.GetDiscountForProductAndUser(_product, _discount));

                        _pc.AddItem(_product, true);
                    }
                }
                else
                {
                    MessageBox.Show("Произошла ошибка при выборе товара");
                }
            }

            if (_closeImidiate)
            {
                Dispose();
            }
        }
    }
}