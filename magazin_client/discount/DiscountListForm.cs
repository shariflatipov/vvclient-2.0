﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using vvclient.utils;

namespace vvclient.discount
{
    public partial class DiscountListForm : Form
    {
        public DiscountListForm()
        {
            InitializeComponent();
        }

        public Discount discount { get; set; }

        private void DiscountListForm_Load(object sender, EventArgs e)
        {
            var discounts = Discount.GetList();
            FillDataGrid(discounts);
            comboBox1.SelectedIndex = 0;
        }

        private void FillDataGrid(List<Discount> list)
        {
            dataGridView1.Rows.Clear();

            foreach (var discount in list)
            {
                dataGridView1.Rows.Add(discount.Id, discount.Name, discount.Surname, discount.Patronymic, discount.Phone,
                    discount.DiscountCode, discount.Percents);
            }
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var dForm = new DiscountAddEditForm(dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString());
            dForm.ShowDialog();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            var dForm = new DiscountAddEditForm();
            if (dForm.ShowDialog() == DialogResult.OK)
            {
                Dispose();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                discount = Discount.Get(dataGridView1.SelectedRows[0].Cells["DiscountCode"].Value.ToString());
                discount = Discount.SetDiscountsLimit(discount);
                if (discount != null) DialogResult = DialogResult.OK;
            }
            Hide();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var list = Discount.GetList(GetDiscountQuery(textBox1.Text));
            FillDataGrid(list);
        }

        private string GetDiscountQuery(string text)
        {
            var limit = " LIMIT 20 ";
            if (comboBox1.SelectedIndex == 0)
            {
                var result = text.Split(' ');
                if (result.Length == 1)
                {
                    return "select * from discount where name " +
                           " like '" + Utils.FirstCharToUpper(text) + "%' or " +
                           " surname like '" + Utils.FirstCharToUpper(text) + "%' " + limit;
                }
                else if (result.Length == 2)
                {
                    return "select * from discount where name like '%"
                           + result[0] + "%' and surname like '%"
                           + result[1] + "%'" + limit;
                }
                else
                {
                    return "select * from discount where name like '%" + result[0] + "%' and surname like '%" + result[1] +
                       "%' and patronymic like '%" + result[2] + "%'" + limit;
                }
            }
            else if (comboBox1.SelectedIndex == 1)
            {
                return "select * from discount where phone like '%" + text + "%'" + limit;
            }
            else if (comboBox1.SelectedIndex == 2)
            {
                return "select * from discount where discount_code like '%" + text + "%'" + limit;
            }
            else
            {
                return "select * from discount" + limit;
            }
        }
    }
}