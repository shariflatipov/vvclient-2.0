﻿using System.Xml.Linq;
using vvclient.utils;

namespace vvclient.discount
{
    public class RemoteDiscount
    {
        private string _barcode;
        private Discount _discount;
        private XMLHeaders _header;
        private int _error = 0;

        public RemoteDiscount(string barcode)
        {
            _barcode = barcode;
            _header = new XMLHeaders();
            _discount = Discount.Get(barcode);
            getRemoteDiscount();
        }


        private void getRemoteDiscount()
        {
            var vip_card = new XElement("vip_card");
            vip_card.Add(new XAttribute("barcode", _barcode));
            _header.SetAct(27);
            _header.GetRootElements().Add(vip_card);
            XElement result = HttpCommunication.HttpCommun(_header.GetRootElements(), 10000);
            if (result != null)
            {
                if (_discount != null)
                {
                    if (result.Element("coupon_code") != null)
                    {
                        _discount.Coupon = (string)result.Element("coupon_code").Value;
                    }
                    if (result.Element("discount_percent") != null)
                    {
                        _discount.NewPercent = (AllowedPercents)int.Parse(result.Element("discount_percent").Value);
                    }
                    else
                    {
                        _error = 1;
                    }
                }
                else
                {
                    _error = 1;
                }
            }
            else
            {
                _error = 1;
            }
        }

        
        public Discount GetDiscount()
        {
            return _discount;
        }

        // If there are no errors then return 0
        public int GetError()
        {
            return _error;
        }
    }
}
