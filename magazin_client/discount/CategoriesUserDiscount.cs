﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Xml.Linq;
using vvclient.product;
using vvclient.utils;

namespace vvclient.discount
{
    public class CategoriesUserDiscount
    {
        public string UserDiscountCode { get; set; }
        public Dictionary<int, float> ProductCategories { get; set; }

        public bool Save()
        {
            try
            {
                foreach (var values in ProductCategories)
                {
                    SQLiteWrapper.ExecuteNonQuery("insert into categories_user_discount " +
                                                  "(user_discount, category_id, discount_rate) " +
                                                  "values('" + UserDiscountCode + "', '" + values.Key + "', '" +
                                                  values.Value + "'");
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static void GetUserDiscounts()
        {
            var header = new XMLHeaders();
            header.SetAct(21);
            var categoryDiscount = new XElement("category_discount", "get");
            header.GetRootElements().Add(categoryDiscount);
            var result = HttpCommunication.HttpCommun(header.GetRootElements(), 20000);

            if (HandleUserDiscountResponse(result))
            {
                categoryDiscount.Value = "ok";
                HttpCommunication.HttpCommun(header.GetRootElements(), 20000);
            }
        }

        /*
            <?xml version="1.0" encoding="utf-8"?>
            <magazin>
                <description>OK</description>
                <status>2</status>
                <people category="4" discount="9.0" discount_code="2200000000880"/>
                <people category="2" discount="5.0" discount_code="2200000000880"/>
                <people category="3" discount="0.0" discount_code="2200000000880"/>
                <people category="1" discount="0.0" discount_code="2200000000880"/>
            </magazin>
         */

        private static bool HandleUserDiscountResponse(XElement result)
        {
            if (result == null) return false;
            try
            {
                var people = from p in result.Elements("people")
                    select p;

                // if there is at list one people(update)
                var flag = false;
                foreach (var person in people)
                {
                    flag = true;
                    float discount;
                    int category;
                    var dc = person.Attribute("discount_code").Value;
                    float.TryParse(person.Attribute("discount").Value, out discount);
                    int.TryParse(person.Attribute("category").Value, out category);

                    using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                    {
                        using (var cmd = new SQLiteCommand())
                        {
                            cmd.Connection = conn;
                            conn.Open();
                            var command = "";
                            try
                            {
                                command = "insert into categories_user_discount " +
                                          " (user_discount, category_id, discount_rate) values " +
                                          " (@user, @category, @discount)";
                                cmd.CommandText = command;
                                cmd.Parameters.Add(new SQLiteParameter("@user", dc));
                                cmd.Parameters.Add(new SQLiteParameter("@category", category));
                                cmd.Parameters.Add(new SQLiteParameter("@discount", discount));
                                cmd.ExecuteNonQuery();
                            }
                            catch (Exception)
                            {
                                command = "update categories_user_discount set " +
                                          " user_discount=@user, category_id=@category, discount_rate=@discount";
                                cmd.CommandText = command;
                                cmd.ExecuteNonQuery();
                            }
                            finally
                            {
                                cmd.Parameters.Clear();
                            }
                        }
                    }
                }
                return flag;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static CategoriesUserDiscount GetDiscount(string user_code)
        {
            CategoriesUserDiscount result = null;
            using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var command = new SQLiteCommand(connection))
                {
                    connection.Open();

                    command.CommandText = "select * from categories_user_discount where user_discount = @user";
                    command.Parameters.Add(new SQLiteParameter("@user", user_code));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            result = new CategoriesUserDiscount();

                            while (reader.Read())
                            {
                                result.UserDiscountCode = reader["user_discount"].ToString();
                                result.ProductCategories.Add(int.Parse(reader["category_id"].ToString()),
                                    float.Parse(reader["discount_rate"].ToString()));
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static float GetDiscountForProductAndUser(Product product, Discount user_discount)
        {
            float result = 0;

            if (user_discount == null || product == null) return 0;
            if (product.GetCategories() == null || product.GetCategories().Count < 1) return 0;
            var categories = string.Join(",", product.GetCategories());

            using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var command = new SQLiteCommand(connection))
                {
                    try
                    {
                        connection.Open();

                        command.CommandText = "select discount_rate from categories_user_discount where "
                                              + " user_discount = '" + user_discount.DiscountCode
                                              + "' and category_id in (" + categories +
                                              ") order by discount_rate desc limit 1";
                        float.TryParse(command.ExecuteScalar().ToString(), out result);
                        //float.TryParse(command.ExecuteScalar().ToString(), out result);
                    }
                    catch (Exception e)
                    {
                        Logger.Log(e.Message);
                    }
                }
            }
            return result;
        }
    }
}