﻿using System.Collections.Generic;
using vvclient.product;

namespace vvclient.discount
{
    class DiscountZeroCalculator : IDiscountCalculator
    {
        public double calculate(double discountValue, List<Product> products)
        {
            return 0;
        }
    }
}
