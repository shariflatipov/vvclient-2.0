﻿namespace vvclient.discount {
    partial class CalculateDiscount {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.lblSelectDiscountHolder = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(373, 167);
            this.label1.TabIndex = 0;
            this.label1.Text = "Проведите карту";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblSelectDiscountHolder
            // 
            this.lblSelectDiscountHolder.BackColor = System.Drawing.Color.White;
            this.lblSelectDiscountHolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectDiscountHolder.Location = new System.Drawing.Point(391, 9);
            this.lblSelectDiscountHolder.Name = "lblSelectDiscountHolder";
            this.lblSelectDiscountHolder.Size = new System.Drawing.Size(253, 167);
            this.lblSelectDiscountHolder.TabIndex = 1;
            this.lblSelectDiscountHolder.Text = "Выберите держателя карты";
            this.lblSelectDiscountHolder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSelectDiscountHolder.Click += new System.EventHandler(this.lblSelectDiscountHolder_Click);
            // 
            // CalculateDiscount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(656, 185);
            this.Controls.Add(this.lblSelectDiscountHolder);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "CalculateDiscount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CalculateDiscount";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CalculateDiscount_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSelectDiscountHolder;
    }
}