﻿using System;
using System.Windows.Forms;

namespace vvclient.discount
{
    public partial class DiscountAddEditForm : Form
    {
        private readonly Discount _discount;
        private readonly bool _flagEdit;

        public DiscountAddEditForm()
        {
            InitializeComponent();
        }

        public DiscountAddEditForm(string discountCode)
        {
            _flagEdit = true;
            InitializeComponent();

            _discount = Discount.Get(discountCode);

            txtName.Text = _discount.Name;
            txtDiscountCode.Text = _discount.DiscountCode;
            txtPatronymic.Text = _discount.Patronymic;
            txtPhone.Text = _discount.Phone;
            txtSurname.Text = _discount.Surname;

            var selectedIndex = 0;

            switch (_discount.Percents)
            {
                case AllowedPercents.P0:
                    selectedIndex = 0;
                    break;
                case AllowedPercents.P2:
                    selectedIndex = 1;
                    break;
                case AllowedPercents.P5:
                    selectedIndex = 2;
                    break;
                case AllowedPercents.P10:
                    selectedIndex = 3;
                    break;
                case AllowedPercents.P15:
                    selectedIndex = 4;
                    break;
                case AllowedPercents.P20:
                    selectedIndex = 5;
                    break;
            }

            cmbPercent.SelectedIndex = selectedIndex;
            txtEmail.Text = _discount.Email;
            cmbGender.SelectedIndex = cmbGender.Items.IndexOf(_discount.Gender);
            cmbWearSize.SelectedIndex = cmbWearSize.Items.IndexOf(_discount.WearSize);
            cmbShoesSize.SelectedIndex = cmbShoesSize.Items.IndexOf(_discount.ShoesSize);
            dateTimePicker1.Value = _discount.Birthdate;
            txtAddress.Text = _discount.Address;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_flagEdit)
            {
                _discount.Name = txtName.Text;
                _discount.DiscountCode = txtDiscountCode.Text;
                _discount.Patronymic = txtPatronymic.Text;
                _discount.Phone = txtPhone.Text;
                _discount.Surname = txtSurname.Text;
                _discount.Percents = (AllowedPercents) int.Parse(cmbPercent.SelectedItem.ToString());
                _discount.Email = txtEmail.Text;
                _discount.Address = txtAddress.Text;

                if(chkBirthdate.Checked)
                {
                    _discount.Birthdate = dateTimePicker1.Value;
                }                 
                if (cmbGender.SelectedItem != null)
                {
                    _discount.Gender = cmbGender.SelectedItem.ToString();
                }
                if (cmbWearSize.SelectedItem != null)
                {
                    _discount.WearSize = cmbWearSize.SelectedItem.ToString();
                }
                if (cmbShoesSize.SelectedItem != null)
                {
                    _discount.ShoesSize = cmbShoesSize.SelectedItem.ToString();
                }

                if (IsValid())
                {
                    if (_discount.Update())
                    {
                        Dispose();
                    }
                    else
                    {
                        MessageBox.Show("Не возможно обновить запись");
                    }
                }
                else
                {
                    MessageBox.Show("Заполните все поля!");
                }
            }
            else
            {
                if (IsValid())
                {
                    var discount = new Discount
                    {
                        Name = txtName.Text,
                        DiscountCode = txtDiscountCode.Text,
                        Patronymic = txtPatronymic.Text,
                        Phone = txtPhone.Text,
                        Surname = txtSurname.Text,
                        Percents = (AllowedPercents) int.Parse(cmbPercent.SelectedItem.ToString()),
                        Email = txtEmail.Text,
                        Address = txtAddress.Text
                    };

                    if(chkBirthdate.Checked)
                    {
                        discount.Birthdate = dateTimePicker1.Value;
                    }                 

                    if (cmbGender.SelectedItem != null) {
                        discount.Gender = cmbGender.SelectedItem.ToString();
                    }
                    else
                    {
                        discount.Gender = "";
                    }
                    if (cmbGender.SelectedItem != null) {
                        discount.WearSize = cmbWearSize.SelectedItem.ToString();
                    }
                    else
                    {
                        discount.WearSize = "";
                    }
                    if (cmbShoesSize.SelectedItem != null)
                    {
                        discount.ShoesSize = cmbShoesSize.SelectedItem.ToString();
                    }
                    else
                    {
                        discount.ShoesSize = "";
                    }

                    if (discount.Save())
                    {
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                        Dispose();
                    }
                    else
                    {
                        MessageBox.Show("Данный пользователь существует");
                    }
                }
                else
                {
                    MessageBox.Show("Заполните все поля!");
                }
            }
        }

        private bool IsValid()
        {
            return (!string.IsNullOrWhiteSpace(txtDiscountCode.Text) &&
                    !string.IsNullOrWhiteSpace(txtName.Text) &&
                    !string.IsNullOrWhiteSpace(cmbPercent.Text) &&
                    !string.IsNullOrWhiteSpace(txtPhone.Text) &&
                    !string.IsNullOrWhiteSpace(txtSurname.Text) &&
                    !string.IsNullOrWhiteSpace(txtAddress.Text)
                    );
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void DiscountAddEditForm_Load(object sender, EventArgs e)
        {

        }

        private void chkBirthdate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePicker1.Visible = chkBirthdate.Checked;
        }
    }
}