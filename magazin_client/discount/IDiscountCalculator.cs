﻿using System.Collections.Generic;
using vvclient.product;

namespace vvclient
{
    internal interface IDiscountCalculator
    {
        double calculate(double discountValue, List<Product> products);
    }
}