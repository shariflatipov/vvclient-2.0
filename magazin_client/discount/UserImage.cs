﻿using System.Windows.Forms;
using vvclient.utils;

namespace vvclient.discount
{
    public partial class UserImage : Form
    {
        public UserImage(string path)
        {
            InitializeComponent();
            SetPicture(path);
        }

        private void SetPicture(string path)
        {
            path = "media/people/" + path + ".jpg";
            pictureBox1.Image = Utils.GetImageFromPath(path);
        }
    }
}