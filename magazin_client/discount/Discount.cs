﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using System.Xml.Linq;
using vvclient.utils;

namespace vvclient.discount
{
    public enum AllowedPercents
    {
        P0 = 0,
        P2 = 2,
        P5 = 5,
        P10 = 10,
        P15 = 15,
        P20 = 20,
        P25 = 25,
        P30 = 30,
        P35 = 35,
        P40 = 40
    }


    public class Discount
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string Phone { get; set; }
        public string DiscountCode { get; set; }
        public AllowedPercents Percents { get; set; }
        public double Capital { get; set; }
        //private const int State = 0;
        public double AvailableLimit { get; set; }
        public double DebtLimit { get; set; }
        public double Saldo { get; set; }
        public string Email { get; set; }
        public string WearSize { get; set; }
        public string ShoesSize { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public DateTime Birthdate { get; set; }

        public string Coupon { get; set; }

        public AllowedPercents NewPercent { get; set; }

        public bool Save()
        {
            try
            {
                SQLiteWrapper.ExecuteNonQuery("insert into discount " +
                                              "(name, surname, patronymic, phone, discount_code, " + 
                                              " [percent], gender, email, wear_size, shoes_size, address, birthdate, state, summa) " +
                                              "values('" + Name + "', " + 
                                                      " '" + Surname + "', " + 
                                                      " '" + Patronymic + "', " + 
                                                      " '" + Phone + "', " +
                                                      " '" + DiscountCode + "', " + 
                                                      " '" + (int) Percents + "', " +
                                                      " '" + Gender + "', " +
                                                      " '" + Email + "', " +
                                                      " '" + WearSize + "', " +
                                                      " '" + ShoesSize + "', " +
                                                      " '" + Address + "', " +
                                                      " '" + Birthdate.ToString("yyyy-MM-dd") + "', " +
                                                      " 0, '"
                                                      + Capital + "')");
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "Create new discount holder");
                return false;
            }
            return true;
        }

        public static Discount Get(string discountCode)
        {
            Discount discount = null;
            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (
                        var command =
                            new SQLiteCommand("select * from discount where discount_code = " + 
                                "'" + discountCode + "'"))
                    {
                        command.Connection = connection;
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                discount = new Discount
                                {
                                    Id = reader[0].ToString(),
                                    Name = reader["name"].ToString(),
                                    Surname = reader["surname"].ToString(),
                                    Patronymic = reader["patronymic"].ToString(),
                                    Phone = reader["phone"].ToString(),
                                    DiscountCode = reader["discount_code"].ToString(),
                                    Percents = (AllowedPercents)int.Parse(reader["percent"].ToString()),
                                    Capital = double.Parse(reader["summa"].ToString()),
                                    Email = reader["email"].ToString(),
                                    Gender = reader["gender"].ToString(),
                                    ShoesSize = reader["shoes_size"].ToString(),
                                    WearSize = reader["wear_size"].ToString(),
                                    Address = reader["address"].ToString()
                                };

                                DateTime birthdate = new DateTime();
                                if (DateTime.TryParse(reader["birthdate"].ToString(), out birthdate)) {
                                    discount.Birthdate = birthdate;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Log(exception, "Discount => Get");
            }
            return discount;
        }

        public static List<Discount> GetList()
        {
            return GetList("select * from discount LIMIT 10");
        }

        public static List<Discount> GetList(string query)
        {
            var results = new List<Discount>();

            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(query, connection))
                    {
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var discount = new Discount
                                {
                                    Id = reader[0].ToString(),
                                    Name = reader["name"].ToString(),
                                    Surname = reader["surname"].ToString(),
                                    Patronymic = reader["patronymic"].ToString(),
                                    Phone = reader["phone"].ToString(),
                                    DiscountCode = reader["discount_code"].ToString(),
                                    Percents = (AllowedPercents)int.Parse(reader["percent"].ToString()),
                                    Capital = double.Parse(reader["summa"].ToString()),
                                    Email = reader["email"].ToString(),
                                    Gender = reader["gender"].ToString(),
                                    ShoesSize = reader["shoes_size"].ToString(),
                                    WearSize = reader["wear_size"].ToString(),
                                    Address = reader["address"].ToString(),
                                };

                                DateTime birthdate = new DateTime();
                                if (DateTime.TryParse(reader["birthdate"].ToString(), out birthdate)) {
                                    discount.Birthdate = birthdate;
                                }

                                results.Add(discount);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Log(exception, "Discount => GetList");
            }

            return results;
        }

        public bool Update()
        {
            try
            {
                var commandText = "update discount set " +
                                  "name = '" + Name + "', " +
                                  "surname='" + Surname + "', " +
                                  "patronymic='" + Patronymic + "', " +
                                  "phone='" + Phone + "', " +
                                  "discount_code='" + DiscountCode + "', " +
                                  "[percent]='" + (int) Percents + "', " +
                                  "email='" + Email + "', " +
                                  "gender='" + Gender + "', " +
                                  "birthdate='" + Birthdate.ToString("yyyy-MM-dd") + "', " +
                                  "wear_size='" + WearSize + "', " +
                                  "shoes_size='" + ShoesSize + "', " +
                                  "address='" + Address + "', " +
                                  "state=0," +
                                  "summa='" + Capital + "' " +
                                  "where id='" + Id + "'";
                SQLiteWrapper.ExecuteNonQuery(commandText);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static List<Discount> GetUnfinishedStates()
        {
            var results = new List<Discount>();

            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (
                        var command = new SQLiteCommand("select * from discount where state = 0", connection))
                    {
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var discount = new Discount
                                {
                                    Id = reader[0].ToString(),
                                    Name = reader["name"].ToString(),
                                    Surname = reader["surname"].ToString(),
                                    Patronymic = reader["patronymic"].ToString(),
                                    Phone = reader["phone"].ToString(),
                                    DiscountCode = reader["discount_code"].ToString(),
                                    Percents = (AllowedPercents)int.Parse(reader["percent"].ToString()),
                                    Capital = double.Parse(reader["summa"].ToString()),
                                    Email = reader["email"].ToString(),
                                    Gender = reader["gender"].ToString(),
                                    ShoesSize = reader["shoes_size"].ToString(),
                                    WearSize = reader["wear_size"].ToString(),
                                    Address = reader["address"].ToString(),
                                };
                                DateTime birthdate = new DateTime();
                                if (DateTime.TryParse(reader["birthdate"].ToString(), out birthdate)) {
                                    discount.Birthdate = birthdate;
                                }

                                results.Add(discount);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Log(exception, "Discount => GetUnfinishedStates");
            }

            return results;
        }

        public static bool FinishState(string id, XElement response)
        {
            try
            {
                if (response.Elements().Any())
                {
                    var xElement = response.Element("status");
                    if (xElement != null && xElement.Value.Equals("2"))
                    {
                        try
                        {
                            SQLiteWrapper.ExecuteNonQuery("update discount set state=1 where id='" + id + "'");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception excep)
            {
                Logger.Log(excep, "CreateXML.cs : function => HandleResponseFromServer()");
                return false;
            }
            return true;
        }

        public static XElement Serialize(Discount discount)
        {
            // <magazin>
            // <seller login="sm1kassa2" stock="kassa2" 
            //date="2013-07-03 15:02:25" checksum="93ddde78c632af5b550f45dd1be4e1d35192" act="4"></seller>
            // <discount  first_name="Бахтиёр" 
            //last_name="Муротов"  patronymic="Маруфович" phone="9277535" discount_code="00002354" percent="20"/> 
            //</magazin>

            var dateToSend = DateTime.Now.ToString(Utils.DateFormat);

            var password = MyCrypt.GetMd5Hash(MainForm.Pass);
            var hashSum = MyCrypt.GetSha1(dateToSend + "#" + MainForm.Login + "#" + password);

            var seller = new XElement("seller");
            seller.Add(new XAttribute("login", MainForm.Login));
            seller.Add(new XAttribute("checksum", hashSum));
            seller.Add(new XAttribute("stock", MainForm.Stock));
            seller.Add(new XAttribute("act", 4));
            seller.Add(new XAttribute("date", dateToSend));

            var element = new XElement("discount");
            element.Add(new XAttribute("first_name", discount.Name));
            element.Add(new XAttribute("last_name", discount.Surname));
            element.Add(new XAttribute("patronymic", discount.Patronymic));
            element.Add(new XAttribute("phone", discount.Phone));
            element.Add(new XAttribute("discount_code", discount.DiscountCode));
            element.Add(new XAttribute("percent", (int) discount.Percents));
            element.Add(new XAttribute("capital", discount.Capital));
            element.Add(new XAttribute("email", discount.Email));
            element.Add(new XAttribute("gender", discount.Gender));
            element.Add(new XAttribute("wear_size", discount.WearSize));
            element.Add(new XAttribute("shoes_size", discount.ShoesSize));
            element.Add(new XAttribute("address", discount.Address));
            element.Add(new XAttribute("birthday", discount.Birthdate.ToString("yyyy-MM-dd")));

            var magazin = new XElement("magazin");
            magazin.Add(new XElement(seller));
            magazin.Add(element);
            return magazin;
        }

        public void ChangeSum(double sum)
        {
            Capital += sum;

            /*if (Capital >= 2000 && Capital <= 2999) {
                Percents = AllowedPercents.P5;
            } else if (Capital >= 3000 && Capital <= 3999) {
                Percents = AllowedPercents.P10;
            } else if (Capital >= 4000 && Capital <= 5999) {
                Percents = AllowedPercents.P15;
            } else if (Capital > 5999) {
                Percents = AllowedPercents.P20;
            }*/
            Update();
        }

        public static bool HandleRecievedDiscountFromServer(XElement discounts)
        {
            if (discounts == null) return false;

            var UpdateHandled = false;

            try
            {
                var query = from mag in discounts.Elements("discount")
                    select mag;

                foreach (var mg in query)
                {
                    try
                    {
                        SQLiteWrapper.ExecuteNonQuery("insert into discount " +
                                                      "(name, surname, patronymic, phone, discount_code, " + 
                                                      "[percent], address, " + 
                                                      " state, summa) " +
                                                      "values('" + mg.Attribute("first_name").Value + "', " + 
                                                      "'" + mg.Attribute("last_name").Value + "', " + 
                                                      "'" + mg.Attribute("patronymic").Value + "', " + 
                                                      "'" + mg.Attribute("phone").Value + "', " +
                                                      "'" + mg.Attribute("discount_code").Value + "', " + 
                                                      "'" + mg.Attribute("percent").Value + "', " +
                                                      "'" + mg.Attribute("address").Value + "', " +
                                                      " 1, '0')");


                        // Trying to download product image otherwise set image to no_image.jpg
                        try
                        {
                            using (var webClient = new WebClient())
                            {
                                if (!File.Exists(mg.Attribute("image").Value))
                                {
                                    webClient.DownloadFile("http://" + MainForm.Ip + "/" + "media/people/" + mg.Attribute("image").Value + ".jpg",
                                        "media/people/" + mg.Attribute("discount_code").Value + ".jpg");
                                }
                            }
                        }
                        catch (Exception)
                        {
                            mg.Attribute("image").Value = "img/no_image.jpg";
                        }

                        //try
                        //{
                        //    Utils.SaveImageFromHex(mg.Attribute("image").Value,
                        //        "media/people/" + mg.Attribute("discount_code").Value + ".jpg");
                        //}
                        //catch (Exception e)
                        //{
                        //    Logger.Log(e, "cannot save image for person: " + mg.Attribute("discount_code").Value);
                        //}
                    }
                    catch (Exception ex)
                    {
                        SQLiteWrapper.ExecuteNonQuery("update discount set " + 
                            "name='" + mg.Attribute("first_name").Value + "', " + 
                            "surname='" + mg.Attribute("last_name").Value + "', " + 
                            "patronymic='" + mg.Attribute("patronymic").Value + "', " + 
                            "phone='" + mg.Attribute("phone").Value + "', " + 
                            "address='" + mg.Attribute("address").Value + "', " + 
                            "[percent]='" + mg.Attribute("percent").Value + "' " +
                            "where discount_code = '" + mg.Attribute("discount_code").Value + "'");
                    }
                }
                UpdateHandled = true;
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "Chrone.cs : function => HandleRecievedDiscountFromServer ----Invalid XML Answer----",
                    Level.Fatal);
            }
            return UpdateHandled;
        }

        // Get discount limits needs to be rafactored ))
        public static Discount SetDiscountsLimit(Discount discount)
        {
            var headers = new XMLHeaders();
            headers.SetAct(20);
            var action = new XElement("discount");
            action.Add(new XAttribute("barcode", discount.DiscountCode));

            headers.GetRootElements().Add(action);

            var result = HttpCommunication.HttpCommun(headers.GetRootElements(), 10000);

            ParseDiscountLimits(result, discount);

            return discount;
        }

        private static Discount ParseDiscountLimits(XElement item, Discount discount)
        {
            try
            {
                discount.AvailableLimit = double.Parse(item.Element("people").Attribute("debt_limit_summ").Value ?? "0");
                discount.DebtLimit = double.Parse(item.Element("people").Attribute("saldo_credit").Value ?? "0");
                discount.Saldo = double.Parse(item.Element("people").Attribute("availabel_limit").Value ?? "0");
            }
            catch (Exception)
            {
                MessageBox.Show("В данный момент информация о пользователе " +
                                " недоступна\r\nПроверьте сетевое подключение");
            }
            return discount;
        }


        public static bool DeleteAll()
        {
            try
            {
                int count = SQLiteWrapper.ExecuteNonQuery("delete from discount");
            }
            catch (Exception e)
            {
                Logger.Log(e, "Can't clear data [discount]", Level.Error);
                return false;
            }
            return true;
        }
    }
}
