﻿using System;
using System.Windows.Forms;
using vvclient.utils;

namespace vvclient.discount
{
    public partial class CalculateDiscount : Form
    {
        private string _tempBarCode;
        Label _messageLabel;

        public CalculateDiscount(Label label)
        {
            _messageLabel = label;
            InitializeComponent();
        }

        public Discount discount { get; set; }

        private void CalculateDiscount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue >= 48 && e.KeyValue <= 58)
            {
                // Только цифры от 0 до 9 и Enter
                _tempBarCode += e.KeyCode.ToString();
            }
            else if (e.KeyCode == Keys.Enter && _tempBarCode.Length > 3)
            {
                // Штрих код полностью прочитан
                _tempBarCode = _tempBarCode.Replace("D", "");
                RemoteDiscount d = new RemoteDiscount(_tempBarCode);
                if (d.GetError() == 0)
                {
                    discount = d.GetDiscount();

                    if (discount.Capital <= 0)
                    {
                        label1.Text = "Сумма карты\r\nравно нулю";
                    }
                    DialogResult = DialogResult.OK;
                }
                else // Fallback if get coupon from server failed
                {
                    _messageLabel.Text = "Сеть не подключена";
                    GetDiscountHolder(_tempBarCode);
                }
                _tempBarCode = "";
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void lblSelectDiscountHolder_Click(object sender, EventArgs e)
        {
            using (var dlf = new DiscountListForm())
            {
                var dl = dlf.ShowDialog();
                if (dl == DialogResult.OK)
                {

                    RemoteDiscount d = new RemoteDiscount(dlf.discount.DiscountCode);
                    if (d.GetError() == 0)
                    {
                        discount = d.GetDiscount();

                        if (discount.Capital <= 0)
                        {
                            label1.Text = "Сумма карты\r\nравно нулю";
                        }
                    }
                    else // Fallback if get coupon from server failed
                    {
                        discount = dlf.discount;
                        _messageLabel.Text = "Сеть не доступна";
                    }

                    DialogResult = DialogResult.OK;
                    Hide();
                }
            }
        }

        private void GetDiscountHolder(string discountCode)
        {
            try
            {
                discount = Discount.Get(discountCode);
                if (discount.Capital <= 0)
                {
                    label1.Text = "Сумма карты\r\nравно нулю";
                }
                DialogResult = DialogResult.OK;
            }
            catch (Exception exception)
            {
                label1.Text = "Карта неопознана\r\nпопробуйте заново";
                Logger.Log(exception, "CalculateDiscount");
            }
        }
    }
}