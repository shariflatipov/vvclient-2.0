﻿using System.Collections.Generic;
using vvclient.product;

namespace vvclient.discount
{
    public class DiscountOfSumCalculator : IDiscountCalculator
    {
        public double calculate(double totalPaid, List<Product> products)
        {
            double sum = 0;
            foreach(Product product in products)
            {
                double discount = product.GetPrice() * product.GetDiscount() * product.GetQuantity() / 100;
                sum += product.GetPrice() * product.GetQuantity() - discount;
            }

            return sum - totalPaid;
        }
    }
}
