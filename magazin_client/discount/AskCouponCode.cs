﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace vvclient.discount
{
    public partial class AskCouponCode : Form
    {

        private string _coupon;
        public bool result = false;

        public AskCouponCode(string coupon)
        {
            _coupon = coupon;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            result = _coupon == textBox1.Text;
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
