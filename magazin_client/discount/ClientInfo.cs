﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace vvclient.discount
{
    public partial class ClientInfo : Form
    {
        private readonly Discount _discount;

        public ClientInfo(Discount discount)
        {
            InitializeComponent();

            if (discount == null)
            {
                MessageBox.Show("Выберите пользователя");
            }
            _discount = discount;

            FillGrid();
        }

        private void FillGrid()
        {
            SetPicture(_discount.DiscountCode);
            lblCapital.Text = _discount.Capital.ToString();
            lblDebt.Text = _discount.DebtLimit.ToString();
            lblDiscount.Text = _discount.Percents.ToString();
            lblFullName.Text = _discount.Surname + " " + _discount.Name + " " + _discount.Patronymic;
            lblLimit.Text = _discount.AvailableLimit.ToString();
            lblPhone.Text = _discount.Phone;
            lblSaldo.Text = _discount.Saldo.ToString();
        }


        private void SetPicture(string path)
        {
            path = "media/people/" + path + ".jpg";
            userPicture.Image = vvclient.utils.Utils.GetImageFromPath(path);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}