﻿using System.Data.SQLite;
using vvclient.utils;

namespace vvclient
{
    internal class SellerSettings
    {
        private const string strValidateUser = "SELECT * FROM seller where login = @login and pass = @pass";
        public string userFName;
        public string userId;
        public string userLName;
        public string userLogin;
        public string userPass;
        public string userPatron;

        public short getUser(string login, string password)
        {
            var flagRecordExists = false;

            using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var command = new SQLiteCommand(connection))
                {
                    command.CommandText = strValidateUser;
                    command.Parameters.Add(new SQLiteParameter("@login", login));
                    command.Parameters.Add(new SQLiteParameter("@pass", password));
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            userId = reader[0].ToString();
                            userLName = reader[1].ToString();
                            userFName = reader[2].ToString();
                            userPatron = reader[3].ToString();
                            userLogin = reader[5].ToString();
                            userPass = reader[6].ToString();
                            flagRecordExists = true;
                        }
                    }
                }
            }

            if (flagRecordExists)
            {
                return 1;
            }
            return -1;
        }
    }
}