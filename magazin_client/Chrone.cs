﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Xml.Linq;
using vvclient.counter;
using vvclient.discount;
using vvclient.product;
using vvclient.utils;

namespace vvclient
{
    internal class Chrone
    {
        public void updates()
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(MainForm.Id))
                {
                    var pu = new XMLHeaders();
                    pu.SetAct(1);
                    pu.OperationType(UpdateTask.Get);

                    var magazin = HttpCommunication.HttpCommun(pu.GetRootElements(), 30000);

                    Dictionary<string, string> result = Product.HandleRecievedUpdate(magazin);

                    if (result.Count > 0)
                    {

                        pu = new XMLHeaders();
                        pu.SetAct(1);
                        pu.OperationType(UpdateTask.Ok);
                        XElement update_new = new XElement("update_new");
                        foreach (KeyValuePair<string, string> kp in result)
                        {
                            XElement product = new XElement("product");
                            product.Add(new XAttribute("id", kp.Key));
                            product.Add(new XElement("batch", kp.Value));
                            update_new.Add(product);
                        }

                        pu.GetRootElements().Add(update_new);
                        
                        HttpCommunication.HttpCommun(pu.GetRootElements(), 30000);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "Chrone.cs : function => Chrone()");
            }
        }

        public void Discounts()
        {
            try
            {
                var discounts = Discount.GetUnfinishedStates();

                foreach (var discount in discounts)
                {
                    var result = HttpCommunication.HttpCommun(Discount.Serialize(discount), 30000);
                    Discount.FinishState(discount.Id, result);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "Chrone.cs : function => Chrone()");
            }
        }

        public void SendTurn()
        {
            try
            {
                CreateXml cXml = null;
                string trnId = null;
                using (var conn = new SQLiteConnection())
                {
                    conn.ConnectionString = SQLiteWrapper.ConnectionString;
                    using (var cmd = new SQLiteCommand())
                    {
                        conn.Open();
                        cmd.Connection = conn;
                        cmd.CommandText =
                            "SELECT sp.id_sold, sp.sum, sp.start_date, sl.login, sl.pass, sp.discount, sp.discount_num, " +
                            "sp.shift, sp.payment_type FROM sold_product sp, seller sl " +
                            "WHERE sp.status <> 2 AND sp.seller_id = sl.id_seller limit 1";

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                cXml = new CreateXml(reader[0].ToString(), reader[2].ToString(),
                                    reader[3].ToString(), reader[4].ToString(),
                                    double.Parse(reader[5].ToString()), reader[6].ToString(),
                                    reader[7].ToString(), reader[8].ToString(),
                                    double.Parse(reader[1].ToString()));

                                trnId = reader[0].ToString();
                            }
                        }
                    }
                }

                if (cXml != null)
                {
                    var magazin = cXml.FillProducts();
                    HttpCommunication.HandleResponseFromServer(magazin, trnId);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "Chrone.cs : function => SendTurn()");
            }
        }

        public void GetCurrencyRates()
        {
            if (MainForm.Login != null && MainForm.Pass != null && MainForm.Stock != null)
            {
                var headers = new XMLHeaders();
                headers.SetAct(13);
                var currencies = headers.GetRootElements();

                var result = HttpCommunication.HttpCommun(currencies, 20000);

                if (result != null)
                {
                    if (result.Elements().Any())
                    {
                        var element = result.Element("status");
                        if (element != null && element.Value.Equals("2"))
                        {
                            Currency.SaveCurrencies(result);
                        }
                    }
                }
            }
        }

        public void GetDiscounts()
        {
            if (MainForm.Login != null && MainForm.Pass != null && MainForm.Stock != null)
            {
                var headers = new XMLHeaders();
                headers.SetAct(6);
                var action = new XElement("discount", "get");
                headers.GetRootElements().Add(action);
                var result = HttpCommunication.HttpCommun(headers.GetRootElements(), 30000);
                if (result.Elements().Any())
                {
                    var element = result.Element("status");
                    if (element != null && element.Value.Equals("2"))
                    {
                        var success = Discount.HandleRecievedDiscountFromServer(result);
                        if (success)
                        {
                            var discountSuccess = new XMLHeaders();
                            action = new XElement("discount", "ok");
                            discountSuccess.SetAct(6);
                            discountSuccess.GetRootElements().Add(action);
                            HttpCommunication.HttpCommun(discountSuccess.GetRootElements(), 30000);
                        }
                    }
                }
            }
        }

        public void SendPeopleCounter()
        {
            Counter counter = new Counter();
            XMLHeaders header = new XMLHeaders();
            header.SetAct(23);
            XElement count = new XElement("count", counter.GetCount());
            XElement date = new XElement("date", counter.GetDate());
            header.GetRootElements().Add(count);
            header.GetRootElements().Add(date);
            HttpCommunication.HttpCommun(header.GetRootElements(), 10000);
        }
    }
}