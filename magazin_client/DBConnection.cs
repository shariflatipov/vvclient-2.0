﻿using System;
using System.Data.SQLite;
using System.Management;
using System.Windows.Forms;
using System.Xml.Linq;
using vvclient.utils;

namespace vvclient
{
    public partial class DBConnection : Form
    {
        private const string HDD = "hdd";
        private readonly string baseBoard = "bBoard";
        private readonly XElement check = new XElement("check");
        private readonly string ipAddress = "ipAddress";
        //private string Date = "date";
        private readonly string isActivate = "isActivate";
        private readonly string macAddress = "macAddress";
        private readonly string netPlata = "netPlata";
        public bool active = true;

        public DBConnection()
        {
            InitializeComponent();

            //GetMACAddress();
            //GetHDDSerial();
            //GetMotherboardSerial();

            var serial = GetMotherboardSerial();

            if (serial == GetDBValues(baseBoard))
            {
                if (GetDBValues(isActivate) != "true")
                {
                    //if (HttpCommunication.HttpCommun(check) == "ok") {
                    //    UpdateDBValues(isActivate, "true");
                    //} else {
                    //    active = false;
                    //    //this.Show();
                    //}
                }
            }
        }

        private void DBConnection_Load(object sender, EventArgs e)
        {
        }

        public void GetMACAddress()
        {
            var query =
                new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = 'TRUE'");
            var queryCollection = query.Get();

            foreach (ManagementObject mo in queryCollection)
            {
                check.Add(new XElement("NetworkCard", mo["Description"]));
                check.Add(new XElement("MACAddress", mo["MACAddress"]));

                insertToDB(netPlata, mo["Description"].ToString());
                insertToDB(macAddress, mo["MACAddress"].ToString());


                var ips = new XElement("ipAdresses");
                var addresses = (string[]) mo["IPAddress"];

                foreach (var ipaddress in addresses)
                {
                    ips.Add(new XElement("IPAddress", ipaddress));
                    insertToDB(ipAddress, ipaddress);
                }
                check.Add(ips);
            }
        }

        public string GetHDDSerial()
        {
            var searcher =
                new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");

            var result = "";
            foreach (var search in searcher.Get())
            {
                if (search["SerialNumber"].ToString().Length > 1)
                {
                    result = search["SerialNumber"].ToString();
                    var hdd = new XElement("hdd", result);
                    check.Add(hdd);
                    insertToDB(HDD, result);
                }
            }

            return result;
        }

        public string GetMotherboardSerial()
        {
            var searcher =
                new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");

            var result = "";

            foreach (var search in searcher.Get())
            {
                if (search["SerialNumber"].ToString().Length > 1)
                {
                    result = search["SerialNumber"].ToString();
                    var bBoard = new XElement("bBoard", result);
                    check.Add(bBoard);
                    insertToDB(baseBoard, result);
                }
            }

            return result;
        }

        public void insertToDB(string name, string status)
        {
            //SqlCeConnection conn = new SqlCeConnection(SQLiteWrapper.ConnectionString);
            //SqlCeCommand cmd = new SqlCeCommand();

            //if (conn.State != ConnectionState.Open) {
            //    conn.Open();
            //}
            //try {
            //    cmd.Connection = conn;
            //    cmd.Parameters.Add("@name", name);
            //    cmd.Parameters.Add("@status", status);

            //    cmd.CommandText = "select status from unit where name_unit = @name";

            //    string result = (string)cmd.ExecuteScalar();

            //    if (result == null) {
            //        cmd.CommandText = "insert into unit  (name_unit, status) values (@name, @status)";
            //        cmd.ExecuteNonQuery();
            //    }
            //} catch (Exception ex) {
            //    Logger.Log(ex, "DBConnection.cs : function => InsertToDB() => exception");
            //} finally {
            //    cmd.Parameters.Clear();
            //    conn.Close();
            //}
        }

        public static string GetDBValues(string param)
        {
            var result = "";

            using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var command = new SQLiteCommand(connection))
                {
                    command.CommandText = "select status from unit where name_unit = @param";
                    command.Parameters.Add(new SQLiteParameter("@param", param));
                    connection.Open();
                    result = (string) command.ExecuteScalar();
                }
            }


            //SqlCeConnection subconn = new SqlCeConnection(SQLiteWrapper.ConnectionString);
            //SqlCeCommand subcmd = new SqlCeCommand();

            //if (subconn.State != ConnectionState.Open) {
            //    subconn.Open();
            //}
            //subcmd.Connection = subconn;
            //subcmd.CommandText = "select status from unit where name_unit = @name";
            //subcmd.Parameters.Add("@name", param);
            //try {
            //    result = (string)subcmd.ExecuteScalar();
            //} catch (Exception ex) {
            //    Logger.Log(ex, "DBConnection.cs : function => GetDBValues()");
            //} finally {
            //    subcmd.Parameters.Clear();
            //    subconn.Close();
            //}
            return result;
        }

        public void UpdateDBValues(string param, string value)
        {
            //SqlCeConnection subconn = new SqlCeConnection(SQLiteWrapper.ConnectionString);
            //SqlCeCommand subcmd = new SqlCeCommand();

            //if (subconn.State != ConnectionState.Open) {
            //    subconn.Open();
            //}
            //subcmd.Connection = subconn;
            //subcmd.CommandText = "update unit set status = @status where name_unit = @name";
            //subcmd.Parameters.Add("@name", param);
            //subcmd.Parameters.Add("@status", value);
            //try {
            //    subcmd.ExecuteScalar();
            //} catch (Exception ex) {
            //    Logger.Log(ex, "DBConnection.cs : function => UpdateDBValues()");
            //} finally {
            //    subcmd.Parameters.Clear();
            //    subconn.Close();
            //}
        }
    }
}