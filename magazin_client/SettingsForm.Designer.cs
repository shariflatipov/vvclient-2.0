﻿namespace vvclient {
    partial class SettingsForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnPrintDailyReport = new System.Windows.Forms.Button();
            this.bntAddDiscount = new System.Windows.Forms.Button();
            this.btnRegisterReturn = new System.Windows.Forms.Button();
            this.btnOtherOperations = new System.Windows.Forms.Button();
            this.btnClearData = new System.Windows.Forms.Button();
            this.changeSettings = new System.Windows.Forms.Button();
            this.btnKeyboard = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSendPeopleCount = new System.Windows.Forms.Button();
            this.lblTurnCounter = new System.Windows.Forms.Label();
            this.btnSetLeads = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPrintDailyReport
            // 
            this.btnPrintDailyReport.Location = new System.Drawing.Point(12, 12);
            this.btnPrintDailyReport.Name = "btnPrintDailyReport";
            this.btnPrintDailyReport.Size = new System.Drawing.Size(151, 71);
            this.btnPrintDailyReport.TabIndex = 0;
            this.btnPrintDailyReport.Text = "Дневной отчет";
            this.btnPrintDailyReport.UseVisualStyleBackColor = true;
            this.btnPrintDailyReport.Click += new System.EventHandler(this.btnPrintDailyReport_Click);
            // 
            // bntAddDiscount
            // 
            this.bntAddDiscount.Location = new System.Drawing.Point(169, 12);
            this.bntAddDiscount.Name = "bntAddDiscount";
            this.bntAddDiscount.Size = new System.Drawing.Size(151, 71);
            this.bntAddDiscount.TabIndex = 1;
            this.bntAddDiscount.Text = "Добавить дисконт";
            this.bntAddDiscount.UseVisualStyleBackColor = true;
            this.bntAddDiscount.Click += new System.EventHandler(this.bntAddDiscount_Click);
            // 
            // btnRegisterReturn
            // 
            this.btnRegisterReturn.Location = new System.Drawing.Point(326, 12);
            this.btnRegisterReturn.Name = "btnRegisterReturn";
            this.btnRegisterReturn.Size = new System.Drawing.Size(151, 71);
            this.btnRegisterReturn.TabIndex = 2;
            this.btnRegisterReturn.Text = "Оформить возврат";
            this.btnRegisterReturn.UseVisualStyleBackColor = true;
            this.btnRegisterReturn.Click += new System.EventHandler(this.btnRegisterReturn_Click);
            // 
            // btnOtherOperations
            // 
            this.btnOtherOperations.Location = new System.Drawing.Point(483, 12);
            this.btnOtherOperations.Name = "btnOtherOperations";
            this.btnOtherOperations.Size = new System.Drawing.Size(151, 71);
            this.btnOtherOperations.TabIndex = 3;
            this.btnOtherOperations.Text = "Другие операции";
            this.btnOtherOperations.UseVisualStyleBackColor = true;
            this.btnOtherOperations.Click += new System.EventHandler(this.btnOtherOperations_Click);
            // 
            // btnClearData
            // 
            this.btnClearData.Location = new System.Drawing.Point(12, 105);
            this.btnClearData.Name = "btnClearData";
            this.btnClearData.Size = new System.Drawing.Size(151, 71);
            this.btnClearData.TabIndex = 4;
            this.btnClearData.Text = "Операции с БД";
            this.btnClearData.UseVisualStyleBackColor = true;
            this.btnClearData.Click += new System.EventHandler(this.btnClearData_Click);
            // 
            // changeSettings
            // 
            this.changeSettings.Location = new System.Drawing.Point(169, 105);
            this.changeSettings.Name = "changeSettings";
            this.changeSettings.Size = new System.Drawing.Size(151, 71);
            this.changeSettings.TabIndex = 5;
            this.changeSettings.Text = "Настройки";
            this.changeSettings.UseVisualStyleBackColor = true;
            this.changeSettings.Click += new System.EventHandler(this.changeSettings_Click);
            // 
            // btnKeyboard
            // 
            this.btnKeyboard.Location = new System.Drawing.Point(600, 400);
            this.btnKeyboard.Name = "btnKeyboard";
            this.btnKeyboard.Size = new System.Drawing.Size(151, 71);
            this.btnKeyboard.TabIndex = 6;
            this.btnKeyboard.Text = "Клавиатура";
            this.btnKeyboard.UseVisualStyleBackColor = true;
            this.btnKeyboard.Click += new System.EventHandler(this.btnKeyboard_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 458);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(206, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Текущее кол-во транзакций в очереди:";
            // 
            // btnSendPeopleCount
            // 
            this.btnSendPeopleCount.Location = new System.Drawing.Point(326, 105);
            this.btnSendPeopleCount.Name = "btnSendPeopleCount";
            this.btnSendPeopleCount.Size = new System.Drawing.Size(151, 71);
            this.btnSendPeopleCount.TabIndex = 9;
            this.btnSendPeopleCount.Text = "Отправить кол-во посетителей";
            this.btnSendPeopleCount.UseVisualStyleBackColor = true;
            this.btnSendPeopleCount.Click += new System.EventHandler(this.btnSendPeopleCount_Click);
            // 
            // lblTurnCounter
            // 
            this.lblTurnCounter.AutoSize = true;
            this.lblTurnCounter.Location = new System.Drawing.Point(252, 458);
            this.lblTurnCounter.Name = "lblTurnCounter";
            this.lblTurnCounter.Size = new System.Drawing.Size(0, 13);
            this.lblTurnCounter.TabIndex = 10;
            // 
            // btnSetLeads
            // 
            this.btnSetLeads.Location = new System.Drawing.Point(483, 105);
            this.btnSetLeads.Name = "btnSetLeads";
            this.btnSetLeads.Size = new System.Drawing.Size(151, 71);
            this.btnSetLeads.TabIndex = 12;
            this.btnSetLeads.Text = "Установить количество поситителей";
            this.btnSetLeads.UseVisualStyleBackColor = true;
            this.btnSetLeads.Click += new System.EventHandler(this.btnSetLeads_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 483);
            this.Controls.Add(this.btnSetLeads);
            this.Controls.Add(this.lblTurnCounter);
            this.Controls.Add(this.btnSendPeopleCount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnKeyboard);
            this.Controls.Add(this.changeSettings);
            this.Controls.Add(this.btnClearData);
            this.Controls.Add(this.btnOtherOperations);
            this.Controls.Add(this.btnRegisterReturn);
            this.Controls.Add(this.bntAddDiscount);
            this.Controls.Add(this.btnPrintDailyReport);
            this.Name = "SettingsForm";
            this.Text = "SettingsForm";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPrintDailyReport;
        private System.Windows.Forms.Button bntAddDiscount;
        private System.Windows.Forms.Button btnRegisterReturn;
        private System.Windows.Forms.Button btnOtherOperations;
        private System.Windows.Forms.Button btnClearData;
        private System.Windows.Forms.Button changeSettings;
        private System.Windows.Forms.Button btnKeyboard;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSendPeopleCount;
        private System.Windows.Forms.Label lblTurnCounter;
        private System.Windows.Forms.Button btnSetLeads;
    }
}