﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using vvclient.discount;
using vvclient.goodsreturn;
using vvclient.settings;
using vvclient.utils;
using vvclient.promotions;
using System.Collections.Generic;
using vvclient.actions;
using vvclient.actions.dao;

namespace vvclient
{
    public partial class MainForm : Form
    {
        public static string Id;
        public static string Login;
        public static string Pass;
        public static string Stock;
        public static string Ip;
        public static Guid GUID;
        public readonly AuthForm Auth = new AuthForm();
        public readonly MainWorkForm MWork = new MainWorkForm();
        public static List<NextWithDiscountProduct> Actions = new List<NextWithDiscountProduct>();
        private Thread _thrChrone;
        private Thread _actionsThread;

        public MainForm()
        {
            InitializeComponent();
        }

        private void Main_Form_Load(object sender, EventArgs e)
        {
            if (!Directory.Exists("logs"))
            {
                Directory.CreateDirectory("logs");
            }

            Ip = DBConnection.GetDBValues("ip");
            Stock = DBConnection.GetDBValues("point");

            var dbc = new DBConnection();

            if (dbc.active == false)
            {
                dbc.MdiParent = this;
                dbc.Dock = DockStyle.Fill;
                dbc.Show();
            }
            else
            {
                _thrChrone = new Thread(ChroneThread) {Name = "thrChrone", IsBackground = true};
                _thrChrone.CurrentCulture = ApplicationCulture.GetCurrentCulture();
                _thrChrone.CurrentUICulture = ApplicationCulture.GetCurrentCulture();
                _thrChrone.Start();

                _actionsThread = new Thread(ActionsThread) { Name = "actionThread", IsBackground = true };
                _actionsThread.CurrentCulture = ApplicationCulture.GetCurrentCulture();
                _actionsThread.CurrentUICulture = ApplicationCulture.GetCurrentCulture();
                _actionsThread.Start();

                dbc.Dispose();
                Auth.MdiParent = this;
                Auth.Show();
            }
        }

        private void ActionsThread()
        {
            while (true)
            {
                try
                {
                    BatchPromotion.GetPromotions();
                    CategoriesUserDiscount.GetUserDiscounts();
                    Actions = ActionSync.GetNextWithDiscountAction();
                }
                catch (Exception ex)
                {
                    Logger.Log(ex, "MainForm.cs : function => ActionsThread()", Level.Warining);
                }

                Thread.Sleep(300000);
            }
        }

        private void ChroneThread()
        {
            while (true)
            {
                try
                {
                    var cr = new Chrone();
                    cr.Discounts();
                    cr.SendTurn();
                    cr.updates();
                    ReturnGood.SendReturns();
                    cr.GetCurrencyRates();
                    cr.GetDiscounts();
                    cr.SendPeopleCounter();
                }
                catch (Exception ex)
                {
                    Logger.Log(ex, "MainWorkForm.cs : function => ChroneThread()", Level.Warining);
                }

                Thread.Sleep(5000);
            }
        }
    }
}