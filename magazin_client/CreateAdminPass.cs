﻿using System;
using System.Windows.Forms;
using vvclient.utils;

namespace vvclient
{
    public partial class CreateAdminPass : Form
    {
        private int tryCnt;

        public CreateAdminPass()
        {
            InitializeComponent();
        }

        private void ButtonCreateAdminPassClick(object sender, EventArgs e)
        {
            var error = 0;

            if (Pass.TextLength > 6)
            {
                if (Pass.Text == PassConfirm.Text)
                {
                    var cdb = new SQLiteWrapper();
                    var mForm = new MainForm();
                    mForm.Show();
                    Hide();
                }
                else
                {
                    MessageBox.Show("Поля не совпадают");
                    error = 1;
                }
            }
            else
            {
                MessageBox.Show("Неправильный формат поля \"Пароль\"");
                error = 1;
            }


            if (error == 1)
            {
                tryCnt++;

                Pass.Text = "";
                PassConfirm.Text = "";

                if (tryCnt > 2)
                {
                    MessageBox.Show("Нажмите кнопку \"Инфо\"");
                }
            }
        }

        private void ButtonHelpClick(object sender, EventArgs e)
        {
            var hl = new Help();
            hl.ShowDialog();
        }
    }
}