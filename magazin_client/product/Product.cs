﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using System.Xml.Linq;
using vvclient.settings;
using vvclient.utils;
using vvclient.actions;
using vvclient.discount;

namespace vvclient.product
{
    public class Product
    {
        public static List<Product> GetOrderedList(DataGridView view)
        {
            var productList = new List<Product>();

            foreach (DataGridViewRow row in view.Rows)
            {
                var product = SetProductById(row.Cells["Id"].Value.ToString(),
                    row.Cells["Batch"].Value.ToString(), false,
                    double.Parse(row.Cells["Discount"].Value.ToString()),
                    double.Parse(row.Cells["Quantity"].Value.ToString()),
                    double.Parse(row.Cells["Price"].Value.ToString()));

                productList.Add(product);
            }

            return productList;
        }

        public static Product SetProductById(DataGridViewRow row, int index)
        {
            var result = new Product() { 
                _rowIndex = index,
                _batch = row.Cells["Batch"].Value.ToString(),
                _discount = double.Parse(row.Cells["Discount"].Value.ToString()),
                _quantity = double.Parse(row.Cells["Quantity"].Value.ToString()),
                _id = row.Cells["Id"].Value.ToString(),
                _name = row.Cells["GoodsName"].Value.ToString(),
                _price = double.Parse(row.Cells["Price"].Value.ToString()),
                _unit = row.Cells["Unit"].Value.ToString()
            };

            return result;
        }

        public static Product GetProduct(string barcode, string batch)
        {
            Product product = null;

            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText =
                            "SELECT product_id, product_name, product_description, unitID, product_price, product_image, pack_count, batch, discount_percent " +
                            "FROM product where product_id=@prod_id and @batch=batch";
                        command.Parameters.Add(new SQLiteParameter("@prod_id", barcode));
                        command.Parameters.Add(new SQLiteParameter("@batch", batch));
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                product = new Product();
                                product.SetId(reader["product_id"].ToString());

                                product.SetName(reader["product_name"].ToString());
                                product.SetDescription(reader["product_description"].ToString());
                                product.SetUnit(reader["unitID"].ToString());
                                product.SetPrice(Convert.ToDouble(reader["product_price"]));
                                product.SetDiscount(Convert.ToDouble(reader["discount_percent"]));
                                product.SetQuantity(1);
                                short packCount;

                                product.SetImage(reader["product_image"].ToString());
                                short.TryParse(reader["pack_count"].ToString(), out packCount);
                                product.SetPackCount(packCount);
                                product.SetBatch(batch);
                            }
                        }

                        if (product != null)
                        {
                            command.CommandText = "select category_id from product_categories where product_id = @prod_id";
                            using (var reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    product._categories = new List<int>();
                                }
                                while (reader.Read())
                                {
                                    product._categories.Add(int.Parse(reader["category_id"].ToString()));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, string.Format("Product.cs : function => SetProductById( ID : = {0})", barcode));
            }
            return product;
        }

        public static Product SetProductById(string barcode, string batch, bool discountFromDatabase,
            double discount = 0, double quantity = 1, double price = 0)
        {
            var product = new Product();

            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(connection))
                    {
                        command.CommandText =
                            "SELECT product_id, product_name, product_description, unitID, product_price, product_image, pack_count, batch, discount_percent " +
                            "FROM product where product_id=@prod_id and @batch=batch";
                        command.Parameters.Add(new SQLiteParameter("@prod_id", barcode));
                        command.Parameters.Add(new SQLiteParameter("@batch", batch));
                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                product.SetId(reader["product_id"].ToString());

                                product.SetName(reader["product_name"].ToString());
                                product.SetDescription(reader["product_description"].ToString());
                                product.SetUnit(reader["unitID"].ToString());
                                if (price == 0)
                                {
                                    product.SetPrice(Convert.ToDouble(reader["product_price"]));
                                }
                                else
                                {
                                    product.SetPrice(price);
                                }
                                product.SetQuantity(quantity);
                                if (discountFromDatabase)
                                {
                                    product.SetDiscount(Convert.ToDouble(reader["discount_percent"]));
                                }
                                else
                                {
                                    product.SetDiscount(discount);
                                }
                                short packCount;

                                product.SetImage(reader["product_image"].ToString());
                                short.TryParse(reader["pack_count"].ToString(), out packCount);
                                product.SetPackCount(packCount);
                                product.SetBatch(batch);
                            }
                        }

                        command.CommandText = "select category_id from product_categories where product_id = @prod_id";
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                product._categories = new List<int>();
                            }
                            while (reader.Read())
                            {
                                product._categories.Add(int.Parse(reader["category_id"].ToString()));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, string.Format("Product.cs : function => SetProductById( ID : = {0})", barcode));
            }
            return product;
        }

        public void SetProductByFields(string id, string name, string description, string unit, double price,
            string image, double quantity, double discount, short packCount, string batch)
        {
            _id = id;
            _name = name;
            _description = description;
            _unit = unit;
            _price = price;
            SetImage(image);
            _discount = discount;
            _quantity = quantity;
            _packCount = packCount;
            _batch = batch;
        }

        public static Product GetProductStub(string description)
        {
            var product = new Product
            {
                _id = "",
                _batch = "0",
                _description = "",
                _discount = 0,
                _name = description,
                _price = 0,
                _quantity = 0,
                _unit = "шт",
                _packCount = 0
            };

            product.SetImage("img/no_goods.jpg");
            return product;
        }

        public static List<Product> GetProducts(string productId, bool likeClause = false)
        {
            var result = new List<Product>();

            try
            {
                using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
                {
                    using (var command = new SQLiteCommand(connection))
                    {
                        string commandText;
                        if (likeClause)
                        {
                            commandText = "SELECT product_id, product_name, product_description, " +
                                                  " unitID, product_price, product_image, pack_count, batch, discount_percent, " +
                                                  " is_list, article, size, color FROM product where product_id like @prod_id or product_name like @prod_id " +
                                                  " or article like @prod_id limit 30 ";
                        } else
                        {
                            commandText = "SELECT product_id, product_name, product_description, " +
                                                  " unitID, product_price, product_image, pack_count, batch, discount_percent, " +
                                                  " is_list, article, size, color FROM product where product_id=@prod_id or product_name=@prod_id " +
                                                  "  or article=@prod_id";
                        }

                        command.CommandText = commandText;
                        if (likeClause)
                        {
                            command.Parameters.Add(new SQLiteParameter("@prod_id", "%" + productId + "%"));
                        } else
                        {
                            command.Parameters.Add(new SQLiteParameter("@prod_id", productId));
                        }

                        connection.Open();

                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var p = new Product();

                                p.SetId(reader["product_id"].ToString());
                                p.SetName(reader["product_name"].ToString());
                                p.SetArticle(reader["article"].ToString());
                                p.SetSize(reader["size"].ToString());
                                p.SetColor(reader["color"].ToString());
                                p.SetDescription(reader["product_description"].ToString());
                                p.SetUnit(reader["unitID"].ToString());
                                p.SetPrice(Math.Round(Convert.ToDouble(reader["product_price"]), Config.RoundPreciesion));
                                p.SetQuantity(1);
                                p.SetDiscount(Math.Round(Convert.ToDouble(reader["discount_percent"]), 4));
                                p.SetImage(reader["product_image"].ToString());
                                short packCount;
                                short.TryParse(reader["pack_count"].ToString(), out packCount);
                                p.SetPackCount(packCount);
                                p.SetBatch(reader["batch"].ToString());
                                p.SetIsList(reader["is_list"].ToString());

                                using (var subcommand = new SQLiteCommand(connection))
                                {
                                    subcommand.CommandText =
                                        "select category_id from product_categories where product_id = @prod_id";
                                    subcommand.Parameters.Add(new SQLiteParameter("@prod_id", productId));
                                    using (var subreader = subcommand.ExecuteReader())
                                    {
                                        if (reader.HasRows)
                                        {
                                            p._categories = new List<int>();
                                        }
                                        while (subreader.Read())
                                        {
                                            p._categories.Add(int.Parse(subreader["category_id"].ToString()));
                                        }
                                    }
                                }

                                result.Add(p);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, string.Format("Product.cs : function => SetProductById( ID : = {0})", productId));
            }
            return result;
        }

        public static int GetDependentCount(string id)
        {
            var result = 0;

            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;

                    try
                    {
                        cmd.CommandText = "select count(*) from product where dependent=@product_id";
                        cmd.Parameters.Add(new SQLiteParameter("@product_id", id));
                        conn.Open();
                        int.TryParse(cmd.ExecuteScalar().ToString(), out result);
                    }
                    catch (SQLiteException sqle)
                    {
                        Logger.Log(sqle, "product_id= " + id);
                    }
                    finally
                    {
                        cmd.Parameters.Clear();
                    }
                }
            }

            return result;
        }

        public static Dictionary<string, string> HandleRecievedUpdate(XElement magazin)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            if (magazin == null) return result;

            try
            {
                var query = from mag in magazin.Elements("product")
                    select mag;

                foreach (var mg in query)
                {
                    var product = ProductIsValid(mg);

                    if (product != null)
                    {
                        var xElement = mg.Element("flag");
                        if (xElement != null && xElement.Value.Equals("2"))
                        {
                            if (product.Delete())
                            {
                                result.Add(product.GetId(), product.GetBatch());
                            }
                        }
                        else
                        {
                            if (product.Save())
                            {
                                result.Add(product.GetId(), product.GetBatch());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "Chrone.cs : function => handleRecievedUpdate() ----Invalid XML Answer----", Level.Fatal);
            }

            return result;
        }

        public static Product ProductIsValid(XElement product)
        {
            var currProduct = new Product();
            var webClient = new WebClient();

            // Trying to download product image otherwise set image to no_image.jpg
            try
            {
                var imageElement = product.Element("image_source");
                if (imageElement != null && !File.Exists(imageElement.Value))
                {
                    webClient.DownloadFile("http://" + MainForm.Ip + "/" + imageElement.Value, imageElement.Value);
                }
                currProduct.SetImagePath(imageElement.Value);
            }
            catch (Exception)
            {
                currProduct.SetImagePath("img/no_image.jpg");
            }

            // Critical fields if they are null we couldn't continue anyway
            if (product.Element("name") == null || product.Attribute("id") == null
                || product.Element("price_sell") == null || product.Element("batch") == null)
            {
                return null;
            }

            currProduct.SetId(product.Attribute("id").Value);
            currProduct.SetName(product.Element("name").Value);
            currProduct.SetArticle(product.Element("article").Value);
            currProduct.SetSize(product.Element("size").Value);
            currProduct.SetColor(product.Element("color").Value);
            currProduct.SetPrice(double.Parse(product.Element("price_sell").Value));
            currProduct.SetIsList(product.Element("is_list").Value);
            currProduct.SetParent(product.Element("depend").Value);
            currProduct.SetUnit(product.Element("unit").Value);
            currProduct.SetListPrice(double.Parse(product.Element("price").Value));
            currProduct.SetPackCount(short.Parse(product.Element("pack_cnt").Value));
            currProduct.SetDiscount(double.Parse(product.Element("discount_percent").Value));
            currProduct.SetBatch(product.Element("batch").Value);

            var categories = product.Element("category");
            if (categories != null)
            {
                var query = from mag in categories.Elements("id")
                    select mag;

                foreach (var mg in query)
                {
                    currProduct.GetCategories().Add(int.Parse(mg.Value));
                }
            }

            return currProduct;
        }

        public bool Save()
        {
            var result = 0;

            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;

                    try
                    {
                        cmd.CommandText = "INSERT INTO product " +
                                          "(dependent, is_list, product_id, product_name, product_image, " +
                                          "unitID, product_list_price, product_price, pack_count, batch, discount_percent, article, size, color) " +
                                          "VALUES " +
                                          "(@dependent, @isList, @product_id, @product_name, @product_image, @unitID, " +
                                          "@product_list_price, @product_price, @pack_count, @batch, @discount, @article, @size, @color);";

                        cmd.Parameters.Add(new SQLiteParameter("@isList", GetIsList()));
                        cmd.Parameters.Add(new SQLiteParameter("@dependent", GetParent()));
                        cmd.Parameters.Add(new SQLiteParameter("@product_id", GetId()));
                        cmd.Parameters.Add(new SQLiteParameter("@product_name", GetName()));
                        cmd.Parameters.Add(new SQLiteParameter("@product_image", GetImagePath()));
                        cmd.Parameters.Add(new SQLiteParameter("@unitID", GetUnit()));
                        cmd.Parameters.Add(new SQLiteParameter("@product_list_price", GetPrice()));
                        cmd.Parameters.Add(new SQLiteParameter("@product_price", GetPrice()));
                        cmd.Parameters.Add(new SQLiteParameter("@pack_count", GetPackCount()));
                        cmd.Parameters.Add(new SQLiteParameter("@discount", GetDiscount()));
                        cmd.Parameters.Add(new SQLiteParameter("@batch", GetBatch()));
                        cmd.Parameters.Add(new SQLiteParameter("@article", GetArticle()));
                        cmd.Parameters.Add(new SQLiteParameter("@size", GetSize()));
                        cmd.Parameters.Add(new SQLiteParameter("@color", GetColor()));
                        conn.Open();
                        result = cmd.ExecuteNonQuery();

                        foreach (var mg in GetCategories())
                        {
                            cmd.CommandText = "insert into product_categories values (@product_id, " + mg + ")";
                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (SQLiteException sqle)
                    {
                        Logger.Log(sqle);
                    }
                    finally
                    {
                        cmd.Parameters.Clear();
                    }
                }
            }
            return result > 0;
        }

        public bool Delete()
        {
            var result = 0;
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    conn.Open();
                    try
                    {
                        cmd.CommandText = "delete from product where product_id = @product_id and batch = @batch";

                        cmd.Parameters.Add(new SQLiteParameter("@product_id", GetId()));
                        cmd.Parameters.Add(new SQLiteParameter("@batch", GetBatch()));
                        result = cmd.ExecuteNonQuery();
                    }
                    catch (SQLiteException)
                    {
                        return false;
                    }
                    return true;
                }
            }
        }

        public static string GetStockRemain(Product product)
        {
            try
            {
                var header = new XMLHeaders();
                header.SetAct(17);
                var productElement = new XElement("product", new XAttribute("barcode", product.GetId()));
                header.GetRootElements().Add(productElement);
                var result = HttpCommunication.HttpCommun(header.GetRootElements(), 3000);
                return HandleStockRemain(result);
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string HandleStockRemain(XElement element)
        {
            var result = "";
            var stocks = from el in element.Elements("stock") select el;
            foreach (var stock in stocks)
            {
                double count = 0;
                var products = from st in stock.Elements("product") select st;
                foreach (var product in products)
                {
                    if (product.Attribute("count") != null)
                    {
                        count += double.Parse(product.Attribute("count").Value);
                    }
                }
                result += stock.Attribute("name").Value + ":  " + count + "шт.\r\n";
            }
            return result;
        }


        public static bool DeleteAll()
        {
            try
            {
                int count = SQLiteWrapper.ExecuteNonQuery("delete from product");
                return true;
            }
            catch (Exception e)
            {
                Logger.Log(e, "Can't delete data [product]", Level.Error);
            }
            return false;
        }

        #region Variables, Getters and Setters

        private int _rowIndex;
        private string _id;
        private string _name;
        private string _article;
        private string _size;
        private string _description;
        private string _color;
        private string _unit;
        private double _price;
        private double _listPrice;
        private Bitmap _image;
        private string _imagePath;
        private double _discount;
        private double _quantity;
        private short _packCount;
        private string _batch;
        private string _isList;
        private List<int> _categories = new List<int>();
        private List<IAction> _actions = new List<IAction>();
        private string _parent;


        public string GetId()
        {
            return _id;
        }

        public void SetId(string id)
        {
            _id = id;
        }

        public string GetName()
        {
            return _name;
        }

        public void SetName(string name)
        {
            _name = name;
        }

        public string GetArticle()
        {
            return _article;
        }

        public void SetArticle(string article)
        {
            _article = article;
        }

        public string GetColor()
        {
            return _color;
        }

        public void SetColor(string color)
        {
            _color = color;
        }

        public string GetSize()
        {
            return _size;
        }

        public void SetSize(string size)
        {
            _size = size;
        }

        public string GetDescription()
        {
            return _description ?? "0";
        }

        public void SetDescription(string description)
        {
            _description = description;
        }

        public string GetUnit()
        {
            return _unit ?? "0";
        }

        public void SetUnit(string unit)
        {
            _unit = unit;
        }

        public double GetPrice()
        {
            return Math.Round(_price, Config.RoundPreciesion);
        }

        public void SetPrice(double price)
        {
            _price = price;
        }

        public double GetListPrice()
        {
            return _listPrice;
        }

        public void SetListPrice(double listPrice)
        {
            _listPrice = listPrice;
        }

        public double GetQuantity()
        {
            return Math.Round(_quantity, 3);
        }

        public void SetQuantity(double quantity)
        {
            _quantity = quantity;
        }

        public void AddQuantity(double quantity)
        {
            _quantity += quantity;
        }

        public Bitmap GetImage()
        {
            return _image;
        }

        private void SetImage(string path)
        {
            try
            {
                _image = new Bitmap(path);
            }
            catch
            {
                try
                {
                    _image = new Bitmap("img/no_image.jpg");
                }
                catch (Exception ex)
                {
                    Logger.Log(ex, "Product.cs => SetImage : " + path);
                }
            }
        }

        public void SetImage(Bitmap image)
        {
            _image = image;
        }

        public void SetImagePath(string path)
        {
            _imagePath = path;
        }

        public string GetImagePath()
        {
            return _imagePath;
        }

        public short GetPackCount()
        {
            return _packCount;
        }

        public void SetPackCount(short packCount)
        {
            _packCount = packCount;
        }

        public double GetDiscount(Discount discount = null)
        {
            return _discount +
                CategoriesUserDiscount.GetDiscountForProductAndUser(this, discount);
                // + CalculateActionDiscount.GetActionDiscount(this);
        }

        public void SetDiscount(double discount)
        {
            _discount = discount;
        }

        public void SetBatch(string batch)
        {
            _batch = batch;
        }

        public string GetBatch()
        {
            return _batch ?? "0";
        }

        public string GetIsList()
        {
            return _isList ?? "0";
        }

        private void SetIsList(string isList)
        {
            _isList = isList;
        }

        public double GetTotal()
        {
            return Math.Round((_price*_quantity) - (_price*_quantity*_discount/100), Config.RoundPreciesion);
        }

        public void SetCategory(List<int> categories)
        {
            _categories = categories;
        }

        public List<int> GetCategories()
        {
            return _categories;
        }

        public void SetActions(List<IAction> actions)
        {
            _actions = actions;
        }

        public  List<IAction> GetActions()
        {
            return _actions;
        }

        public string GetParent()
        {
            return _parent ?? "0";
        }

        public void SetParent(string parent)
        {
            _parent = parent;
        }
        
        public int GetRowIndex()
        {
            return _rowIndex;
        }

        public void SetRowIndex(int index)
        {
            _rowIndex = index;
        }

        #endregion
    }
}