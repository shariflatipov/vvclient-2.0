﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using vvclient.actions;
using vvclient.broker;
using vvclient.settings;
using vvclient.utils;

namespace vvclient.product
{
    public class ProductCart
    {
        private readonly DataGridView _dGrid;
        private CalculateActionDiscount _cad = new CalculateActionDiscount();

        public ProductCart(DataGridView dgvc)
        {
            _dGrid = dgvc;
        }

        /// <summary>
        /// Добавить товар в список покупок
        /// </summary>
        /// <param name="product">Объект Product который нужно включить в список</param>
        /// <param name="append">Если данный товар уже есть в списке то прибавить или не прибавлять к количеству</param>
        public void AddItem(Product product, bool append = false)
        {
            var productIndex = CheckProductExistance(product.GetId(), product.GetBatch());

            if (productIndex >= 0)
            {
                double currentQuantity = 0;
                if (append)
                {
                    currentQuantity = Convert.ToDouble(
                        _dGrid.Rows[productIndex].Cells["Quantity"].Value);
                }
                product.SetQuantity(product.GetQuantity() + currentQuantity);
                _dGrid.Rows[productIndex].Cells["Quantity"].Value = product.GetQuantity();
                _dGrid.Rows[productIndex].Cells["Discount"].Value = product.GetDiscount();
                CalculateSubTotal(productIndex);
                SetSelectedItem(productIndex);
            }
            else
            {
                AddProduct(product);

                if (_dGrid.Rows.Count > 0)
                {
                    SetSelectedItem(_dGrid.Rows.Count - 1);
                }
                else
                {
                    SetSelectedItem(0);
                }
            }
        }

        public void AddItemByPrice(Product product)
        {
            AddProduct(product);

            if (_dGrid.Rows.Count > 0)
            {
                SetSelectedItem(_dGrid.Rows.Count - 1);
            }
            else
            {
                SetSelectedItem(0);
            }
        }


        public void RemoveItem(int index)
        {
            try
            {
                _dGrid.Rows.RemoveAt(index);
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "RemoveItem");
            }
        }

        public void Clear()
        {
            _dGrid.Rows.Clear();
        }

        /// <summary>
        ///     Highlights items in DataGridView
        /// </summary>
        /// <param name="itemIndex">Row index to highlight</param>
        private void SetSelectedItem(int itemIndex)
        {
            try
            {
                _dGrid.Rows[itemIndex].Selected = true;
                _dGrid.FirstDisplayedScrollingRowIndex = itemIndex;
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "setSelectedItem", Level.Warining);
            }
        }

        /// <summary>
        ///     Check if product already exists in datagridview
        /// </summary>
        /// <param name="productId">Product Bar Code</param>
        /// <param name="batch">Batch of the product</param>
        /// <returns>Index of Product in DataGrid</returns>
        private int CheckProductExistance(string productId, string batch)
        {
            foreach (DataGridViewRow row in _dGrid.Rows)
            {
                try
                {
                    if (row.Cells["Id"].Value.Equals(productId)
                        && row.Cells["Batch"].Value.Equals(batch))
                    {
                        return row.Index;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Log(exception, "ProductCart=>CheckProductExistance", Level.Fatal);
                }
            }
            return -1;
        }

        public Dictionary<string, double> CalculateTotal()
        {
            double total = 0;
            double totalQuantity = 0;
            foreach (DataGridViewRow row in _dGrid.Rows)
            {
                var ttl = (Convert.ToDouble(row.Cells["Price"].Value))
                          *(Convert.ToDouble(row.Cells["Quantity"].Value));
                totalQuantity += (Convert.ToDouble(row.Cells["Quantity"].Value));
                total += ttl - (ttl*(Convert.ToDouble(row.Cells["Discount"].Value))/100);
            }
            return new Dictionary<string, double>() {
                { "total",  Math.Round(total, Config.RoundPreciesion) },
                { "totalQuantity",  totalQuantity }
            };
        }

        private void CalculateSubTotal(int index)
        {
            var ttl = (Convert.ToDouble(_dGrid.Rows[index].Cells["Price"].Value))*
                      (Convert.ToDouble(_dGrid.Rows[index].Cells["Quantity"].Value));
            ttl -= (ttl*(Convert.ToDouble(_dGrid.Rows[index].Cells["Discount"].Value))/100);
            _dGrid.Rows[index].Cells["Total"].Value = Math.Round(ttl, Config.RoundPreciesion);
        }

        private void AddProduct(Product product)
        {
            _dGrid.Rows.Add(product.GetId(), product.GetName(), product.GetArticle(), product.GetColor(), product.GetDescription(), product.GetQuantity(),
                product.GetUnit(), product.GetBatch(), product.GetPrice(), product.GetDiscount(), product.GetTotal());
            MainWorkForm._product = product;

            var thread = new Thread(MessageSender.Send);
            thread.Start("1~" + product.GetName() + "~" + product.GetQuantity() + "~" + product.GetPrice());
        }
    }
}
