﻿namespace vvclient.counter
{
    partial class CounterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericCouter = new System.Windows.Forms.NumericUpDown();
            this.btnSetLeads = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericCouter)).BeginInit();
            this.SuspendLayout();
            // 
            // numericCouter
            // 
            this.numericCouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericCouter.Location = new System.Drawing.Point(12, 12);
            this.numericCouter.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericCouter.Name = "numericCouter";
            this.numericCouter.Size = new System.Drawing.Size(247, 80);
            this.numericCouter.TabIndex = 0;
            // 
            // btnSetLeads
            // 
            this.btnSetLeads.Location = new System.Drawing.Point(12, 110);
            this.btnSetLeads.Name = "btnSetLeads";
            this.btnSetLeads.Size = new System.Drawing.Size(247, 66);
            this.btnSetLeads.TabIndex = 1;
            this.btnSetLeads.Text = "Установить";
            this.btnSetLeads.UseVisualStyleBackColor = true;
            this.btnSetLeads.Click += new System.EventHandler(this.btnSetLeads_Click);
            // 
            // CounterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 213);
            this.Controls.Add(this.btnSetLeads);
            this.Controls.Add(this.numericCouter);
            this.Name = "CounterForm";
            this.Text = "Количество поситителей";
            this.Load += new System.EventHandler(this.CounterForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericCouter)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericCouter;
        private System.Windows.Forms.Button btnSetLeads;
    }
}