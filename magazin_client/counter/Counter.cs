﻿using System;
using System.Data.SQLite;

using vvclient.utils;

namespace vvclient.counter
{
    public class Counter
    {
        private long _id = 0;
        public int Count { get; set; }
        private string _date = "";

        public Counter()
        {
            GetCounterForCurrentDate();
        }

        private void GetCounterForCurrentDate()
        {
            bool read = false;
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                string commandString = "select id, count, date from people_counter where date = '" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                using (var command = new SQLiteCommand(commandString, conn))
                {
                    conn.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            _id = long.Parse(reader["id"].ToString());
                            Count = int.Parse(reader["count"].ToString());
                            _date = reader["date"].ToString();
                            read = true;
                        }
                    }
                }
            }
            if (!read)
            {
                string cs = "insert into people_counter(date, count) values('" + DateTime.Now.ToString("yyyy-MM-dd") + "', 0)";
                _id = SQLiteWrapper.ExecuteAndReturnLastId(cs);
                Count = 0;
            }
        }


        public int GetCount()
        {
            return Count;
        }


        public string GetDate()
        {
            return _date;
        }

        private bool UpdateCounter()
        {
            string commandString = "update people_counter set count = " + Count + " where id = " + _id;
            if (SQLiteWrapper.ExecuteNonQuery(commandString) >=1)
            {
                return true;
            }
            return false;
        }

        public void IncrementCounter()
        {
            Count += 1;
        }

        public void DecrementCounter()
        {
            if (Count > 0)
            {
                Count -= 1;
            }
        }

        public bool Save()
        {
            return UpdateCounter();
        }
    }
}
