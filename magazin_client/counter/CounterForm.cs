﻿using System;
using System.Windows.Forms;

namespace vvclient.counter
{
    public partial class CounterForm : Form
    {
        private Counter _counter;
        public CounterForm()
        {
            InitializeComponent();
        }

        private void btnSetLeads_Click(object sender, EventArgs e)
        {
            _counter.Count = (int)numericCouter.Value;
            if (_counter.Save())
            {
                DialogResult = DialogResult.OK;
            } else
            {
                DialogResult = DialogResult.None;
            }
        }

        private void CounterForm_Load(object sender, EventArgs e)
        {
            _counter = new Counter();
            numericCouter.Value = _counter.Count;
        }
    }
}
