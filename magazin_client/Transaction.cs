﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using vvclient.payments;
using vvclient.product;
using vvclient.utils;

namespace vvclient
{
    internal class Transaction
    {
        private readonly List<Product> _products;

        public Transaction(List<Product> products)
        {
            _products = products;
        }

        /// <summary>
        ///     Inserts new sold into sold_product table
        /// </summary>
        public string SaveSoldProduct(double sum, double discount, string discountCode,
            PaymentType type = PaymentType.cash)
        {
            var lastId = Utils.GetId();
            using (var connection = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                var commandText =
                    "INSERT INTO sold_product (id_sold, description, status, end_date, start_date, sum, seller_id, discount, discount_num, shift, payment_type)" +
                    "VALUES ('" + lastId + "', 'Sold started', '0', '" + DateTime.Now.ToString(Utils.DateFormat) + "', " +
                    "'" + DateTime.Now.ToString(Utils.DateFormat) + "', '" + sum + "', '" + MainForm.Id + "', " +
                    discount + ", '" + discountCode + "', '" + MainForm.GUID + "', '" + type + "')";

                var commandDetails =
                    "insert into transactions(sold_id, product_id, count, price, discount, batch) values ";


                using (var command = new SQLiteCommand(commandText, connection))
                {
                    connection.Open();
                    using (var transaction = connection.BeginTransaction())
                    {
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();

                        foreach (var product in _products)
                        {
                            command.CommandText = commandDetails + "('" + lastId + "', '" + product.GetId() +
                                                  "', '" + product.GetQuantity() + "', '" + product.GetPrice() +
                                                  "', '" + product.GetDiscount() + "', '" + product.GetBatch() +
                                                  "');";
                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                }
            }
            return lastId;
        }

        /// <summary>
        ///     deletes from sold_product table record with trnId
        /// </summary>
        /// <param name="trnId">transaction id</param>
        /// <returns>true on success and false on failure</returns>
        public static bool FinishProduct(string trnId)
        {
            using (var conn = new SQLiteConnection())
            {
                conn.ConnectionString = SQLiteWrapper.ConnectionString;
                using (var cmd = new SQLiteCommand())
                {
                    try
                    {
                        conn.Open();
                        cmd.Connection = conn;
                        cmd.CommandText = "DELETE FROM transactions WHERE sold_id = @sold_id";
                        cmd.Parameters.Add(new SQLiteParameter("@sold_id", trnId));
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "DELETE FROM sold_product WHERE id_sold = @sold_id";
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(ex, "SQLCommunication.cs : function => FinishProduct()");
                        return false;
                    }
                    finally
                    {
                        cmd.Parameters.Clear();
                    }
                    return true;
                }
            }
        }

        public static bool FinishProduct(string trnId, string description, short status)
        {
            using (var conn = new SQLiteConnection(SQLiteWrapper.ConnectionString))
            {
                using (var cmd = new SQLiteCommand())
                {
                    conn.Open();
                    try
                    {
                        cmd.Connection = conn;
                        cmd.CommandText =
                            "UPDATE sold_product set end_date = @en_dt, status = @status, description = @desc WHERE id_sold = @sold_id";
                        cmd.Parameters.Add(new SQLiteParameter("@desc", description));
                        cmd.Parameters.Add(new SQLiteParameter("@status", status));
                        cmd.Parameters.Add(new SQLiteParameter("@en_dt", DateTime.Now.ToString(Utils.DateFormat)));
                        cmd.Parameters.Add(new SQLiteParameter("@sold_id", trnId));
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(ex,
                            "SQLCommunication.cs : function => FinishProduct(Int64 trnId, string description, short status)");
                        return false;
                    }
                    finally
                    {
                        cmd.Parameters.Clear();
                    }
                    return true;
                }
            }
        }
    }
}