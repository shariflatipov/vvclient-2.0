﻿using System;
using System.Configuration;
using System.IO.Ports;
using System.Windows.Forms;
using vvclient.utils;

namespace vvclient.peripheral
{
    internal class VfdChineese : IVfdDisplays, IDisposable
    {
        private readonly byte[] _end = {0x0D};
        private readonly string _portName;
        private readonly SerialPort _sp = new SerialPort();
        private readonly byte[] _start = {0x1B, 0x73, 0x30, 0x1B, 0x51, 0x41};

        public VfdChineese()
        {
            try
            {
                _portName = ConfigurationManager.AppSettings["vfdport"] ?? "com1";
                _sp.BaudRate = 2400;
                _sp.PortName = _portName;
                _sp.Parity = Parity.None;
                _sp.DataBits = 8;
                _sp.StopBits = StopBits.One;
                _sp.Handshake = Handshake.XOnXOff;
                _sp.Open();
            }
            catch (Exception e)
            {
                string msg = "Невозможно открыть порт:" + _portName;
                MessageBox.Show(msg);
                Logger.Log("VFD Display class=[VfdDisplay]", Level.Error);
                Logger.Log(e, msg, Level.Error);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Write(string text)
        {
            if (_sp.IsOpen)
            {
                _sp.Write(_start, 0, _start.Length);
                _sp.Write(text);
                _sp.Write(_end, 0, _end.Length);
            }
        }

        bool IVfdDisplays.IsNumeric
        {
            get { return true; }
        }

        public void ClearDisplay()
        {
            if (_sp.IsOpen)
            {
                _sp.Write(_start, 0, _start.Length);
                _sp.Write(_end, 0, _end.Length);
            }
        }

        public void Blink(byte frequency)
        {
        }

        public void Close()
        {
            if (_sp.IsOpen)
            {
                _sp.Close();
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                _sp.Dispose();
            }
            // free native resources
        }
    }
}