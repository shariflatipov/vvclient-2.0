﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using vvclient.utils;

namespace vvclient.peripheral
{
    public class Default : IPrinter, IDisposable
    {
        private readonly Font _fontBarcode = new Font("Code EAN13", 20);
        private bool _printBarCode;
        private Font _printFont;
        private string _strToPrint;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Print(string str)
        {
            _strToPrint += str;
        }

        public void Cut()
        {
            _printFont = new Font("Courier New", 8, FontStyle.Bold);

            var pd = new PrintDocument {PrintController = new StandardPrintController()};

            var asx = new PaperSize("npf", 850, 3000);
            pd.DefaultPageSettings.PaperSize = asx;
            pd.PrintPage += (PdPrintPage);
            pd.Print();
        }

        public void BoldOn()
        {
        }

        public void BoldOff()
        {
        }

        public void PrintEan13(string code, bool start)
        {
            _printBarCode = start;
        }

        private void PdPrintPage(object sender, PrintPageEventArgs ev)
        {
            var font = (_printBarCode) ? _fontBarcode : _printFont;
            var printLogo = settings.Config.ShowLogo;
            int logoOffset = 10;
            if (printLogo)
            {
                logoOffset = 90;
                try
                {
                    Image img = Image.FromFile("img/logo.png");
                    ev.Graphics.DrawImage(img, new Rectangle(0, 0, img.Width, img.Height));
                }
                catch (Exception e)
                {
                    Logger.Log(e);
                }
            }
            ev.Graphics.DrawString(_strToPrint, font, Brushes.Black, new Point(0, logoOffset));
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                _fontBarcode.Dispose();
                _printFont.Dispose();
            }
            // free native resources
        }
    }
}