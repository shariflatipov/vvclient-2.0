﻿namespace vvclient.peripheral
{
    public interface IPrinter
    {
        void Print(string str);
        void Cut();
        void BoldOn();
        void BoldOff();
        void PrintEan13(string code, bool start);
    }
}