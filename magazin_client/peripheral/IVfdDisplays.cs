﻿namespace vvclient.peripheral
{
    public interface IVfdDisplays
    {
        bool IsNumeric { get; }
        void Write(string text);
        void ClearDisplay();
        void Blink(byte frequency);
        void Close();
    }
}