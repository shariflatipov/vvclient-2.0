﻿using System;
using System.IO.Ports;
using System.Text;
using System.Windows.Forms;
using vvclient.settings;

namespace vvclient.peripheral
{
    internal class PosiflexAura6800 : IPrinter, IDisposable
    {
        private readonly byte[] _boldOff = {27, 69, 2};
        private readonly byte[] _boldOn = {27, 69, 1};
        private readonly byte[] _centeringOff = {27, 97, 0};
        private readonly byte[] _centeringOn = {27, 97, 1};
        private readonly byte[] _cut = {29, 86, 49, 27, 61, 2};
        private readonly byte[] _end = {27, 61, 2};
        private readonly SerialPort _sp = new SerialPort();
        private readonly byte[] _start = {27, 61, 1};

        public PosiflexAura6800()
        {
            try
            {
                _sp.BaudRate = 115200;
                _sp.Parity = Parity.None;
                _sp.PortName = Config.PrinterPort;
                _sp.StopBits = StopBits.One;
                _sp.DataBits = 8;
                _sp.Open();
            }
            catch (Exception)
            {
                MessageBox.Show("Невозможно открыть порт:" + Config.PrinterPort);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Print(string textToPrint)
        {
            if (_sp.IsOpen)
            {
                var cyrillic = Encoding.GetEncoding("cp866");
                var data = cyrillic.GetBytes(textToPrint);
                _sp.Write(_start, 0, _start.Length);
                _sp.Write(data, 0, data.Length);
                _sp.Write(_end, 0, _end.Length);
            }
        }

        public void BoldOn()
        {
            if (_sp.IsOpen)
            {
                _sp.Write(_start, 0, _start.Length);
                _sp.Write(_boldOn, 0, _boldOn.Length);
                _sp.Write(_end, 0, _end.Length);
            }
        }

        public void BoldOff()
        {
            if (_sp.IsOpen)
            {
                _sp.Write(_start, 0, _start.Length);
                _sp.Write(_boldOff, 0, _boldOff.Length);
                _sp.Write(_end, 0, _end.Length);
            }
        }

        public void Cut()
        {
            if (_sp.IsOpen)
            {
                _sp.Write(_start, 0, _start.Length);
                _sp.Write(_cut, 0, _cut.Length);
                _sp.Write(_end, 0, _end.Length);
                _sp.Close();
            }
        }

        public void PrintEan13(string code, bool start = false)
        {
            if (_sp.IsOpen)
            {
                _sp.Write(_start, 0, _start.Length);
                _sp.Write(new byte[] {0x1d, 0x66, 0x01}, 0, _centeringOff.Length); // Select hri font
                _sp.Write(_end, 0, _end.Length);

                _sp.Write(_start, 0, _start.Length);
                _sp.Write(new byte[] {0x1d, 0x48, 0x32}, 0, _centeringOff.Length);
                // select hri char size (the font below the barcode)
                _sp.Write(_end, 0, _end.Length);

                _sp.Write(_start, 0, _start.Length);
                _sp.Write(new byte[] {0x1d, 0x68, 0x50}, 0, _centeringOff.Length);
                // select barcode height (last digit is height, in this example its 80 (or #50)
                _sp.Write(_end, 0, _end.Length);

                _sp.Write(_start, 0, _start.Length);
                _sp.Write(new byte[] {0x1d, 0x77, 0x02}, 0, _centeringOff.Length);
                // select barcode width (last digit is width, in this example 2, which is default)
                _sp.Write(_end, 0, _end.Length);

                _sp.Write(_start, 0, _start.Length);

                // print barcode (this sequence must be followed by 12 digits that constitute the actual code 
                // (the 13th digit is a checksum number, that the printer calculates automatically), 
                // so the full (hex) sequence would be something like: 1d 6b 43 0c 39 31 32 30 30 30 30 31 30 36 30 31)
                _sp.Write(new byte[] {0x1d, 0x6b, 0x43, 0x0c}, 0, _centeringOff.Length);
                _sp.Write(_end, 0, _end.Length);
            }
        }

        public void CenteringOn()
        {
            if (_sp.IsOpen)
            {
                _sp.Write(_start, 0, _start.Length);
                _sp.Write(_centeringOn, 0, _centeringOn.Length);
                _sp.Write(_end, 0, _end.Length);
            }
        }

        public void CenteringOff()
        {
            if (_sp.IsOpen)
            {
                _sp.Write(_start, 0, _start.Length);
                _sp.Write(_centeringOff, 0, _centeringOff.Length);
                _sp.Write(_end, 0, _end.Length);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                _sp.Dispose();
            }
            // free native resources
        }
    }
}