﻿using System;
using System.Configuration;
using System.IO.Ports;
using System.Text;
using System.Windows.Forms;
using vvclient.utils;

namespace vvclient.peripheral
{
    public class VfdDisplay : IVfdDisplays, IDisposable
    {
        private readonly byte[] _cmdBlink = new byte[3];
        private readonly byte[] _cmdCharacteSet = {27, 82, 12};
        private readonly byte[] _cmdClearDisplay = {12};
        private readonly byte[] _cmdCodeTable = {27, 116, 7};
        private readonly string _portName;
        private readonly SerialPort _sp = new SerialPort();

        public VfdDisplay()
        {
            try
            {
                _portName = ConfigurationManager.AppSettings["vfdport"];
                _sp.BaudRate = 9600;
                _sp.PortName = _portName;
                _sp.Parity = Parity.None;
                _sp.DataBits = 8;
                _sp.StopBits = StopBits.One;
                _sp.Open();
            }
            catch (Exception e)
            {
                string msg = "Невозможно открыть порт:" + _portName;
                MessageBox.Show(msg);
                Logger.Log("VFD Display class=[VfdDisplay]", Level.Error);
                Logger.Log(e, msg, Level.Error);
            }

            if (_sp.IsOpen)
            {
                _sp.Write(_cmdCodeTable, 0, _cmdCodeTable.Length);
                _sp.Write(_cmdCharacteSet, 0, _cmdCharacteSet.Length);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Write(string text)
        {
            if (_sp.IsOpen)
            {
                var data = FormatData(text);
                _sp.Write(data, 0, data.Length);
            }
        }

        public void ClearDisplay()
        {
            if (_sp.IsOpen)
            {
                _sp.Write(_cmdClearDisplay, 0, _cmdClearDisplay.Length);
            }
        }

        public void Blink(byte frequency)
        {
            if (_sp.IsOpen)
            {
                if (frequency > 255)
                {
                    frequency = 0;
                }

                _cmdBlink[0] = 31;
                _cmdBlink[1] = 69;
                _cmdBlink[2] = frequency;
                _sp.Write(_cmdBlink, 0, _cmdBlink.Length);
            }
        }

        bool IVfdDisplays.IsNumeric
        {
            get { return false; }
        }

        public void Close()
        {
            if (_sp.IsOpen)
            {
                _sp.Close();
            }
        }

        private byte[] FormatData(string text)
        {
            var enc = Encoding.GetEncoding("cp866");

            var data = enc.GetBytes(text);

            return data;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                _sp.Dispose();
            }
            // free native resources
        }
    }
}